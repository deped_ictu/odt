
$(function(){

//update tn and doc-type
var title = "";
var ref_no ="";
var status ="";
var sender ="";
var cname ="";
$(".btnedit").click(function(){
	var token=$('input[name=_token]').val();
	var id = $(this).data('id');
	title = $(this).data('title');
	ref_no = $(this).data('refno');
	status = $(this).data('status');
	user = $(this).data('user');
	sender = $(this).data('sender');
	cname = $(this).data('cname');
	
	//$("#action").val(action);
	//$("#route").val(route);
	//$("#remarks").val(remarks);
	$("#txtids").val(id);
	alert(title);
});

 $("#btnupdate_tn").click(function(){
		var id = $("#txtids").val();
        var tn = $("#txttno").val();
		//var title = $(this).data('title');
		//var refno = $(this).data('refno');
		var act = $("#txtact").val();
		var rem = $("#txtre").val();
		//var stat = $(this).data('status');
		//var send = $(this).data('sender');
		//var cname = $(this).data('cname');
		var doc_type = $("#doc_type").val();
		var route = $("#txtoffice").val();
        var _token=$('input[name=_token]').val();
		var action = 'Received';
		//var remarks = $("#remarks").val();
		var user = $("#txtagent").val();
		//get the date
		var d = new Date();
		var year = d.getFullYear();
		var month =  ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
		var day = d.getDate();
		var date = year + '-' + month[d.getMonth()] + '-' + day;
		//get the time
		var t = new Date();
		var hour = t.getHours();
		var min = t.getMinutes();
		var sec = t.getSeconds();
		var time = hour + ':' + min + ':' + sec;
// alert(tn); alert(date); alert(time); alert(action); alert(doc_type); 
		alert(cname);
		alert (id);
            $.ajax({
                method:'post',
                url: UpdateTicketTN,
                data:{
                    '_token':_token,
                    'tn':tn,
                    'doc_type':doc_type,
                    'action':action,
					//'route':route,
					//'remarks':remarks,
					'date':date,
					'time':time,
					'user':user,
					'id':id,
                }
            })
			
			//insert another record
			$.ajax({
                method:'post',
                url: INSERTRECORD,
                data:{
                    '_token':_token,
                    'tn':tn,
					'title':title,
					'ref_no':ref_no,
					'action':act,
					'remarks':rem,
					'status':status,
					'sender':sender,
					'cname':cname,
                    'doc_type':doc_type,
					'route':route,
					'date':date,
					'time':time,
					'user':user,
                }
            })
			
            .done(function(msg){
				//alert(msg);
              window.location="/incoming_tickets";
              //console.log(msg['message']);
			
            });
    });
//end update ticket

//delete ticket with thesame tn
$(".btndelete_ticket").click(function(){
	var token=$('input[name=_token]').val();
	var tn = $(this).data('tn');
		if(confirm("Are you sure do you want to delete this record with ticket number" + ' ' + tn + '?'))
		{
			$.ajax({
				type:"POST",
				url:DeleteURL,
				data:{_token:token, txttn:tn},
				success:function(data)
				{
					window.location="/dashboard";
				}
			})
		}
		else {
			return false;
		}
});
//end delete ticket
//local storage
	$('.uptworow').click(function(){
		var tn=$(this).data('tn');
		localStorage.setItem('tn_cache', tn);
		alert(localStorage.getItem('tn_cache'));
	})
	
//update two rows 
$(".uptworow").click(function(){
	var token=$('input[name=_token]').val();
	var tn = $(this).data('tn');
	var title = $(this).data('title');
	var cname = $(this).data('cname');
	var dt = $(this).data('dt');
	var route = $(this).data('route');
	$("#txttno2").val(tn);
	$("#txttitle2").val(title);
	$("#txtcname2").val(cname);
	$("#txtdoc_type2").val(dt);
	$("#txtroute2").val(route);
});

 $("#btnsave").click(function(){
		var tn_local = localStorage.getItem('tn_cache');
        var tn = $("#txttno2").val();
		var title = $("#txttitle2").val();
		var cname = $("#txtcname2").val();
		var dt =  $("#txtdoc_type2").val();
		//var route = $("#txtroute2").val();
        var _token=$('input[name=_token]').val();
        alert (tn_local); 
// alert(tn); alert(date); alert(time); alert(action); alert(doc_type); 
            $.ajax({
                method:'post',
                url: EditURL,
                data:{
                    '_token':_token,
                    'tn_local':tn_local,
					'tn':tn,
                    'title':title,
					'cname':cname,
					'doc_type':dt,
					//'route':route,
                }
            })
            .done(function(msg){
				alert(msg);
              window.location="/dashboard";
              //console.log(msg['message']);
			
            });
    });
//update one ticket 
$(".updatet").click(function(){
	var token=$('input[name=_token]').val();
	var title = $(this).data('title');
	var route = $(this).data('route');
	var action = $(this).data('action');
	var id = $(this).data('id');
	
	$("#txttitle").val(title);
	$("#txtroute").val(route);
	$("#txtaction").val(action);
	$("#txtids3").val(id);
	//alert(title); alert(tn);
});

 $("#btnsaveupdate").click(function(){
		var title = $("#txttitle").val();
        var route = $("#txtroute").val();
		var action = $("#txtaction").val();
        var _token = $('input[name=_token]').val();
		var id = $("#txtids3").val();
        //alert (title); 
// alert(tn); alert(date); alert(time); alert(action); alert(doc_type); 
            $.ajax({
                method:'post',
                url: UPDATEONETICKET,
                data:{
                    '_token':_token,
                    'title':title,
					'route':route,
                    'action':action,
					'id':id,
                }
            })
            .done(function(msg){
				//alert(msg);
              window.location="/all_tickets";
              //console.log(msg);
			
            });
    });

//sync data online



}); //end