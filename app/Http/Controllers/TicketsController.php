<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Alert;
use Illuminate\Support\Facades\Session;
class TicketsController extends Controller
{
    //login view
    public function login(){
        return view('index');
    }    
    //login action
    public function login_dts(Request $request){
        $username = $request['txtUserName'];
        $password = $request['txtPassword'];
   
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'dts_login',
        'UserName'=> $username,
        'Password'=> $password,
        ]
    ]); 
    //return $response;
    $data=json_decode($response->getBody()->getContents(), true);
        foreach($data as $user){
        $UserName = $user['UserName'];
        $Password = $user['Password'];
        $lname = $user['Lastname'];
        $fname = $user['Firstname'];
        $dept = $user['Department'];
        $school_name = $user['sname'];
        $pname = $user['principal'];
        }
        if(!empty($data)){
            session(['UserName'=>$UserName]);
            session(['Lastname'=>$lname]);
            session(['Firstname'=>$fname]);
            session(['Department'=>$dept]);
            session(['sname'=>$school_name]);
            session(['principal'=>$pname]);
            session(['Password'=>$Password]);
            //Alert::success('You are successfully login!');
           
        return redirect('dashboard');
        }
        else if(empty($data)){
           // Alert::success('You are successfully login!'); 
        return redirect('index');
       }   
    }
    //logout
    public function logout(){
        Session::flush();
        return redirect()->route('index');
    }
    //dashboard
public function dashboard(){
    //assigned ticket per unit
    if (session('UserName')==null){
        Alert::info('Please Login!');
        return redirect()->route('index');
    }
    else{
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'getallticketsunit',  
        "department"=>session("Department"),
        ]
    ]);
    //select principal name
    $client=new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response_shname = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'selectshname',
            "UserName"=>session("UserName"),
        ]
    ]);
    //recently created tickets
    $client=new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $recent = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'recenttickets',
            'department'=>session("Department"),
        ]
    ]);
    //recently created tickets by school
    $client=new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $recentSA = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'recentticketsbyschool',
            'department'=>session("Department"),
        ]
    ]);
    //last tracking number
    $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $Last_TN = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'lasttrackingno',
            //'id'=> $id,
            ]
        ]);
    //count incoming ticket per unit
    $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $count_p = $client->request('POST', $url, [
        'form_params'=>[
        'tag'=>'countpendingtickets', 
        'department'=>session("Department"),
        ]]);
    //count out going ticket
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $count_outgoing = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'countreceivedtickets', 
    'department'=>session("Department"),
    ]]);
    //count incoming tickets for records
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $incoming_rec = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'incomingticketsforrecords', 
    //'department'=>session("Department"),
    ]]);
    //count outgoing tickets for records
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $outgoing_rec = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'outgoingticketsforrecords', 
    //'department'=>session("Department"),
    ]]);
    $rec_outgoing=json_decode($outgoing_rec->getBody()->getContents(),true);
    $rec_incoming=json_decode($incoming_rec->getBody()->getContents(),true);
    $outgoing=json_decode($count_outgoing->getBody()->getContents(),true);
    $count_pending = json_decode($count_p->getBody()->getContents(),true);
    $recent_SA = json_decode($recentSA->getBody()->getContents(), true);
    $LastTN=json_decode($Last_TN->getBody()->getContents(), true);
    $recent_tickets = json_decode($recent->getBody()->getContents(), true);
    $shname = json_decode($response_shname->getBody()->getContents(), true);
    $data=json_decode($response->getBody()->getContents(), true);
    
    //return $recent_SA;
    return view ('dashboard', ['ticket'=>$data, 'oicname'=>$shname, 'recent_t'=>$recent_tickets, 'lasttn'=>$LastTN, 'recentSA'=>$recent_SA, 'count_p'=>$count_pending, 'outgoing'=>$outgoing, 'rec_incoming'=>$rec_incoming, 'rec_outgoing'=>$rec_outgoing]);
    //return $count;
        }
    }
    //pending ticket per unit
    public function pending(){
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'getpendingticket',  
        "department"=>session("Department"),
        ]
    ]);
        //get last id
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response1 = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'getticket',
            //'id'=> $id,
            ]
        ]);
        //count incoming ticket per unit
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $count_p = $client->request('POST', $url, [
        'form_params'=>[
        'tag'=>'countpendingtickets', 
        'department'=>session("Department"),
        ]]);
        //count out going ticket
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $count_outgoing = $client->request('POST', $url, [
        'form_params'=>[
        'tag'=>'countreceivedtickets', 
        'department'=>session("Department"),
        ]]);
        //count incoming tickets for records
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $incoming_rec = $client->request('POST', $url, [
        'form_params'=>[
        'tag'=>'incomingticketsforrecords', 
        //'department'=>session("Department"),
        ]]);
        //count outgoing tickets for records
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $outgoing_rec = $client->request('POST', $url, [
        'form_params'=>[
        'tag'=>'outgoingticketsforrecords', 
        //'department'=>session("Department"),
        ]]);
    $rec_outgoing=json_decode($outgoing_rec->getBody()->getContents(),true);
    $rec_incoming=json_decode($incoming_rec->getBody()->getContents(),true);
    $outgoing=json_decode($count_outgoing->getBody()->getContents(),true);
    $count_pending = json_decode($count_p->getBody()->getContents(),true);
    $data1=json_decode($response1->getBody()->getContents(), true);
    $pending=json_decode($response->getBody()->getContents(), true);
    return view ('pending_tickets', ['pending'=>$pending, 'getid'=>$data1, 'count_p'=>$count_pending, 'outgoing'=>$outgoing, 'rec_incoming'=>$rec_incoming, 'rec_outgoing'=>$rec_outgoing]);
    }
    //add ticket
    public function addTicket(Request $request){
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $time = date('H:i:s');
    $tn = $request['tn'];
    $ref_no = $request['ref_no'];
    $title = $request['title'];
    $remarks=$request['remarks'];
    $status = $request['status'];
    $sender = $request['sender'];  
    $action = $request['action'];  
    $cname = $request['cname']; 
    $doc_type = $request['doc_type']; 
    $route = $request['route']; 
    $route1 = Session::get('Department');
    $lname = Session::get('Lastname');
    $fname = Session::get('Firstname');
    $user = $fname.' '. $lname;
    //$agent = Session::get('Firstname');
    //connection to guzzle
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'addticket',
            'tn'=>$tn,
            'ref_no'=>$ref_no,
            'title'=>$title,
            'action'=>$action,
            'remarks'=>$remarks,
            'status'=>$status,
            'sender'=>$sender,          
            'cname'=>$cname,
            'doc_type'=>$doc_type,
            'route'=>$route1,
            'date'=>$date,
            'time'=>$time,
            'user'=>$user,                              
            ]
        ]);
       //second record 
       $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'addticket',
            'tn'=>$tn,
            'ref_no'=>$ref_no,
            'title'=>$title,
            'action'=>'',
            'remarks'=>$remarks,
            'status'=>$status,
            'sender'=>$sender,          
            'cname'=>$cname,
            'doc_type'=>$doc_type,
            'route'=>$route,
            'date'=>$date,
            'time'=>$time,
            'user'=>$user,                             
            ]
        ]);
        Alert::success('Record Successfully Added', 'Record Save'); 
            return redirect()->route('dashboard');   
    }
    //add ticket school account
    public function addTicketSchoolAccount(Request $request){
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $time = date('H:i:s');
    $tn = $request['tn'];
    $ref_no = $request['ref_no'];
    $title = $request['title'];
    $remarks=$request['remarks'];
    $status = $request['status'];
    $sender = $request['sender'];  
    $action = $request['action'];  
    $cname = Session::get('Department');
    $doc_type = $request['doc_type']; 
    $route = $request['route']; 
    //$route1 = Session::get('Department');
    $lname = Session::get('Lastname');
    $fname = Session::get('Firstname');
    $user = $fname.' '. $lname;
    //$agent = Session::get('Firstname');
    //connection to guzzle
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'addticket_schoolacc',
            'tn'=>$tn,
            'ref_no'=>$ref_no,
            'title'=>$title,
            'action'=>$action,
            'remarks'=>$remarks,
            'status'=>$status,
            'sender'=>$sender,          
            'cname'=>$cname,
            'doc_type'=>$doc_type,
            'route'=>$route,
            'date'=>$date,
            'time'=>$time,
            'user'=>$user,                           
            ]
        ]);
      //return  $response;
        Alert::success('Record Successfully Added', 'Record Save'); 
            return redirect()->route('dashboard');   
    }
    //accept pending ticket
    public function AcceptTicket(Request $request){
    $id = $request['id'];
    $date=$request['date'];
    $time=$request['time'];
    $action=$request['action'];
    $lname = Session::get('Lastname');
    $fname = Session::get('Firstname');
    $user = $fname.' '. $lname;
    //update
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'acceptticket',
        'id'=> $id,
        'action'=>$action,
        'date'=>$date,
        'time'=>$time,
        'user'=>$user,
        ]
    ]);
    //return $agent;
    Alert::success('', 'Ticket successfully accepted!');
    //return redirect()->route('transparency/req_quotation');
    }
    //update tn and doc-type
    public function UpdateTN(Request $request){
    //insert
    $action1=''; //transferred
    $route1=$request['route'];
    $status1=$request['status'];
    $title1=$request['title'];
    $ref_no1=$request['ref_no'];
    $sender1=$request['sender'];
    $cname1=$request['cname'];
    $remarks1=$request['remarks'];
    $user=$request['user'];
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $time = date('H:i:s');
    //update
    $id_up = $request['id'];
    $tn_up=$request['tn'];
    $doc_type_up=$request['doc_type'];
    $action_up=$request['action']; //received
    $user=$request['user'];
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $time = date('H:i:s');
    //update
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'updateblanktn', 
        'id'=> $id_up,
        'tn'=>$tn_up,
        'doc_type'=>$doc_type_up,
        'action'=>$action_up,
        'date'=>$date,
        'time'=>$time,
        'user'=>$user, 
        ]
    ]);
    //insert blank ACTION
    $client1 = new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response_insert = $client1->request('POST', $url, [
    'form_params'=>[
        'tag'=>'transferticketfromschool',
        'tn'=>$tn_up,
        'ref_no'=>$ref_no1,
        'title'=>$title1,
        'action'=>$action1,
        'remarks'=>$remarks1,
        'status'=>$status1,
        'sender'=>$sender1,
        'cname'=>$cname1,
        'doc_type'=>$doc_type_up,
        'route'=>$route1,
        'date'=>$date,
        'time'=>$time,
        'user'=>$user,
    ]
    ]);
    }
    //search key words
    public function search(Request $request){
    $search = $request['txtsearch_tickettn'];
    //connection to guzzle
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
   $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'searchticket',  
        'tn'=>$search,
        'title'=>$search,
        ]
    ]);
    $data=json_decode($response->getBody()->getContents(), true);
    return view ('search', ['searchticket'=>$data]);
    }
    //search exact match
    public function searchexactticket(Request $request){
    $search = $request['txtsearch_ticket'];
         //connection to guzzle
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
   $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'searchexactticket',  
        'tn'=>$search,
        ]
    ]);
    $data=json_decode($response->getBody()->getContents(), true);
    return $data;
    }
    //ticket details
    public function ticketdetails(Request $request){
    $tn = $request['tn'];
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response_nt = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'selectthesametn',
        'tn'=> $tn,
        ]
    ]);
    //count incoming ticket per unit
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $count_p = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'countpendingtickets', 
    'department'=>session("Department"),
    ]]);
    //count out going ticket
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $count_outgoing = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'countreceivedtickets', 
    'department'=>session("Department"),
    ]]);
     //count incoming tickets for records
     $client= new \GuzzleHttp\Client();
     $url = WEBSERVICE_URL;
     $incoming_rec = $client->request('POST', $url, [
     'form_params'=>[
     'tag'=>'incomingticketsforrecords', 
     //'department'=>session("Department"),
     ]]);
     //count outgoing tickets for records
     $client= new \GuzzleHttp\Client();
     $url = WEBSERVICE_URL;
     $outgoing_rec = $client->request('POST', $url, [
     'form_params'=>[
     'tag'=>'outgoingticketsforrecords', 
     //'department'=>session("Department"),
     ]]);
        $rec_outgoing=json_decode($outgoing_rec->getBody()->getContents(),true);
        $rec_incoming=json_decode($incoming_rec->getBody()->getContents(),true);
        $outgoing=json_decode($count_outgoing->getBody()->getContents(),true);
        $count_pending = json_decode($count_p->getBody()->getContents(),true);
    $data=json_decode($response_nt->getBody()->getContents(), true);
    //return $response_nt;
    return view('ticket', ['ticket'=>$data, 'count_p'=>$count_pending, 'outgoing'=>$outgoing, 'rec_incoming'=>$rec_incoming, 'rec_outgoing'=>$rec_outgoing]);
        //return $data;
    }
    //search ticket    
    public function search_trail(Request $request){
        $search = $request['txtsearch_tickettn'];
    //connection to guzzle
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'searchticket_trail',  
    'tn'=>$search,
    ]
    ]);
    $data= json_decode($response->getBody()->getContents(),true);
    return $data;
    }
    //search trail ticket (index)   
    public function search_trailI(Request $request){
    $search = $request['txtsearch_ticket'];
    //connection to guzzle
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'searchticket_trail',  
    'tn'=>$search,
    ]
    ]);
    $data= json_decode($response->getBody()->getContents(),true);
    return $data;
    }
    //transfer one ticket
    public function Transfer_Ticket(Request $request){
    $id = $request['id'];
    $action = 'Transferred';
    $route=$request['route'];
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $time = date('H:i:s');
    $lname = Session::get('Lastname');
    $fname = Session::get('Firstname');
    $user = $fname.' '. $lname;
    //transferred status
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'transferticket',
        'id'=>$id,
        'action'=>$action,
        'route'=>$route,
        'date'=>$date,
        'time'=>$time,
        'user'=>$user, 
        ]
    ]);
    }
    //add document trail
    public function AddDocumentTrail(Request $request){
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $time = date('H:i:s');
    $tn = $request['txttn'];
    $ref_no = $request['txtref_no'];
    $title = $request['txttitle'];
    $remarks=$request['txtremarks'];
    $status = $request['txtstatus'];
    $sender = $request['txtsender'];  
    $action = $request['txtaction'];  
    $cname = $request['txtcname']; 
    $doc_type = $request['txtdoc_type']; 
    $route = $request['txtroute']; 
    $route1 = Session::get('Department');
    $lname = Session::get('Lastname');
    $fname = Session::get('Firstname');
    $user = $fname.' '. $lname;
    //$agent = Session::get('Firstname');
    //connection to guzzle
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'adddocumenttrail',
            'tn'=>$tn,
            'ref_no'=>$ref_no,
            'title'=>$title,
            'action'=>$action,
            'remarks'=>$remarks,
            'status'=>$status,
            'sender'=>$sender,          
            'cname'=>$cname,
            'doc_type'=>$doc_type,
            'route'=>$route,
            'date'=>$date,
            'time'=>$time,
            'user'=>$user,                        
            ]
        ]);
      //return  $response;
        Alert::success('', 'Record Save'); 
        if(Session::get('Department')=='Records'){
        return redirect()->route('outgoing_tickets');   
        }
        else{
            return redirect()->route('dashboard');   
        }    
    }
    //incoming tickets
    public function IncomingTickets(){
        if (session('UserName')==null){
            Alert::info('Please Login!');
            return redirect()->route('index');
        }
        else{
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'incomingtickets',  
            //"department"=>session("Department"),
            ]
        ]);
        //count pending ticket per unit
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $count_p = $client->request('POST', $url, [
        'form_params'=>[
        'tag'=>'countpendingtickets', 
        'department'=>session("Department"),
        ]]);
        //count out going ticket
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $count_outgoing = $client->request('POST', $url, [
        'form_params'=>[
        'tag'=>'countreceivedtickets', 
        'department'=>session("Department"),
        ]]);
        //count incoming tickets for records
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $incoming_rec = $client->request('POST', $url, [
        'form_params'=>[
        'tag'=>'incomingticketsforrecords', 
        //'department'=>session("Department"),
        ]]);
        //count outgoing tickets for records
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $outgoing_rec = $client->request('POST', $url, [
        'form_params'=>[
        'tag'=>'outgoingticketsforrecords', 
        //'department'=>session("Department"),
        ]]);
        $rec_outgoing=json_decode($outgoing_rec->getBody()->getContents(),true);
        $rec_incoming=json_decode($incoming_rec->getBody()->getContents(),true);
        $outgoing=json_decode($count_outgoing->getBody()->getContents(),true);
        $count_pending = json_decode($count_p->getBody()->getContents(),true);
        $data=json_decode($response->getBody()->getContents(), true);
        //return $data;
        return view ('incoming_tickets', ['incomingT'=>$data, 'count_p'=>$count_pending, 'outgoing'=>$outgoing, 'rec_incoming'=>$rec_incoming, 'rec_outgoing'=>$rec_outgoing]);
        //return view ('closed_tickets', ['closedst'=>$data]);
        }
    }
    //outgoing tickets
    public function OutgoingTickets(){
        if (session('UserName')==null){
            Alert::info('Please Login!');
            return redirect()->route('index');
        }
        else{
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'outgoingtickets',  
        // "department"=>session("Department"),
            ]
        ]);
         //count incoming ticket per unit
         $client= new \GuzzleHttp\Client();
         $url = WEBSERVICE_URL;
         $count_p = $client->request('POST', $url, [
         'form_params'=>[
         'tag'=>'countpendingtickets', 
         'department'=>session("Department"),
         ]]);
         //count out going ticket
         $client= new \GuzzleHttp\Client();
         $url = WEBSERVICE_URL;
         $count_outgoing = $client->request('POST', $url, [
         'form_params'=>[
         'tag'=>'countreceivedtickets', 
         'department'=>session("Department"),
         ]]);
         //count incoming tickets for records
         $client= new \GuzzleHttp\Client();
         $url = WEBSERVICE_URL;
         $incoming_rec = $client->request('POST', $url, [
         'form_params'=>[
         'tag'=>'incomingticketsforrecords', 
         //'department'=>session("Department"),
         ]]);
         //count outgoing tickets for records
         $client= new \GuzzleHttp\Client();
         $url = WEBSERVICE_URL;
         $outgoing_rec = $client->request('POST', $url, [
         'form_params'=>[
         'tag'=>'outgoingticketsforrecords', 
         //'department'=>session("Department"),
         ]]);
        $rec_outgoing=json_decode($outgoing_rec->getBody()->getContents(),true);
        $rec_incoming=json_decode($incoming_rec->getBody()->getContents(),true);
        $outgoing=json_decode($count_outgoing->getBody()->getContents(),true);
        $count_pending = json_decode($count_p->getBody()->getContents(),true);
        $outgoing=json_decode($count_outgoing->getBody()->getContents(),true);
        $data=json_decode($response->getBody()->getContents(), true);
        //return $data;
        return view ('outgoing_tickets', ['outgoingT'=>$data, 'outgoing'=>$outgoing, 'count_p'=>$count_pending, 'outgoing'=>$outgoing, 'rec_incoming'=>$rec_incoming, 'rec_outgoing'=>$rec_outgoing]);
        //return view ('closed_tickets', ['closedst'=>$data]);
        }
    }
    //my tickets (created by school)
    public function SchoolTickets(){
        if (session('UserName')==null){
            Alert::info('Please Login!');
            return redirect()->route('index');
        }
        else{
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'SchoolTickets',  
            "department"=>session("Department"),
            ]
        ]);
        $data=json_decode($response->getBody()->getContents(), true);
        //return $data;
        return view ('school_tickets', ['schooltkts'=>$data]);
        //return view ('closed_tickets', ['closedst'=>$data]);
        }
    } 
    //delete ticket thesame tn
    public function deletedticket(Request $request){
        $tn = $request['txttn'];
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'deleteticket',
            'tn'=> $tn,
            ]
        ]);
        //return $response;
        //Alert::success('Record Successfully Deleted', 'Record Save');
        //return redirect()->route('transparency/req_quotation');
    }   
    //update thesame tracking number
    public function UpdatethesameTN(Request $request){
        $tn = $request['tn'];
        $tn_local=$request['tn_local'];
        $title = $request['title'];
        $cname = $request['cname'];
        $doc_type = $request['doc_type'];
        //$route = $request['route'];
        //update
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'updatethesametn',
            'tn'=> $tn,
            'tn_local'=>$tn_local,
            'title'=>$title, 
            'cname'=>$cname,
            'doc_type'=>$doc_type,
            //'route'=>$route, 
            ]
        ]);
        return $response;
    // Alert::success('', 'Ticket successfully accepted!');
        //return redirect()->route('dashboard');
    }    
    //open tickets
    public function OPENTICKETS(){
        if (session('UserName')==null){
            Alert::info('Please Login!');
            return redirect()->route('index');
        }
        else{
            //count open tickets per office
            $client= new \GuzzleHttp\Client();
            $url = WEBSERVICE_URL;
            $count = $client->request('POST', $url, [
            'form_params'=>[
            'tag'=>'openticketssds', 
            ]]);
    //count incoming ticket per unit
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $count_p = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'countpendingtickets', 
    'department'=>session("Department"),
    ]]);
    //count out going ticket
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $count_outgoing = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'countreceivedtickets', 
    'department'=>session("Department"),
    ]]);
     //count incoming tickets for records
     $client= new \GuzzleHttp\Client();
     $url = WEBSERVICE_URL;
     $incoming_rec = $client->request('POST', $url, [
     'form_params'=>[
     'tag'=>'incomingticketsforrecords', 
     //'department'=>session("Department"),
     ]]);
     //count outgoing tickets for records
     $client= new \GuzzleHttp\Client();
     $url = WEBSERVICE_URL;
     $outgoing_rec = $client->request('POST', $url, [
     'form_params'=>[
     'tag'=>'outgoingticketsforrecords', 
     //'department'=>session("Department"),
     ]]);
        $rec_outgoing=json_decode($outgoing_rec->getBody()->getContents(),true);
        $rec_incoming=json_decode($incoming_rec->getBody()->getContents(),true);
        $outgoing=json_decode($count_outgoing->getBody()->getContents(),true);
        $count_pending = json_decode($count_p->getBody()->getContents(),true);
        $count_tick = json_decode($count->getBody()->getContents(),true);
        //$open_tickets = json_decode($open->getBody()->getContents(), true);
            //return $count_tick;
    return view ('admin', ['countopentkts'=>$count_tick, 'count_p'=>$count_pending, 'outgoing'=>$outgoing, 'rec_incoming'=>$rec_incoming, 'rec_outgoing'=>$rec_outgoing]);
        }
    }    
    //list of open tickets
    public function LISTOPENTICKETS(Request $request){
        $office=$request['route'];
        if (session('UserName')==null){
            Alert::info('Please Login!');
            return redirect()->route('index');
        }
        else{
            //count open tickets per office
            $client= new \GuzzleHttp\Client();
            $url = WEBSERVICE_URL;
            $list_count = $client->request('POST', $url, [
            'form_params'=>[
            'tag'=>'opentickets', 
            'route'=>$office 
            ]]);
    //count incoming ticket per unit
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $count_p = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'countpendingtickets', 
    'department'=>session("Department"),
    ]]);
    //count out going ticket
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $count_outgoing = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'countreceivedtickets', 
    'department'=>session("Department"),
    ]]);
     //count incoming tickets for records
     $client= new \GuzzleHttp\Client();
     $url = WEBSERVICE_URL;
     $incoming_rec = $client->request('POST', $url, [
     'form_params'=>[
     'tag'=>'incomingticketsforrecords', 
     //'department'=>session("Department"),
     ]]);
     //count outgoing tickets for records
     $client= new \GuzzleHttp\Client();
     $url = WEBSERVICE_URL;
     $outgoing_rec = $client->request('POST', $url, [
     'form_params'=>[
     'tag'=>'outgoingticketsforrecords', 
     //'department'=>session("Department"),
     ]]);
        $rec_outgoing=json_decode($outgoing_rec->getBody()->getContents(),true);
        $rec_incoming=json_decode($incoming_rec->getBody()->getContents(),true);
        $outgoing=json_decode($count_outgoing->getBody()->getContents(),true);
        $count_pending = json_decode($count_p->getBody()->getContents(),true);
        $listoopt = json_decode($list_count->getBody()->getContents(),true);
        //$listoopt = $list_count->getBody()->getContents();
            //return $list_count;
    return view ('open_tickets', ['listopen_tckts'=>$listoopt, 'count_p'=>$count_pending, 'outgoing'=>$outgoing, 'rec_incoming'=>$rec_incoming, 'rec_outgoing'=>$rec_outgoing]);
        }
    }
//print logs (current date)
public function printlogs(Request $request){
    if (session('UserName')==null){
    Alert::info('Please Login!');
    return redirect()->route('index');
}
else{
    date_default_timezone_set('Asia/Manila');
    $dates = date('Y-m-d');

    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'printlogs',  
    'date'=>$dates,
    "department"=>session("Department"),
    ]
    ]);
    //count incoming tickets
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $count_p = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'countpendingtickets', 
    'department'=>session("Department"),
    ]]);
    //count out going ticket
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $count_outgoing = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'countreceivedtickets', 
    'department'=>session("Department"),
    ]]);
     //count incoming tickets for records
     $client= new \GuzzleHttp\Client();
     $url = WEBSERVICE_URL;
     $incoming_rec = $client->request('POST', $url, [
     'form_params'=>[
     'tag'=>'incomingticketsforrecords', 
     //'department'=>session("Department"),
     ]]);
     //count outgoing tickets for records
     $client= new \GuzzleHttp\Client();
     $url = WEBSERVICE_URL;
     $outgoing_rec = $client->request('POST', $url, [
     'form_params'=>[
     'tag'=>'outgoingticketsforrecords', 
     //'department'=>session("Department"),
     ]]);
     $rec_outgoing=json_decode($outgoing_rec->getBody()->getContents(),true);
     $rec_incoming=json_decode($incoming_rec->getBody()->getContents(),true);
    $outgoing=json_decode($count_outgoing->getBody()->getContents(),true);
    $count_pending = json_decode($count_p->getBody()->getContents(),true);
    $data=json_decode($response->getBody()->getContents(), true);
    //return $response;
    //return view ('print_logs', );
    return view ('print_logs', ['logs'=>$data, 'count_p'=>$count_pending, 'outgoing'=>$outgoing, 'rec_incoming'=>$rec_incoming, 'rec_outgoing'=>$rec_outgoing]);
    }
}
//print logs (filter date)
public function filterdate(Request $request){
    $from = $request['From'];
    $to = $request['to'];
         //connection to guzzle
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
   $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'filterdate',  
        'From'=>$from,
        'to'=>$to,
        "department"=>session("Department"),

        ]
    ]);
    
    $data=json_decode($response->getBody()->getContents(), true);
    return $data;  
    //return view ('filterdate', ['filter'=>$data]);

    } 
//all tickets
public function AllTickets(Request $request){
    if (session('UserName')==null){
    Alert::info('Please Login!');
    return redirect()->route('index');
}
else{
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'alltickets',  
    ]
    ]);
    //count incoming tickets
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $count_p = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'countpendingtickets', 
    'department'=>session("Department"),
    ]]);
    //count out going ticket
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $count_outgoing = $client->request('POST', $url, [
    'form_params'=>[
    'tag'=>'countreceivedtickets', 
    'department'=>session("Department"),
    ]]);
    $outgoing=json_decode($count_outgoing->getBody()->getContents(),true);
    $count_pending = json_decode($count_p->getBody()->getContents(),true);
    $data=json_decode($response->getBody()->getContents(), true);
    //return $data;
    //return view ('print_logs', );
    return view ('all_tickets', ['alltickets'=>$data, 'count_p'=>$count_pending, 'outgoing'=>$outgoing]);
    }
}
//update one ticket
public function UpdateOneTicket(Request $request){
    $id = $request['id'];
    $title=$request['title'];
    $route=$request['route'];
    $action=$request['action'];
    
    //update
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'updateonticket',
        'id'=> $id,
        'title'=>$title,
        'route'=>$route,
        'action'=>$action,
        ]
    ]);
    //return $response;
    //Alert::success('', 'Ticket successfully updated!');
    }

//update username & password
public function UpdateUserNamePass(Request $request){
    $username = $request['UserName'];
    $confirm_pass=$request['Password'];
    $fn=Session::get('Firstname');

    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'update_unpass',
        'UserName'=> $username,
        'Firstname'=>$fn,
        'Password'=>$confirm_pass,
        ]
    ]);
    Alert::success('', 'Username and Password successfully updated!');
    Session::flush();  
    return redirect()->route('index');
    }
//sync data online

}// end
