<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Alert;

class TicketController extends Controller
{    
    public function archived_tickets(){
        //assigned ticket per unit
        if (session('Email')==null){
            Alert::info('Please Login!');
            return redirect()->route('index');
        }
        else{
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'archivedtickets',  
            "department"=>session("Department"),
            ]
        ]);
        
        //count ticket (tickets received)
            $client= new \GuzzleHttp\Client();
            $url = WEBSERVICE_URL;
            $response1 = $client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'countticket',  
                "department"=>session("Department"),
                ]
            ]);
        
        //count ticket (tickets closed / released)
            $client= new \GuzzleHttp\Client();
            $url = WEBSERVICE_URL;
            $response2 = $client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'countticketclosed',  
                "department"=>session("Department"),
                ]
            ]);
            
        //count transferred ticket
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response3 = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'counttickettransferred',  
            "department"=>session("Department"),
            ]
        ]);
        
        //count pending tickets
        $client=new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response_pending = $client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'countpending',
                "department"=>session("Department"),
            ]
        ]);
        
        //count assigned tickets
        $client=new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response_assigned = $client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'countassigned',
                "department"=>session("Department"),
            ]
        ]);
        
        //count transfer tickets to sds from records
        $client=new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response_sds_trans = $client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'counttransfersds',
                "department"=>session("Department"),
            ]
        ]);
        
            //get last id
            $client= new \GuzzleHttp\Client();
            $url = WEBSERVICE_URL;
            $response1 = $client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'getticket',
                //'id'=> $id,
                ]
        ]);
        $data_sds_trans=json_decode($response_sds_trans->getBody()->getContents(), true);
        $data_assigned=json_decode($response_assigned->getBody()->getContents(), true);
        $data_pending=json_decode($response_pending->getBody()->getContents(), true);
        $data1=json_decode($response1->getBody()->getContents(), true);
        $transferred=json_decode($response3->getBody()->getContents(), true);
        $closed=json_decode($response2->getBody()->getContents(), true);
        $count=json_decode($response1->getBody()->getContents(), true);
        $data=json_decode($response->getBody()->getContents(), true);
        
        //return $data_pending;
        return view ('archived', ['archived_ticket'=>$data, 'countticket'=>$count, 'closedticket'=>$closed, 'transferred'=>$transferred, 'getid'=>$data1, 'pendingtick'=>$data_pending, 'assigned'=>$data_assigned, 'sdstransfer'=>$data_sds_trans ]);
        //return $count;
            }
        }

    //add remarks
public function archivedRemarks(Request $request){
    $id = $request['id'];
    $remarks=$request['remarks'];

    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'remarks_archived',
        'id'=> $id,
        'remarks'=>$remarks,
    
        ]
    ]);
    return $response;
    Alert::success('Remarks successfully added!', 'Record Saved');
    //return redirect()->route('transparency/req_quotation');
}

//Server Side (school account)
    public function fetch_tickets(Request $request)
    {
        $data_arr = $request->all();
        $client = new Client();
        $url = WEBSERVICE_URL;
        $response = $client->request("POST", $url, ['form_params' => [
                'tag' => 'fetch_tickets',
                'data' => $data_arr
        ]
        ]);
        $res = $response->getBody()->getContents();
        return $res;
    }


}
?>