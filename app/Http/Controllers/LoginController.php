<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Alert;


class LoginController extends Controller
{

    public function login(){
            return view('index');
    }

//get id    
    public function getid(){

        //$id = $request['id'];
       //get last id
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response1 = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'getticket',
        //'id'=> $id,
        ]
    ]);
//select principal name
    $client=new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response_shname = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'selectshname',
            "UserName"=>session("UserName"),
        ]
    ]);

    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'incomingtickets',  
        "department"=>session("Department"),
        ]
    ]);
    
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response_out = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'outgoingtickets',  
        "department"=>session("Department"),
        ]
    ]);
    
    $outgoing=json_decode($response_out->getBody()->getContents(), true);
    $data=json_decode($response->getBody()->getContents(), true);
    $shname=json_encode($response_shname->getBody()->getContents(), true);
    $data1=json_decode($response1->getBody()->getContents(), true);
    return view ('/component/modal', ['getid'=>$data1, 'oicname'=>$shname, 'incomingT'=>$data, 'outgoingT'=>$outgoing]);

                //Alert::info('You are successfully login!');                
               // return view('created_ticket');            
    }

    public function login_dts(Request $request){
        $username = $request['txtUserName'];
        $password = $request['txtPassword'];
   
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'dts_login',
        'UserName'=> $username,
        'Password'=> $password,
        ]
    ]);
    $data=json_decode($response->getBody()->getContents(), true);
        foreach($data as $user){
        $UserName = $user['UserName'];
        $Password = $user['Password'];
        $lname = $user['Lastname'];
        $fname = $user['Firstname'];
        $dept = $user['Department'];
        $school_name = $user['sname'];
        $pname = $user['principal'];
        }
        if (!empty($data)){
            session(['UserName'=>$UserName]);
            session(['Lastname'=>$lname]);
            session(['Firstname'=>$fname]);
            session(['Department'=>$dept]);
            session(['sname'=>$school_name]);
            session(['principal'=>$pname]);
            //Alert::success('You are successfully login!');
            
        return redirect('dashboard');
        }
        else if(empty($data)){
            Alert::success('You are successfully login!'); 
        return redirect('index');
       }   
}
    
//logout
public function logout(){
    Session::flush();
    return redirect()->route('index');
}

//dashboard
public function dashboard(){
//assigned ticket per unit
if (session('UserName')==null){
    Alert::info('Please Login!');
    return redirect()->route('index');
}
else{
$client= new \GuzzleHttp\Client();
$url = WEBSERVICE_URL;
$response = $client->request('POST', $url, [
'form_params'=>[
    'tag'=>'getallticketsunit',  
    "department"=>session("Department"),
    ]
]);

//select principal name
$client=new \GuzzleHttp\Client();
$url = WEBSERVICE_URL;
$response_shname = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'selectshname',
        "UserName"=>session("UserName"),
    ]
]);

//recently created tickets
$client=new \GuzzleHttp\Client();
$url = WEBSERVICE_URL;
$recent = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'recenttickets',
        'department'=>session("Department"),
    ]
]);
//recently created tickets by school
$client=new \GuzzleHttp\Client();
$url = WEBSERVICE_URL;
$recentSA = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'recentticketsbyschool',
        'department'=>session("Department"),
    ]
]);
//last tracking number
$client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $Last_TN = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'lasttrackingno',
        //'id'=> $id,
        ]
    ]);
$recent_SA = json_decode($recentSA->getBody()->getContents(), true);
$LastTN=json_decode($Last_TN->getBody()->getContents(), true);
$recent_tickets = json_decode($recent->getBody()->getContents(), true);
$shname = json_decode($response_shname->getBody()->getContents(), true);
$data=json_decode($response->getBody()->getContents(), true);

//return $recent_SA;
return view ('dashboard', ['ticket'=>$data, 'oicname'=>$shname, 'recent_t'=>$recent_tickets, 'lasttn'=>$LastTN, 'recentSA'=>$recent_SA]);
//return $count;
    }
}

//pending ticket per unit
public function pending(){
$client= new \GuzzleHttp\Client();
$url = WEBSERVICE_URL;
$response = $client->request('POST', $url, [
'form_params'=>[
    'tag'=>'getpendingticket',  
    "department"=>session("Department"),
    ]
]);
    //get last id
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response1 = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'getticket',
        //'id'=> $id,
        ]
    ]);
$data1=json_decode($response1->getBody()->getContents(), true);
$pending=json_decode($response->getBody()->getContents(), true);
return view ('pending_tickets', ['pending'=>$pending, 'getid'=>$data1]);
}

//add ticket
public function addTicket(Request $request){
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $time = date('H:i:s');
    $tn = $request['tn'];
    $ref_no = $request['ref_no'];
    $title = $request['title'];
    $remarks=$request['remarks'];
    $status = $request['status'];
    $sender = $request['sender'];  
    $action = $request['action'];  
    $cname = $request['cname']; 
    $doc_type = $request['doc_type']; 
    $route = $request['route']; 
    $route1 = Session::get('Department');
    $lname = Session::get('Lastname');
    $fname = Session::get('Firstname');
    $user = $fname.' '. $lname;
    //$agent = Session::get('Firstname');
    //connection to guzzle
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'addticket',
            'tn'=>$tn,
            'ref_no'=>$ref_no,
            'title'=>$title,
            'action'=>$action,
            'remarks'=>$remarks,
            'status'=>$status,
            'sender'=>$sender,          
            'cname'=>$cname,
            'doc_type'=>$doc_type,
            'route'=>$route1,
            'date'=>$date,
            'time'=>$time,
            'user'=>$user,                              
            ]
        ]);
       //second record 
       $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'addticket',
            'tn'=>$tn,
            'ref_no'=>$ref_no,
            'title'=>$title,
            'action'=>'',
            'remarks'=>$remarks,
            'status'=>$status,
            'sender'=>$sender,          
            'cname'=>$cname,
            'doc_type'=>$doc_type,
            'route'=>$route,
            'date'=>$date,
            'time'=>$time,
            'user'=>$user,      
                                    
            ]
        ]);
      //return  $response;
        Alert::success('Record Successfully Added', 'Record Save'); 
        //if(Session::get('Department')=='Records'){
        //return redirect()->route('created_ticket');   
        //}
        //else{
            return redirect()->route('dashboard');   
        //}    
}

//add ticket school account
public function addTicketSchoolAccount(Request $request){
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $time = date('H:i:s');
    $tn = $request['tn'];
    $ref_no = $request['ref_no'];
    $title = $request['title'];
    $remarks=$request['remarks'];
    $status = $request['status'];
    $sender = $request['sender'];  
    $action = $request['action'];  
    $cname = Session::get('Department');
    $doc_type = $request['doc_type']; 
    $route = $request['route']; 
    //$route1 = Session::get('Department');
    $lname = Session::get('Lastname');
    $fname = Session::get('Firstname');
    $user = $fname.' '. $lname;
    //$agent = Session::get('Firstname');
    //connection to guzzle
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'addticket_schoolacc',
            'tn'=>$tn,
            'ref_no'=>$ref_no,
            'title'=>$title,
            'action'=>$action,
            'remarks'=>$remarks,
            'status'=>$status,
            'sender'=>$sender,          
            'cname'=>$cname,
            'doc_type'=>$doc_type,
            'route'=>$route,
            'date'=>$date,
            'time'=>$time,
            'user'=>$user,      
                                    
            ]
        ]);
      //return  $response;
        Alert::success('Record Successfully Added', 'Record Save'); 
            return redirect()->route('dashboard');   
}
//add remarks
public function addRemarks(Request $request){
    $id = $request['id'];
    $remarks=$request['remarks'];

    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'addremarks',
        'id'=> $id,
        'remarks'=>$remarks,
    
        ]
    ]);
    //return $response;
    Alert::success('Remarks successfully added!', 'Record Saved');
    //return redirect()->route('transparency/req_quotation');
}

//accept pending ticket
public function AcceptTicket(Request $request){
    $id = $request['id'];
    $date=$request['date'];
    $time=$request['time'];
    $action=$request['action'];
    $lname = Session::get('Lastname');
    $fname = Session::get('Firstname');
    $user = $fname.' '. $lname;

    //update
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'acceptticket',
        'id'=> $id,
        'action'=>$action,
        'date'=>$date,
        'time'=>$time,
        'user'=>$user,
        
        ]
    ]);

    //return $agent;
    Alert::success('', 'Ticket successfully accepted!');
    //return redirect()->route('transparency/req_quotation');
}

//update tn and doc-type
public function UpdateTN(Request $request){
    //insert
    $action1=''; //transferred
    $route1=$request['route'];
    $status1=$request['status'];
    $title1=$request['title'];
    $ref_no1=$request['ref_no'];
    $sender1=$request['sender'];
    $cname1=$request['cname'];
    $remarks1=$request['remarks'];
    $user=$request['user'];
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $time = date('H:i:s');
    //update
    $id_up = $request['id'];
    $tn_up=$request['tn'];
    $doc_type_up=$request['doc_type'];
    $action_up=$request['action']; //received
    $user=$request['user'];
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $time = date('H:i:s');

    //update
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'updateblanktn', 
        'id'=> $id_up,
        'tn'=>$tn_up,
        'doc_type'=>$doc_type_up,
        'action'=>$action_up,
        'date'=>$date,
        'time'=>$time,
        'user'=>$user, 
        ]
    ]);

    //insert blank ACTION
    $client1 = new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response_insert = $client1->request('POST', $url, [
    'form_params'=>[
        'tag'=>'transferticketfromschool',
        'tn'=>$tn_up,
        'ref_no'=>$ref_no1,
        'title'=>$title1,
        'action'=>$action1,
        'remarks'=>$remarks1,
        'status'=>$status1,
        'sender'=>$sender1,
        'cname'=>$cname1,
        'doc_type'=>$doc_type_up,
        'route'=>$route1,
        'date'=>$date,
        'time'=>$time,
        'user'=>$user,
    ]
]);
    //return $response_insert;
    //Alert::success('', 'Ticket successfully updated!');
    //return redirect()->route('transparency/req_quotation');
}

//search key words
public function search(Request $request){
    $search = $request['txtsearch_tickettn'];
         //connection to guzzle
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
   $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'searchticket',  
        'tn'=>$search,
        'title'=>$search,
        ]
    ]);
    
    $data=json_decode($response->getBody()->getContents(), true);
    //return $response;
    //return $data;
    return view ('search', ['searchticket'=>$data]);
    //return $data;
    }

//search hd ticket keywords
public function searchHD(Request $request){
    $searchhd = $request['txtsearch_hd'];
    //connection to guzzle
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
   $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'shelpdesk',  
        'category'=>$searchhd,
        'remarks'=>$searchhd,
        
        ]
    ]);
    $data_hd = json_decode($response->getBody()->getContents(), true);
    if($data_hd==''){
        Alert::info('No records found!');
    }
    else if($data_hd!=''){
        return view('searchhd', ['searchHD'=>$data_hd]);
    }
}

//search exact match
public function searchexactticket(Request $request){
    $search = $request['txtsearch_ticket'];
         //connection to guzzle
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
   $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'searchexactticket',  
        'tn'=>$search,

        ]
    ]);
    
    $data=json_decode($response->getBody()->getContents(), true);
    return $data;

    }


//ticket details
public function ticketdetails(Request $request){
    $tn = $request['tn'];
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response_nt = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'selectthesametn',
        'tn'=> $tn,
        ]
    ]);
    
    $data=json_decode($response_nt->getBody()->getContents(), true);
    //return $response_nt;
    return view('ticket', ['ticket'=>$data]);
        //return $data;
}
//reference number
public function ReferenceNumber(Request $request){
    $ref_no = $request['ref_no'];
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response_rn = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'selectrefno',
        'ref_no'=>$ref_no,
        ]
    ]);
    
    $data=json_decode($response_rn->getBody()->getContents(), true);
    //return $response_nt;
    return view('reference', ['refno'=>$data]);
        //return $data;
}
//my ticket
public function myticket(){
    if (session('UserName')==null){
        Alert::info('Please Login!');
        return redirect()->route('index');
    }
    else{
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'myticket',  
        "department"=>session("Department"),
        ]
    ]);
    
    $data=json_decode($response->getBody()->getContents(), true);
    //return $data;
    return view ('mytickets', ['mytickets'=>$data]);
    //return view ('closed_tickets', ['closedst'=>$data]);
    }
}

//help desk
public function hd(){
    //getseries_id(modal)
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response1 = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'getticket',
        //'id'=> $id,
        ]
    ]);

//helpdesk ticket
$client= new \GuzzleHttp\Client();
$url = WEBSERVICE_URL;
$response_hd = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'getallhdticketsunit',
        "department"=>session("Department"),
    ]
]);
    $data_hd=json_decode($response_hd->getBody()->getContents(), true);
    $data1=json_decode($response1->getBody()->getContents(), true);
    return view('helpdesk', ['getid'=>$data1, 'hdticket'=>$data_hd]);       
}

//knowledgebase
public function kb(){
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response1 = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'getticket',
        //'id'=> $id,
        ]
    ]);
    $data1=json_decode($response1->getBody()->getContents(), true);
    return view('knowledgebase', ['getid'=>$data1]);       
}

//helpdesk action
public function helpdeskaction(Request $request){
    $lname = Session::get('Lastname');
    $fname = Session::get('Firstname');
    $name= $fname.' '.$lname;
    $username = Session::get('UserName');
    $dept = Session::get('Department');
    $remarks=$request['txtremarks'];
    $category = $request['txtcategory'];
    $status = 'Active';
    
    date_default_timezone_set('Asia/Manila');
    $date_time = date('Y-m-d H:i:s');
    $hd_ticket = date('ymdhis');
    //connection to guzzle
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'helpdesk',
            'hd_ticket'=>$hd_ticket,
            'name'=>$name,
            'email'=>$email,
            'department'=>$dept,
            'remarks'=>$remarks,
            'category'=>$category,
            'date_time'=>$date_time,
            'status'=>$status,
                                                  
            ]
        ]);
       
      //return $response;
       // Alert::success('Record Successfully Added', 'Record Save'); 
       return redirect()->route('helpdesk');      
}

//help desk logs
public function helpdesklogs(){
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'helpdesklogs',  
        ]
    ]);
    
    $logs=json_decode($response->getBody()->getContents(), true);
    //return $logs;
    return view ('helpdesk_logs', ['hdlogs'=>$logs]);
    }

//search ticket    
public function search_trail(Request $request){
    $search = $request['txtsearch_tickettn'];
   //connection to guzzle
$client= new \GuzzleHttp\Client();
$url = WEBSERVICE_URL;
$response = $client->request('POST', $url, [
'form_params'=>[
  'tag'=>'searchticket_trail',  
  'tn'=>$search,
  ]
]);

$data= json_decode($response->getBody()->getContents(),true);
return $data;
}

//search trail ticket (index)   
public function search_trailI(Request $request){
    $search = $request['txtsearch_ticket'];
   //connection to guzzle
$client= new \GuzzleHttp\Client();
$url = WEBSERVICE_URL;
$response = $client->request('POST', $url, [
'form_params'=>[
  'tag'=>'searchticket_trail',  
  'tn'=>$search,
  ]
]);

$data= json_decode($response->getBody()->getContents(),true);
return $data;
}
//ticket from other office
public function TicketsFOO(Request $request){

    $client=new \GuzzleHttp\Client();
$url = WEBSERVICE_URL;
$response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'ticketsfoo',
        'department'=>session("Department"),
    ]
]);

$TFOO = json_decode($response->getBody()->getContents(), true);
return view ('mytickets', ['ticketsFOO'=>$TFOO]);

}

//change principal name
public function changeprincipalname(Request $request){
    $prin_name = $request['txtprincipal'];
    //$title=$request['title'];
   // $origin=$request['origin'];
    //$cname=$request['cname'];

    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'updateprincipal',
        'principal'=> $prin_name,
        "UserName"=>session("UserName"),
    
        ]
    ]);
    //return $response;
    Alert::success('Record Updated Successfully!', 'Record Save');
    return redirect()->route('dashboard');
}
//transfer one ticket
public function Transfer_Ticket(Request $request){
    $id = $request['id'];
    $action = 'Transferred';
    $route=$request['route'];
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $time = date('H:i:s');

    $lname = Session::get('Lastname');
    $fname = Session::get('Firstname');
    $user = $fname.' '. $lname;

//transferred status
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'transferticket',
        'id'=>$id,
        'action'=>$action,
        'route'=>$route,
        'date'=>$date,
        'time'=>$time,
        'user'=>$user,
        
        ]
    ]);
    //return $response; 
    //Alert::success('', 'Record Save. Ticket successfully accepted!');
    //return redirect()->route('dashboard');
}

//pending transfer status
public function PendingTransfer(Request $request){
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $time = date('H:i:s');
    $tn = $request['tn'];
    $ref_no = $request['ref_no'];
    $title = $request['title'];
    $remarks=$request['remarks'];
    $status = $request['status'];
    $sender = $request['sender'];  
    $action = 'Pending Transfer';  
    $cname = $request['cname']; 
    $doc_type = $request['doc_type']; 
    $route = $request['route']; 
    //$route1 = Session::get('Department');
    $lname = Session::get('Lastname');
    $fname = Session::get('Firstname');
    $user = $fname.' '. $lname;
    //$agent = Session::get('Firstname');
    //connection to guzzle
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'pendingtransfer',
            'tn'=>$tn,
            'ref_no'=>$ref_no,
            'title'=>$title,
            'action'=>$action,
            'remarks'=>$remarks,
            'status'=>$status,
            'sender'=>$sender,          
            'cname'=>$cname,
            'doc_type'=>$doc_type,
            'route'=>$route,
            'date'=>$date,
            'time'=>$time,
            'user'=>$user,                                          
            ]
        ]);
      //return  $response;
        Alert::success('Record Successfully Added', 'Record Save'); 
        //if(Session::get('Department')=='Records'){
        //return redirect()->route('created_ticket');   
        //}
        //else{
            return redirect()->route('dashboard');   
        //}    
}
//add document trail
public function AddDocumentTrail(Request $request){
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $time = date('H:i:s');
    $tn = $request['txttn'];
    $ref_no = $request['txtref_no'];
    $title = $request['txttitle'];
    $remarks=$request['txtremarks'];
    $status = $request['txtstatus'];
    $sender = $request['txtsender'];  
    $action = $request['txtaction'];  
    $cname = $request['txtcname']; 
    $doc_type = $request['txtdoc_type']; 
    $route = $request['txtroute']; 
    $route1 = Session::get('Department');
    $lname = Session::get('Lastname');
    $fname = Session::get('Firstname');
    $user = $fname.' '. $lname;
    //$agent = Session::get('Firstname');
    //connection to guzzle
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'adddocumenttrail',
            'tn'=>$tn,
            'ref_no'=>$ref_no,
            'title'=>$title,
            'action'=>$action,
            'remarks'=>$remarks,
            'status'=>$status,
            'sender'=>$sender,          
            'cname'=>$cname,
            'doc_type'=>$doc_type,
            'route'=>$route,
            'date'=>$date,
            'time'=>$time,
            'user'=>$user,      
                                    
            ]
        ]);
       
      //return  $response;
        Alert::success('', 'Record Save'); 
        if(Session::get('Department')=='Records'){
        return redirect()->route('outgoing_tickets');   
        }
        else{
            return redirect()->route('dashboard');   
        }    
}
//add document trail (ticket from school)
public function AddDocumentTrailSA(Request $request){
    date_default_timezone_set('Asia/Manila');
    $date1 = date('Y-m-d');
    $time1 = date('H:i:s');
    $date = $request['date'];
    $time = $request['time'];
    $tn = $request['txttn'];
    $ref_no = $request['txtref_no'];
    $title = $request['txttitle'];
    $remarks=$request['txtremarks'];
    $remarks1=$request['remarks'];
    $status = $request['txtstatus'];
    $sender = $request['txtsender'];  
    $action = $request['txtaction'];
    $action1 = $request['action'];  
    $cname = $request['txtcname']; 
    $doc_type = $request['txtdoc_type']; 
    $route = $request['txtroute']; 
    $route1 = $request['route'];
    //$route1 = Session::get('Department');
    $lname = Session::get('Lastname');
    $fname = Session::get('Firstname');
    $user = $fname.' '. $lname;
    $user1 = $request['user'];
    //$agent = Session::get('Firstname');

//update blank tn
$client=new \GuzzleHttp\Client();
$url = WEBSERVICE_URL;
$response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'add_document_trailSA',
            'tn'=>$tn,
            'ref_no'=>$ref_no,
            'title'=>$title,
            'action'=>$action1,
            'remarks'=>$remarks1,
            'status'=>$status,
            'sender'=>$sender,          
            'cname'=>$cname,
            'doc_type'=>$doc_type,
            'route'=>$route1,
            'date'=>$date,
            'time'=>$time,
            'user'=>$user1,    
    ]
]);
    //connection to guzzle (insert)
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $response = $client->request('POST', $url, [
        'form_params'=>[
            'tag'=>'add_document_trailSA',
            'tn'=>$tn,
            'ref_no'=>$ref_no,
            'title'=>$title,
            'action'=>$action,
            'remarks'=>$remarks,
            'status'=>$status,
            'sender'=>$sender,          
            'cname'=>$cname,
            'doc_type'=>$doc_type,
            'route'=>$route,
            'date'=>$date1,
            'time'=>$time1,
            'user'=>$user,      
                                    
            ]
        ]);
   
      //return  $date1;
        Alert::success('', 'Record Save'); 
        //if(Session::get('Department')=='Records'){
        //return redirect()->route('created_ticket');   
        //}
        //else{
            return redirect()->route('dashboard');   
        //}    
}

//incoming tickets
public function IncomingTickets(){
    if (session('UserName')==null){
        Alert::info('Please Login!');
        return redirect()->route('index');
    }
    else{
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'incomingtickets',  
        //"department"=>session("Department"),
        ]
    ]);
    
    $data=json_decode($response->getBody()->getContents(), true);
    //return $data;
    return view ('incoming_tickets', ['incomingT'=>$data]);
    //return view ('closed_tickets', ['closedst'=>$data]);
    }
}

//outgoing tickets
public function OutgoingTickets(){
    if (session('UserName')==null){
        Alert::info('Please Login!');
        return redirect()->route('index');
    }
    else{
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'outgoingtickets',  
       // "department"=>session("Department"),
        ]
    ]);
    
    $data=json_decode($response->getBody()->getContents(), true);
    //return $data;
    return view ('outgoing_tickets', ['outgoingT'=>$data]);
    //return view ('closed_tickets', ['closedst'=>$data]);
    }
}
//my tickets (created by school)
public function SchoolTickets(){
    if (session('UserName')==null){
        Alert::info('Please Login!');
        return redirect()->route('index');
    }
    else{
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'SchoolTickets',  
        "department"=>session("Department"),
        ]
    ]);
    
    $data=json_decode($response->getBody()->getContents(), true);
    //return $data;
    return view ('school_tickets', ['schooltkts'=>$data]);
    //return view ('closed_tickets', ['closedst'=>$data]);
    }
}
//delete ticket thesame tn
public function deletedticket(Request $request){

    $tn = $request['txttn'];
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'deleteticket',
        'tn'=> $tn,
        ]
    ]);
    //return $response;
    //Alert::success('Record Successfully Deleted', 'Record Save');
    //return redirect()->route('transparency/req_quotation');
}

//update thesame tracking number
public function UpdatethesameTN(Request $request){
    $tn = $request['tn'];
    $tn_local=$request['tn_local'];
    $title = $request['title'];
    
    //update
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'updatethesametn',
        'tn'=> $tn,
        'tn_local'=>$tn_local,
        'title'=>$title,
        
        ]
    ]);
    //return $tn_local;
   // Alert::success('', 'Ticket successfully accepted!');
    //return redirect()->route('dashboard');
}

//update page
public function UPDATE(){
    if (session('UserName')==null){
        Alert::info('Please Login!');
        return redirect()->route('index');
    }
    else{
        $client=new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $recent = $client->request('POST', $url, [
            'form_params'=>[
                'tag'=>'recenttickets',
                'department'=>session("Department"),
            ]
        ]);
    $recent_tickets = json_decode($recent->getBody()->getContents(), true);
    $type=array('Admin', 'Finance', 'Legal', 'Personnel', 'Private');
        //return $recent;
   return view ('update_ticket', ['recent_t'=>$recent_tickets, 'doc_type'=>$type]);
    }
}
//open tickets
public function OPENTICKETS(){
    if (session('UserName')==null){
        Alert::info('Please Login!');
        return redirect()->route('index');
    }
    else{

        //count open tickets per office
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $count = $client->request('POST', $url, [
        'form_params'=>[
        'tag'=>'openticketssds', 
        ]]);

    $count_tick = json_decode($count->getBody()->getContents(),true);
    //$open_tickets = json_decode($open->getBody()->getContents(), true);
        //return $count_tick;
   return view ('admin', ['countopentkts'=>$count_tick]);
    }
}
//list of open tickets
public function LISTOPENTICKETS(Request $request){
    $office=$request['route'];
    if (session('UserName')==null){
        Alert::info('Please Login!');
        return redirect()->route('index');
    }
    else{

        //count open tickets per office
        $client= new \GuzzleHttp\Client();
        $url = WEBSERVICE_URL;
        $list_count = $client->request('POST', $url, [
        'form_params'=>[
        'tag'=>'opentickets', 
        'route'=>$office 
        ]]);

    $listoopt = json_decode($list_count->getBody()->getContents(),true);
     //$listoopt = $list_count->getBody()->getContents();
        //return $list_count;
   return view ('open_tickets', ['listopen_tckts'=>$listoopt]);
    }
}
//delete ticket not submitted from school
public function deletedticketfromschool(Request $request){

    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'autodeletetickets',
        ]
    ]);
    return $response;
    Alert::success('Record Successfully Deleted', 'Record Save');
    return redirect()->route('incoming_tickets');
}

} //end of class
?>


