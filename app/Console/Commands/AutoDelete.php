<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class AutoDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blanktn:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    //write your auto delete code
    $client= new \GuzzleHttp\Client();
    $url = WEBSERVICE_URL;
    $response = $client->request('POST', $url, [
    'form_params'=>[
        'tag'=>'autodeletetickets',
        ]
    ]);
    //return $response;
}