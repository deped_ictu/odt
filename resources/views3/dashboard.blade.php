@extends('layouts.master')



@section ('title')

    DepEd | Marikina DTS | Login

@endsection



@section('content')  

<script>
window.location.replace('http://localhost:8000/index');
</script>

		<div class="row">

			<div class="col-md-12">

				<h2>Dashboard</h2>

			</div>

		</div>



		<div class="row">	

		@if(Session::get('Department')=='Records' || Session::get('Department')=='ICTU')



			@include('inc/dashboard_records')


		@elseif(Session::get('Department')=='SDS')

			@include('inc/sds_admin')



		@elseif (Session::get('Department')=='CID' || Session::get('Department')=='Finance' || Session::get('Department')=='Legal' || Session::get('Department')=='SGOD' || Session::get('Department')=='Personnel' || Session::get('Department')=='Health' || Session::get('Department')=='Admin' || Session::get('Department')=='Cash' || Session::get('Department')=='Supply' || Session::get('Department')=='PSDS')



			@include('inc/dashboard_user') 



		@else

			@include('inc/dashboard_school')



		@endif

		</div>



@endsection