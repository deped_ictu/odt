@extends('layouts.master_modal')
@section('content')
		  
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">
					
						<h3>Pending Tickets route to {{ Session::get('Department') }} office</h3>
					</div>
					<div class="panel-body">
					<table id="assigned_ticket" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th width="30">ID</th>
							<th width="100">TN</th>
							<th width="60">Title</th>
							<th width="30">Accept</th>
						</tr>
					</thead>
					<tbody>

					@foreach($pending as $pending)
					
						@if(Session::get('Email')=='sds@yahoo.com' && $pending['doc_type']=='Admin')
								<tr>     
									<td>{{ $pending['id'] }}</td>     
										@if($pending['status_2']=='Priority')
										<td style="background-color:#ffcccc; color:#000000; font-family:monospace;text-transform:uppercase;">{{ $pending['tn'] }}</td>
										@else 
										<td style="font-family:monospace;text-transform:uppercase;">{{ $pending['tn'] }}</td>
										@endif
										<td>{{ $pending['title'] }}</td>
									<td><button class='btn btn-primary btn_accept'  data-id="{{$pending['id']}}" data-tn="{{$pending['tn']}}"><span class="glyphicon glyphicon glyphicon-ok"></span></button></td>
									<input type="hidden" id="id" name="txtid"/>
										
										<!--add remarks-->
										
								</tr>

						@elseif( Session::get('Email')=='janievid.gabute@deped.gov.ph' && $pending['doc_type']=='Personnel')
								<tr>     
									<td>{{ $pending['id'] }}</td>     
										@if($pending['status_2']=='Priority')
										<td style="background-color:#ffcccc; color:#000000; font-family:monospace;text-transform:uppercase;">{{ $pending['tn'] }}</td>
										@else 
										<td style="font-family:monospace;text-transform:uppercase;">{{ $pending['tn'] }}</td>
										@endif
										<td>{{ $pending['title'] }}</td>
									<td><button class='btn btn-primary btn_accept'  data-id="{{$pending['id']}}" data-tn="{{$pending['tn']}}"><span class="glyphicon glyphicon glyphicon-ok"></span></button></td>
									<input type="hidden" id="id" name="txtid"/>
										
										<!--add remarks-->
										
								</tr>
						@elseif( Session::get('Email')=='camilleeunice.carmona001@deped.gov.ph' && $pending['doc_type']=='Legal') 
								<tr>     
									<td>{{ $pending['id'] }}</td>     
										@if($pending['status_2']=='Priority')
										<td style="background-color:#ffcccc; color:#000000; font-family:monospace;text-transform:uppercase;">{{ $pending['tn'] }}</td>
										@else 
										<td style="font-family:monospace;text-transform:uppercase;">{{ $pending['tn'] }}</td>
										@endif
										<td>{{ $pending['title'] }}</td>
									<td><button class='btn btn-primary btn_accept'  data-id="{{$pending['id']}}" data-tn="{{$pending['tn']}}"><span class="glyphicon glyphicon glyphicon-ok"></span></button></td>
									<input type="hidden" id="id" name="txtid"/>
										
										<!--add remarks-->
										
								</tr>

						 @elseif( Session::get('Email')=='seal.marcellana@deped.gov.ph' && $pending['doc_type']=='Finance') 
						 		<tr>     
									<td>{{ $pending['id'] }}</td>     
										@if($pending['status_2']=='Priority')
										<td style="background-color:#ffcccc; color:#000000; font-family:monospace;text-transform:uppercase;">{{ $pending['tn'] }}</td>
										@else 
										<td syle="font-family:monospace;text-transform:uppercase;">{{ $pending['tn'] }}</td>
										@endif
										<td>{{ $pending['title'] }}</td>
									<td><button class='btn btn-primary btn_accept'  data-id="{{$pending['id']}}" data-tn="{{$pending['tn']}}"><span class="glyphicon glyphicon glyphicon-ok"></span></button></td>
									<input type="hidden" id="id" name="txtid"/>
										
										<!--add remarks-->
										
								</tr>

						@else
								<tr>     
									<td>{{ $pending['id'] }}</td>     
										@if($pending['status_2']=='Priority')
										<td style="background-color:#ffcccc; color:#000000; font-family:monospace;text-transform:uppercase;">{{ $pending['tn'] }}</td>
										@else 
										<td style="font-family:monospace;text-transform:uppercase;">{{ $pending['tn'] }}</td>
										@endif
										<td>{{ $pending['title'] }}</td>
									<td><button class='btn btn-primary btn_accept'  data-id="{{$pending['id']}}" data-tn="{{$pending['tn']}}"><span class="glyphicon glyphicon glyphicon-ok"></span></button></td>
									<input type="hidden" id="id" name="txtid"/>
										
										<!--add remarks-->
										
								</tr>
						@endif
					@endforeach
					</tbody>  
					</table>
				</div>
			</div>
		</div>

<script>
	var acceptTicketURL="{{route('accept_ticket')}}";
</script>

@endsection