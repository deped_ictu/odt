@extends('layouts.master_modal')

@section('content')
<div class="col-sm-12">
  <table id="overdue_ticket" class="table table-striped table-bordered">
  <thead>
      <tr>
          
          <th width="30">TN</th>
          <th>Title</th>
          
          <th width="60">Date/Time</th>
          
          <th width="50" id="action">Actions</th>
      </tr>
  </thead>
  <tbody>
      <?php
      $datacount = 0;
      $overduecount = 0;
      ?>
      @foreach($ticket as $data)						
          @if ($data['route']==Session::get('Department') )
              @if ($data['status']!='Transferred' && $data['status']!='Closed' && $data['status']!='Pending Submission' && $data['status']!='Released')
                  <?php
                    //VIRMIL ADDED CODE >>>>>>>> OVERDUE DETECTOR
                    $datacount ++;
                    $ticketnumber = $data['tn'];
                    $fetched_document_day = strtotime($data['date_time']);
                    $is_overdue = false;
                    $gotdate = $data['date_time'];
                    
                    //SNIPPED FROM OLA v2
                    
                    //Compute All Serving Days
                    $NumberOfDocumentDays = 0;
            
                    //Current Month Integers
                    $month = date("m",$fetched_document_day);
                    $list = "";
                    $lmonth = date("m",$fetched_document_day) -1;
                    $year = date("Y",$fetched_document_day); 
                
                    for($d = date("d",$fetched_document_day); $d <= 31;$d++){
                        $time=mktime(12, 0, 0, $month, $d, $year);
                        if(date('m',$time) == $month){
                            if(date("l",$time) != "Saturday" && date("l",$time) != "Sunday"){
                                if($d <= date("d")){
                                        $NumberOfDocumentDays += 1;
                                }
                            }
                        }
                    }
                    //echo $NumberOfDocumentDays;
                    if($NumberOfDocumentDays > 5){
                        $is_overdue = true;
                        $overduecount +=1;
                    }
                    //END OF SNIP
                  ?>
                    
                      <?php          
                      $imaglink = "<img class=popimg src=https://media.giphy.com/media/qjqUcgIyRjsl2/source.gif><br>";
                  if($is_overdue == true){
                      // OVERDUE
                      ?>
                    <tr> 
                      <td style='background-color: #E57373;'>
                      <strong style='color:white; text-shadow: 0px 1px 1px rgba(0,0,0,0.2);'>
                      <a href='#' class='btnaddinfo' style='color:white;' data-dayscount="{{$NumberOfDocumentDays}}"
                      data-location="{{ $data['route'] }}"
                      data-remarks="{{ $data['remarks'] }}"
                      data-status="{{ $data['status'] }}"
                      data-toggle="modal"
                      data-target="#datamodal"> {{ $ticketnumber }}</a>
                      </strong></span></td>
                      <td>{{ $data['title'] }}</td>
                      
                      <td>{{ $data['date_time'] }} - {{ $data['time'] }}</td>
                      <td>
                      @if($data['status']!='Pending Transfer' && $data['route']==Session::get('Department'))
                      <div class="dropdown dropleft">
                                  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action
                                  <span class="caret"></span></button>
                                  <ul class="dropdown-menu dropdown-menu-left" style="z-index:99999">
                                  <!--user accounts-->                                    
                                  
                                      <li><a href="" class='btnadd_remarks'  data-id="{{$data['id']}}" data-remarks="{{$data['remarks']}}" data-tn="{{$data['tn']}}" data-agent="{{$data['agent']}}" data-toggle="modal" data-target="#add_remarks">Add Remarks</a></li>
                                      
                                      <li><a href="#" class="closed_ticket" data-id="{{$data['id']}}" data-title="{{$data['title']}}" data-status="{{$data['status']}}" data-remarks="{{$data['remarks']}}" data-tn="{{$data['tn']}}" data-agent="{{$data['agent']}}" data-status2="{{$data['status_2']}}" data-email="{{$data['email']}}" data-origin="{{$data['origin']}}" data-cname="{{$data['cname']}}" data-doctype="{{$data['doc_type']}}" data-route="{{$data['route']}}" data-date="{{$data['date_time']}}" data-time="{{$data['time']}}" data-dateout="{{$data['date_out']}}" data-timeout="{{$data['time_out']}}" data-toggle="modal" data-target="#close_ticket">Close Ticket</a></li>

                                      <li><a href="#" class="transfer_ticket" a href="#transfer_ticket" data-toggle="modal" data-target="#transfer_ticket" data-id="{{$data['id']}}" data-title="{{$data['title']}}" data-status="{{$data['status']}}" data-remarks="{{$data['remarks']}}" data-tn="{{$data['tn']}}" data-agent="{{$data['agent']}}" data-status2="{{$data['status_2']}}" data-email="{{$data['email']}}" data-origin="{{$data['origin']}}" data-cname="{{$data['cname']}}" data-doctype="{{$data['doc_type']}}" data-route="{{$data['route']}}" data-date="{{$data['date_time']}}" data-time="{{$data['time']}}" data-dateout="{{$data['date_out']}}" data-timeout="{{$data['time_out']}}">Transfer Ticket</a></li>
                                      
                                      @if($data['status_2']!='')
                                      @else
                                      <li><a href="#" class="btndelete_ticket" id="delete_ticket" data-id="{{$data['id']}}">Delete</a></li>
                                      @endif
                                      
                      @else
                                  
                                  <button class="btn btn-primary" disabled="">Select Action <span class="caret"></span></button>
                                  
                      @endif
                                      
                                  </ul>
                          </div>
                    </td>
                    </tr>
                      <?php
                  }
                  echo "<script>
                  
                  $('#LabelOpen').html($datacount);
                  $('#LabelOverdue').html($overduecount);
                  </script>";
                  //VIRMIL ADDED CODE >>>>>>>> OVERDUE DETECTOR
                  ?>
    
                 
              @endif
          @endif
      @endforeach
  </tbody>  
  </table>



</div>





<script>
//For dashborad popover

$(".btnaddinfo").click(function(){
var daysindepartment = $(this).data("dayscount");
var Loc = $(this).data("location");
var Rem = $(this).data("remarks");
var Stat = $(this).data("status");
if(Loc == "" && Loc == null){ Loc="(empty)";}
if(Rem.length ==0 ){ Rem="(empty)";}
if(Stat == "" && Stat == null){ Stat="(empty)";}
$("#d1").html(Loc);
$("#d2").html(Rem);
$("#d3").html(Stat); 

if(daysindepartment > 5){
$("#overduepanel").css("display","block");
}else{
$("#overduepanel").css("display","none");
}
$("#d0").html(daysindepartment); 

});


var closedTicketURL="{{route('closed_ticket')}}";
var submitTicketURL="{{route('submit_ticket')}}";

</script>

