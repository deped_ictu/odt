@extends('layouts.master')
@section ('title')
    DepEd | Marikina DTS | Print Todays Logs
@endsection

@section('content') 

<div class="container" id="print_records">
<h3>DTS Logs 
(<?php $d = date("Y-m-d"); 
echo date("F j, Y", strtotime("$d"));
?>)
</h3>
<br/>
<!--
<div class="col-md-2">
<input type="text" name="From" id="From" class="form-control print-hidden" placeholder="From Date"/>
</div>
<div class="col-md-2">
<input type="text" name="to" id="to" class="form-control print-hidden" placeholder="To Date"/>
</div>
-->
<div class="col-md-8">
<!--<input type="button" name="range" id="range" value="Search" class="btn btn-success print-hidden"/>-->
<input type="button" name="print" id="print" value="Print" class="btn btn-primary print-hidden" onClick="window.print()" />
</div>

<div class="clearfix"></div>
<br/>
<div>

<table id="p_records" class="table table-bordered">
<tr>
<th width="20%">Tracking Number</th>
<th>Title</th>
<th width="20%">From</th>
<th width="10%">Time</th>
</tr>
@foreach($logs as $data)
<tr>
    <td style="font-family:monospace;text-transform:uppercase;">{{$data['tn']}}</td>
    <td>{{$data['title']}}</td>
    <td>{{$data['origin']}} - {{$data['cname']}}</td>
    <td>{{$data['date_time']}} - {{$data['time']}}</td>

</tr>
@endforeach

</table>
</div>
</div>
<!-- Script -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<script>
$(document).ready(function(){
	$.datepicker.setDefaults({
		dateFormat: 'yy-mm-dd'
	});
	$(function(){
		$("#From").datepicker();
		$("#to").datepicker();
	});
	$('#range').click(function(){
		var From = $('#From').val();
		var to = $('#to').val();
		if(From != '' && to != '')
		{
			$.ajax({
				url:"/inc/fetch",
				method:"POST",
				data:{From:From, to:to},
				success:function(data)
				{
					$('#print_records').html(data);
				}
			});
		}
		else
		{
			alert("Please Select the Date");
		}
	});
});
</script>

@endsection