@extends('layouts.master')

@section ('title')
    DepEd | Marikina DTS | Login
@endsection

@section('content')  

	<div class="row">
		<div class="col-md-12">
			<h2>Dashboard</h2>
		</div>
	</div>
		
    <div class="row">
        <div class="col-sm-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <!--Search-->
                    <!--<form class="navbar-form navbar-left" action="{{route('search')}}" autocomplete="off" method="POST">-->
                    {!! csrf_field() !!}
                    <div class="input-group input-group-lg">
                        <input type="text" id="search" class="form-control" name="txtsearch_ticket" placeholder="" required/>
                        <span class="input-group-btn">
                            <button data-toggle="modal" data-target="#searchmodal" class="btn btn-default" id="btnsearch">GO</button>
                        </span>
                    </div>
                <!--</form>-->
                <script>
                   $("#btnsearch").click(function(){
                      var searchkeyword = $("#search").val();
                      //alert(searchkeyword);
                      $.ajax({
                          type: "POST",
                          url: "{{route('search')}}",
                          data: {
                              _token:"{!! csrf_token() !!}",
                              txtsearch_ticket:searchkeyword},
                          success: function(data){
                              $("#ticketnumbersearched").html(searchkeyword + "<br>");
                              $("#doctitle").html("(not found)");
                              $("#off_origin").html("(not found)");
                              data = JSON.parse(data);
                              $("#search").val("");
                              //alert(JSON.stringify(data));
                             $("#searchresult").html("");
                             for(var i =0; i < data.length;i++){
                                 
                              $fdate = new Date(data[i]["date_time"]);
                              $fdate = $fdate.getDay() + "/" + $fdate.getMonth() + "/" + $fdate.getFullYear();
                              $("#doctitle").html(data[i]["title"].toUpperCase());
                              $("#searchresult").append("<tr><td>" + $fdate + " - " + data[i]["time"] + "</td>" + 
                              "<td>" + data[i]['route']  + "</td>" + 
                              "<td><div class='rem'>" + data[i]['remarks'] +"</div></td>" + 
                              "<td>" + data[i]['status'] +"</td>" + 
                              "</tr>");
                              if(i <= data.length){
                                  $("#off_origin").html(data[i]["origin"] + " (" + data[i]["cname"] + ")");
                              }
                             }
                          }
                      })
                  })
                </script>
            <!--End of Search-->	
        
                <?php
                $sdodepartments = array("SGOD","CID","ICTU","SDS","Finance","Admin","Supply","Cash", "Records","Personnel", "Health", "Legal", "PSDS", "ASDS");
                $Is_Sdo_account = false;
                if(in_array(Session::get('Department'),$sdodepartments)){
                    //this is a SDO account
                    $Is_Sdo_account = true;			
                }else{
                    //This is a School Account
                    $Is_Sdo_account = false;
                }
                ?>                   
                
            </div><!--end of panel-heading-->
            
            <div class="list-group">
                <a href="{{route('dashboard')}}" class="list-group-item">Assigned Ticket</a>
                <a href="#" class="list-group-item" data-toggle="modal" data-target="#create_ticket">Create Ticket</a>
                <a class="list-group-item" href="#" data-toggle="modal" data-target="#dynamicmodal">Pending Ticket</a>
                @if(Session::get('Department')=='Records')
                <a href="{{route('pending_tickets_fromschool')}}" class="list-group-item" data-toggle="modal" data-target="#">Clear Tickets</a>
                @else
                @endif
                <!--<a href="#" class="list-group-item" data-toggle="modal" data-target="#">Follow-up Ticket</a>-->
            </div>
            
        </div><!--end of panel-info-->
        
            @if($Is_Sdo_account==false)
            @else
            <table class="table table-striped table-bordered">
                <tr>
                    <th style="text-align:center;">Open</th>
                    <th style="text-align:center;">Overdue</th>
                </tr>
                <tr>
                    <td align="center" ><span id="LabelOpen"></span></td>
                    <td align="center"><span id='LabelOverdue'></span></td>
                </tr>
            </table>
            
            <!--EFFICIENCY COUNT-->
            <div class='panel panel-default' id="effpanel" style="background-image: linear-gradient(to right, #55efc4 , #00b894);">
              <div class='panel-body'> <center><h1 id='effrat' style='color:white; text-shadow: 0px 1px 2px rgba(0,0,0,0.5);'></h1>
              <h1 style='color:white; text-shadow: 0px 1px 2px rgba(0,0,0,0.5);' id='efficiencyrat'></h1>
              </center></div>
            </div>
            @endif
            
        </div><!--end of col-sm3-->
        
        <div class="col-sm-9">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4>Created Tickets by  {{ Session::get('Department') }} office</h4>
                </div>
                <div class="panel-body table-responsive">
                    <table id="" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="30">ID</th>
                                <th width="30">TN</th>
                                <th>Title</th>
                                <th width="60">From</th>
                                <th width="60">Date/Time</th>
                                <th width="50">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        <?php
                        $datacount = 0;
                        $overduecount = 0;
                            date_default_timezone_set('Asia/Manila');
                            $date_time = date('Y-m-d');
                            $time = date('H:i:s');
                        ?>
                            @foreach($createdticket as $data)						
                                @if ($data['route']==Session::get('Department') )
                                    @if ($data['status']!='Transferred' && $data['status']!='Closed' && $data['status']!='Pending Submission' && $data['status']!='Released' && $data['status']!='Pending Transfer')
                                        <tr>     
                                        <td>{{ $data['id'] }}</td>     
                                          <?php
                                          //VIRMIL ADDED CODE >>>>>>>> OVERDUE DETECTOR
                                          $datacount ++;
                                          $ticketnumber = $data['tn'];
                                          $fetched_document_day = strtotime($data['date_time']);
                                          $is_overdue = false;
                                          $gotdate = $data['date_time'];
                                          
                                          //SNIPPED FROM OLA v2
                                          
                                          //Compute All Serving Days
                                          $NumberOfDocumentDays = 0;
                                  
                                          //Current Month Integers
                                          $month = date("m",$fetched_document_day);
                                          $list = "";
                                          $lmonth = date("m",$fetched_document_day) -1;
                                          $year = date("Y",$fetched_document_day); 
                                      
                                          for($d = date("d",$fetched_document_day); $d <= 31;$d++){
                                              $time=mktime(12, 0, 0, $month, $d, $year);
                                              if(date('m',$time) == $month){
                                                  if(date("l",$time) != "Saturday" && date("l",$time) != "Sunday"){
                                                      if($d <= date("d")){
                                                              $NumberOfDocumentDays += 1;
                                                      }
                                                  }
                                              }
                                          }
                                          //echo $NumberOfDocumentDays;
                                          if($NumberOfDocumentDays > 5){
                                              $is_overdue = true;
                                              $overduecount +=1;
                                          }
                                          //END OF SNIP
                              
                                          $imaglink = "<img class=popimg src=https://media.giphy.com/media/qjqUcgIyRjsl2/source.gif><br>";
                                      if($is_overdue == true){
                                          // OVERDUE
                                          ?>
                                          
                                          <td style='background-color: #E57373; font-family:monospace;text-transform:uppercase;'>
                                          <strong style='color:white; text-shadow: 0px 1px 1px rgba(0,0,0,0.2);'>
                                          <a href='#' class='btnaddinfo' style='color:white;' data-dayscount="{{$NumberOfDocumentDays}}"
                                          data-location="{{ $data['route'] }}"
                                          data-remarks="{{ $data['remarks'] }}"
                                          data-status="{{ $data['status'] }}"
                                          data-toggle="modal"
                                          data-target="#datamodal"> {{ $ticketnumber }}</a>
                                          </strong></span></td>
                                         
                                          <?php
                                       
                                      }elseif($data['status_2']=='Priority'){
                                          // PRIORITY
                                          ?>
                                          
                                           <td style='background-color: #9C27B0; font-family:monospace;text-transform:uppercase;'>
                                          <strong style='color:white; text-shadow: 0px 1px 1px rgba(0,0,0,0.2);'>
                                          <a href='#' class='btnaddinfo' style='color:white;' data-dayscount="{{$NumberOfDocumentDays}}"
                                          data-location="{{ $data['route'] }}"
                                          data-remarks="{{ $data['remarks'] }}"
                                          data-status="{{ $data['status'] }}"
                                          data-toggle="modal"
                                          data-target="#datamodal"> {{ $ticketnumber }}</a>
                                          </strong></span></td>
                                          
                                          <?php
                                          
                                      }else{
                                          //DEFAULT
                                          ?>
                                          
                                          <td style='font-family:monospace;text-transform:uppercase;'>
                                          <span ><strong>
                                          <a href='#' class='btnaddinfo' data-toggle="modal" data-dayscount="{{$NumberOfDocumentDays}}"
                                          data-location="{{ $data['route'] }}"
                                          data-remarks="{{ $data['remarks'] }}"
                                          data-status="{{ $data['status'] }}"
                                          data-target="#datamodal">{{ $ticketnumber }}</a>
                                          </strong></span></td>
                                          
                                          <?php
                                      }
                                      echo "<script>
                                      
                                      $('#LabelOpen').html($datacount);
                                      $('#LabelOverdue').html($overduecount);
                                      </script>";
                                      //VIRMIL ADDED CODE >>>>>>>> OVERDUE DETECTOR
                                      ?>
                                  
                                      
                                          <td>{{ $data['title'] }}</td>
                                          <td>{{ $data['origin'] }}</td>
                                          <td>{{ $data['date_time'] }} - {{ $data['time'] }}</td>
                                          <td>@if($data['transfer_to']==''){{ $data['status'] }} @else {{$data['status']}} - {{$data['transfer_to']}} @endif</td>
                                         
                                      </tr>
                                  @endif
                              @endif
                          @endforeach
                        </tbody>  
                    </table>
                </div><!--end of panel-body-->
            </div><!--end of panel-info-->
        </div><!--end of col-sm-9-->
    </div>



<script>
//For dashborad popover

$(".btnaddinfo").click(function(){
var daysindepartment = $(this).data("dayscount");
var Loc = $(this).data("location");
var Rem = $(this).data("remarks");
var Stat = $(this).data("status");
if(Loc == "" && Loc == null){ Loc="(empty)";}
if(Rem.length ==0 ){ Rem="(empty)";}
if(Stat == "" && Stat == null){ Stat="(empty)";}
$("#d1").html(Loc);
$("#d2").html(Rem);
$("#d3").html(Stat); 

if(daysindepartment > 5){
$("#overduepanel").css("display","block");
}else{
$("#overduepanel").css("display","none");
}
$("#d0").html(daysindepartment); 

});


var closedTicketURL="{{route('closed_ticket')}}";
var submitTicketURL="{{route('submit_ticket')}}";

</script>

<script>

EfficiencyRating();
function EfficiencyRating(){
var open = $("#LabelOpen").html();
var overdue = $("#LabelOverdue").html();
var rating = open - overdue;
rating = rating / open * 100;
var n = rating.toFixed(2);
if(rating > 90){
$("#effrat").html('<i class="far fa-grin-stars"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
}else if(rating > 80){
$("#effpanel").css("background-image","linear-gradient(to right, #FFEB3B , #8BC34A)");
$("#effrat").html('<i class="far fa-smile"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
}else if(rating > 70){
$("#effrat").html('<i class="far fa-meh"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
}else if(rating > 50){
$("#effpanel").css("background-image","linear-gradient(to right, #F44336 , #FFC107)");
$("#effrat").html('<i class="far fa-frown"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
}else if(rating > 20){
$("#effrat").html('<i class="far fa-sad-tear"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
}

$("#efficiencyrat").html(n);
}     
setInterval(function()
{
EfficiencyRating();

}, 300);
</script>

@endsection