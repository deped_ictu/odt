<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>SDO Marikina City - Document Tracking System</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- Draggable -->

  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/JsBarcode.all.min.js"></script>
 
 <!-- end -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<style>

@media print {
    body, html {margin: 0px; }
	.print-buttons {display: none;}
	.letter, .a4 {display: none;}
  }

	h1, p {line-height: 100%;
}

.print-buttons {
	position: relative;
	top: 0;
	left: 0;
	float: right;
}

.fa {
  font-size: 30px;
  width: 50px;
  text-align: center;
  text-decoration: none;
}

.fa:hover {
    opacity: 0.7;
}

.glyphicon-print {
  background: #3B5998;
  color: white;
}

.glyphicon-print {
  padding: 10px;
  background: #3B5998;
  color: white;
}

.glyphicon-remove {
  padding: 10px;
  background: #cb2027;
  color: white;
}

.letter {
	position: absolute;
	top: 792pt;
	width: 100%;
	border: 2px dashed #ccc;
}

.a4 {
	position: absolute;
	top: 841pt;
	width: 100%;
	border: 2px dashed #ccc;
}

.well {
	min-height: 370px;
}

ul>li {padding-bottom: 2px; line-height: 55%;}
ul>li {font-size:24px;}
ul>li>span {font-size:11px; vertical-align:middle;}

</style>


<script src="js/jquery-1.7.2.min.js"></script>
<script>
$(document).ready(function() {
    $('#office1').keyup(function(e) {
        var txtVal = $(this).val();
        $('#office2').val(txtVal);
		$('#office3').val(txtVal);
    });
    
	   $('#person1').keyup(function(e) {
        var txtVal = $(this).val();
        $('#person2').val(txtVal);
		$('#person3').val(txtVal);
    });
	
    $('#office2').keyup(function(e) {
        var txtVal = $(this).val();
        $('#office1').val(txtVal);
    });
	
	 $('#office3').keyup(function(e) {
        var txtVal = $(this).val();
        $('#office2').val(txtVal);
    });
});
</script>

</style>
</head>
<body>
<div class="container-fluid">
    @yield('content')
</div>
</body>
</html>