<!DOCTYPE html>
<html lang="en">
	<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="csrf_token" content="{{ csrf_token()}}">

    <title>@yield('title')</title>
	
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
	
    <!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/transfer.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/assign.js')}}"></script>

     
     <!-- sweetalert -->
    <script type="text/javascript" src="{{asset('sweetalert-master/dist/sweetalert.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('sweetalert-master/dist/sweetalert.css')}}"></link>
	
    <!--<link rel="stylesheet" href="{{asset('css/stylesheet.css')}} " />-->
    <link rel="stylesheet" href="{{asset('css/login.css')}}" type="text/css" />

	</head>

	<body>
    <script type="text/javascript">
            var csrfToken = $('[name="csrf_token"]').attr('content');

            setInterval(refreshToken, 300000); // 5 minutes 

            function refreshToken(){
                $.get('refresh-csrf').done(function(data){
                    csrfToken = data; // the new token
                });
            }

            setInterval(refreshToken, 300000); // 5 minutes

    </script>

    @include('components.datatable')
    @include('sweet::alert')
    @include('components.modal')
    <div class="container-full clearfix">
    @yield('content')
    </div>
    
    </body>
    </html>