<!DOCTYPE html>
<html lang="en">
	<head>
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="csrf_token" content="{{ csrf_token()}}">
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
	<link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet">
    <!--print logs-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css"/>
    
    <script type="text/javascript" src="{{asset('js/main.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/transfer.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/assign.js')}}"></script>
     
     <!-- sweetalert -->
    <script type="text/javascript" src="{{asset('sweetalert-master/dist/sweetalert.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('sweetalert-master/dist/sweetalert.css')}}"></link>
	
    <!--<link rel="stylesheet" href="{{asset('css/stylesheet.css')}} " />-->
    <link rel="stylesheet" href="{{asset('css/login.css')}}" type="text/css" />
   
    <style>
    @media print
    {
         #print, #range, #from, #to {
            display: none;
        }
    
        a {
            display: none;
        }
    }
    
    .wrap {
        min-height: 750px; 
        padding-top: 15px; 
        background-color:#fff;
        /*border-top-left-radius: 6px;
        border-top-right-radius: 6px;*/
    }
    
    .footer {
        padding: 15px;
        background-color: #34495e;
        border-bottom-right-radius: 6px;
        border-bottom-left-radius: 6px;
        color: #fff;
    }
    </style>


	</head>

	<body style="background-color: #95a5a6">
    @include('components.header')
    <div class="container wrap">
    @include('components.datatable')
    @include('sweet::alert')
    @include('components.modal')
    @yield('content')
    </div>
    <div class="container footer">
        <div class="row">
            <div class="col-sm-12">
                Department of Education<br />
                National Capital Region<br />
                SCHOOLS DIVISION OFFICE - MARIKINA CITY
            </div>
        </div>
    </div>
    </body>
    </html>