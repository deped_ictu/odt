<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="The official website of the Schools Division of Marikina City">
<meta name="keywords" content="DepEd, Marikina, SDO, Education, Schools, NCR">
<meta name="author" content="Ryan Lee Regencia">

<title>@yield('title')</title>
<link rel="shortcut icon" type="image/x-icon" href="images/DO-logo.png" title=" Division of City Schools Marikina"/>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

  <link rel="stylesheet" href="{{asset('css/login.css')}}" type="text/css" />
  <link rel="stylesheet" href="{{asset('css/custom.css')}}" type="text/css" />

  <script>
function check_empty_fields()
{
var ValidateInput = true;

$(".validate #email,#password").each(function(){
    if ($.trim($(this).val()).length == 0){
        $(this).addClass("highlight");
        ValidateInput = false;
        $(this).focus();
    }
    else{
        $(this).removeClass("highlight");
    }
});

if (!ValidateInput) {
	$("#login-failed").fadeIn(200);
	$("#login-failed").text('Log in failed!');				
}
  return ValidateInput;  
 }
 </script>
</head>
<body>

<div class="container">
@yield('content')
</div>
</body>
</html>