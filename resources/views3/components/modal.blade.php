	<!-- CREATE TICKET FORM-->
	<div id="create_ticket" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h2 class="modal-title">Create a Ticket</h2>			
			</div>  
			<div class="modal-body">
				<form method="post" action="{{route('addticket')}}">		
				{!! csrf_field() !!}	
				<div class="form-login">
					<input type="hidden" id="tn" name="txttn" class="form-control input-lg chat-input" />          
					<div class="form-group">
						<label class="input-group">Sender:</label>
						<input type="text" id="origin" name="txtorigin" class="form-control input-lg chat-input" placeholder="First, Middle Name,  Last Name" required />
					</div>
					<div class="form-group">
						<label class="input-group">Email:</label>
						<input type="email" id="email" name="txtemail" value="@if(preg_match("/ES|HS/",Session::get('Department'))) {{Session::get('Email')}} @else @endif" class="form-control input-lg chat-input" placeholder="Email (Optional) for school email is required" />
					</div>
          
					<div class="form-group">
						<label class="input-group">Company Name:</label>
						<input type="text" id="cname" name="txtcname" value="@if(preg_match("/ES|HS/",Session::get('Department'))) {{Session::get('Department')}} @else @endif" class="form-control input-lg chat-input" placeholder="Company Name" required />
					</div>
					<div class="form-group">
						<label class="input-group">Title:</label>
						<input type="text" id="title" name="txttitle" class="form-control input-lg chat-input" placeholder="Title of document" required />
					</div>	
					<div class="form-group">				
						<label class="input-group">Remarks:</label>
						<textarea class="form-control" rows="5" id="comment" name="txtremarks"></textarea>
					</div>
				</div>
			</div>
			<div class="panel-footer clearfix">				   
				<button type="submit" class="btn btn-primary btn-lg pull-right" name="create_ticket" value="Submit">Submit</button>
			</div>
			</form>
		</div>
	</div>
	</div>	
	
<!-- Modal add remarks-->
<div class="modal fade" id="add_remarks" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" id="id" name="txtid"/>
		<div class="form-group form-inline">
            <h3>Update ticket number <input type="text" name="txtremarks" id="txttn" style="border:0px; background-color: transparent" /></h3>
        </div>
      </div>
      <div class="modal-body">
		<div class="form-group">
            <label for="add">&nbsp;<b>Add Remarks:</b></label>
            <textarea type="text" name = "txtremarks" id="addremarks" class="form-control"></textarea>				
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="add_rem">Update</button>
      </div>
    </div>
  </div>
</div>
<script>
    var updateURL="{{route('addremarks')}}";
	var acceptTicketUrl="{{route('accept_ticket')}}";
</script>

<!--edit ticket modal-->
<div class="modal fade" id="update_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Update Ticket</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <input type="text" name ="txtid" id="txtidv" />

            <div class="form-group">
                <label for="add">&nbsp;<b>Title</b>&nbsp;&nbsp;</label>
                <input type="text" name ="txttitle" id="txttitle" class="form-control" />
            </div>
						<div class="form-group">
                <label for="add">&nbsp;<b>From</b>&nbsp;&nbsp;</label>
                <input type="text" name = "txtorigin" id="txtorigin" class="form-control" />
            </div>
						<div class="form-group">
                <label for="add">&nbsp;<b>Company Name</b>&nbsp;&nbsp;</label>
                <input type="text" name = "txtcname" id="txtcname" class="form-control" />
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btnupdate">Update</button>
      </div>
    </div>
  </div>
</div>
<script>
    var UpdateTicketURL="{{route('update_ticket')}}";
    var DeleteURL="{{route('delete_ticket')}}";
</script>

<!-- Closed Ticket-->
<div class="modal fade" id="close_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Close Ticket</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <input type="hidden" id="id_close" name="txtid"/>
						
			<div class="form-group">
                <label for="add">&nbsp;<b>TN:</b>
                <input type="text" name = "txttn" id="txttn_close" class="form-control" style="border:0px;" />
				</label>
            </div>
		
			<div class="form-group">
                <label for="add">&nbsp;<b>Add Remarks:</b></label>
                <textarea type="text" name = "txtremarks" id="close_remarks" class="form-control"></textarea>				
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="closed_ticket">Submit</button>
      </div>
    </div>
  </div>
</div>
<script>
    var CloseTicketURL="{{route('closed_ticket')}}";
</script>

<!-- Released Ticket-->
<div class="modal fade" id="release_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Release Ticket</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <input type="hidden" id="id_release" name="txtid"/>
						
						<div class="form-group">
                <label for="add">&nbsp;<b>TN:</b>
                <input type="text" name = "txttn" id="txttn_release" class="form-control" style="border:0px;" />
								</label>
            </div>
		
						<div class="form-group">
                <label for="add">&nbsp;<b>Add Remarks:</b></label>
                <textarea type="text" name = "txtremarks" id="released" class="form-control"></textarea>				
            </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="rel_ticket">Submit</button>
      </div>
    </div>
  </div>
</div>
<script>
    var ReleaseTicketURL="{{route('released_ticket')}}";
</script>




<!-- MODAL TO ASSIGN TICKET-->
<div class="modal fade" id="assign_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Assign Ticket</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <input type="hidden" id="ida" name="txtid"/>
						
			<div class="form-group">
                <label for="add">&nbsp;<b>TN:</b>
                <input type="text" name = "tn" id="tnassign" class="form-control" style="border:0px;" />
				</label>
            </div>

			<table class="table table-striped table-bordered">
				<tr>
					<td>Document Type
					<select style="height:50px;" class="form-control" name="doc_type" id="type">
							<option value="">&nbsp;</option> 
							<option value="Admin">Admin</option>
							<option value="BAC">BAC</option>
							<option value="Finance">Finance</option>  
							<option value="Legal">Legal</option> 
							<option value="Personnel">Personnel</option> 
							<option value="PR/PO D1">PR/PO D1</option>
							<option value="PR/PO D2">PR/PO D2</option>
						</select>
					</td>
          <td>Office
						<select style="height:50px;" class="form-control" name="route" id="aroute">
							<option value="SDS">SDS</option>
							<option value="ASDS">ASDS</option>  
							<option value="CID">CID</option> 
							<option value="SGOD">SGOD</option> 
							<option value="Finance">Finance</option> 
							<option value="ICTU">ICTU</option> 
							<option value="Admin">Admin</option> 
							<option value="Supply">Supply</option>
							<option value="HRMO">HRMO</option>  
							<option value="Cash">Cash</option> 
							<option value="Health">Health and Nutrition</option> 
							<option value="PSDS">PSDS</option> 
						</select>
					</td>
					</tr>
					<tr>
				
						<td colspan="2"> Remarks
						<textarea class="form-control" name="remarks" id="rassign"></textarea>
					</td>
					</tr>
					<tr>
						<td colspan="2">Mark as priority &nbsp;<input type="checkbox" name="status_2" id="astat2" /></td>
					</tr>
					<input type="hidden" name="ID" value="">
			</table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="tassign">Assign</button>
      </div>
    </div>
  </div>
</div>
<script>
    var AssignTicketURL="{{route('assign_ticket')}}";
</script>

<!-- transfer ticket-->
<div class="modal fade" id="transfer_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Transfer Ticket</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <input type="hidden" id="idt" name="txtid"/>
						
			<div class="form-group">
                <label for="add">&nbsp;<b>TN:</b>
                <input type="text" name = "txtremarks" id="transfer" class="form-control" style="border:0px;" />
				</label>
            </div>

			<table class="table table-striped table-bordered">
				<tr>
					<td>Office
						<select style="height:50px;" class="form-control" name="route" id="office">
							<option value="">&nbsp;</option> 
							<option value="SDS">SDS</option>
							<option value="ASDS">ASDS</option>  
							<option value="CID">CID</option> 
							<option value="SGOD">SGOD</option> 
							<option value="Finance">Finance</option> 
							<option value="ICTU">ICTU</option> 
              <option value="Records">Records</option> 
							<option value="Admin">Admin</option> 
							<option value="Supply and Property">Supply and Property</option>
							<option value="HRMO">HRMO</option>  
							<option value="Cash">Cash</option> 
							<option value="Health and Nutrition">Health and Nutrition</option> 
							<option value="PSDS">PSDS</option> 
						</select>
					</td>
					<td> Remarks
						<textarea class="form-control" name="remarks" id="trans_remarks"></textarea>
					</td>
				</tr>
			</table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="trans_ticket">Transfer</button>
      </div>
    </div>
  </div>
</div>
<script>
    var TransferTicketURL="{{route('transfer_ticket')}}";
</script>

<!-- reset password-->
<div id="reset_password" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h2 class="modal-title">Password Reset</h2>			
			</div>  
			<div class="modal-body">
			      <input type="hidden" name="txtAccountCode" id="txtAccountCode" value="<?php //echo $_SESSION['email'] ?>">
				 <form action="inc/changepassword.php" name="changePassword" id="changePassword" method="POST" >
                      <div class="form-group">
                       		<label class="col-lg-4 control-label">Old Password</label>
                                <div class="col-lg-8">
                                        <input type="password" placeholder="Password" class="form-control" name="oldtxtPassword" id="oldtxtPassword"required >	
                                        <span class="help-block m-b-none text-danger lblPassword_Note"></span>
                                 </div>
                      </div>   
					<div class="form-group">
                           <label class="col-lg-4 control-label">New Password</label>
                               <div class="col-lg-8">
                                      <input type="password" placeholder="Password" class="form-control" name="newtxtPassword" id="newtxtPassword"required >	
                                      <span class="help-block m-b-none text-danger lblPassword_Note"></span>
                                </div>
                     </div>
                     <div class="form-group">
                         <label class="col-lg-4 control-label">Confirm Password</label>
                             <div class="col-lg-8">
                                 <input type="password" placeholder="Confirm Password" class="form-control" name="txtConfirmPassword" id="txtConfirmPassword" required >
                                    <span class="help-block m-b-none text-danger lblConfirmPassword_Note"></span>
                             </div>
                      </div>
					<div class="panel-footer clearfix">				   
						<button class="btn btn-sm btn-success" data-loading-text="Please wait..." type="submit" name="btnConfirm" id="btnConfirm" value="confirm">Reset</button>
					</div>
             </form>
		</div>
	</div>
</div>
</div>

	<script>
	function myFunction() {
    var x = document.getElementById("txtPassword");
    if (x.type === "txtPassword") {
        x.type = "text";
    } else {
        x.type = "txtPassword";
    }
}
	</script>	

<!--modal for pending ticket-->
<div class="modal fade" role="dialog" id="dynamicmodal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		    <div class="modal-header">
		        <h3>Pending Tickets route to {{ Session::get('Department') }} office</h3>
			</div>
			<div class="modal-body table-responsive"  id="pendingticks">

			</div>
			<div class="modal-footer">
				<button class="btn" id="close">Close</button>
			</div>
		</div>
	</div>
</div>	<!--end of modal-->
<script>

document.getElementById("pendingticks").innerHTML='<object type="text/html" data="pending_tickets" style="height: 500px; width:100%;"></object>';

 $("#close").on('click', function () {
   $("#dynamicmodal").toggle('modal');
        location.reload();
    });
</script>

<!--modal for search result-->
<div class="modal fade" role="dialog" id="searchmodal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <h5 class="modal-title" id="exampleModalLabel">Ticket Number: <strong style='color: #03A9F4;' id='ticketnumbersearched'></strong><strong id='doctitle'></strong><br>from <strong id='off_origin'></strong></h5>
                
			</div>
			<div class="modal-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Date/Time</th>
                                <th>Location</th>
                                <th>Remarks</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                            <tbody id="searchresult">

                            </tbody>
                    </table>
			</div>
		</div>
	</div>
</div>



<!-- Modal for Dashborad Ticket Number Additional Information -->
<div class="modal fade" role="dialog" id='datamodal'>
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <br>
        <br>
         <div class="panel panel-default"  id='overduepanel'>
        <div class="panel-body">
           <center> <img src="https://media.giphy.com/media/onsxwAbM8xUNa/giphy.gif" style="width: 50%;">
            <p style='color: #F44336; font-family: roboto; font-size: 30px;'>Hey there!</p>
         <p>This ticket is overdue for almost <strong id="d0"></strong> days!</p></center>
        </div>
         </div>
         
         <div class="row">
             <div class="col-sm-6">
                 <div class="panel panel-default">
                     <div class="panel-body">
                    <p style='font-family: roboto; font-size: 30px; color: #03A9F4;'><i class="fas fa-map-marker"></i> Location</p>
                    <div  id="d1"style="color: white; background-color: #03A9F4; border-radius: 4px; padding: 5px;">

                    </div>
                       
                     </div>
                 </div>

             </div>
              <div class="col-sm-6">
                  <div class="panel panel-default">
                      <div class="panel-body">
                    <p style='font-family: roboto; font-size: 30px; color: #4CAF50;'><i class="fas fa-bookmark"></i> Status</p>
                    <div id="d3" style="color: white; background-color: #4CAF50; border-radius: 4px; padding: 5px;">

                    </div>
                        
                      </div>
                  </div>

              </div>
         </div>
      <div class="panel panel-default">
                      <div class="panel-body">
                              <p style='font-family: roboto; font-size: 30px;'>Remarks</p>
         <p id="d2">...</p>
   </div>
                  </div>
         
     
         
         

      </div>
    </div>

  </div>
</div>












