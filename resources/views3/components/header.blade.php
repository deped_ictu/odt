<div class="container" style="margin-top: 15px; padding:0;">
    <!-- Navigation -->
    <nav class="navbar navbar-default" style="margin:0; border-radius:0; border-top-right-radius: 6px; border-top-left-radius: 6px;">
    	<div class="container-fluid">
    		<div class="navbar-header">
    			<a class="navbar-brand" href="#">Online Document Tracking System</a>
    		</div>
    		<ul class="nav navbar-nav">
            @if(Session::get('Department')=='ICTU_Admin')
            <li class="active"><a href="{{route('dashboard')}}">Dashboard</a></li>			
    				<ul class="nav navbar-nav pull-right"> 
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports<span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{route('print_logs')}}">DTS Logs</a></li>
                                <li><a href="reports.php">Summary</a></li>
                                <li><a href="ticket_status.php">Ticket Status</a></li>
                            </ul>
                        </li>
    	    	    </ul>
                    @elseif (Session::get('Department')=='Records' || Session::get('Department')=='ICTU')
    				 <li class="active"><a href="{{route('dashboard')}}">Dashboard</a></li>
    				<ul class="nav navbar-nav pull-right"> 
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports<span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{route('print_logs')}}">DTS Logs</a></li>
                                <!--<li><a href="reports.php">Summary</a></li>
                                <li><a href="ticket_status.php">Ticket Status</a></li>-->
                            </ul>
                        </li>
    	    	    </ul>
                    @else
                    <li class="active"><a href="{{route('dashboard')}}" >Dashboard</a></li>
            @endif
    		</ul>
    
    		<ul class="nav navbar-nav pull-right"> 
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hi {{Session::get('Firstname')}} {{Session::get('Lastname')}}! <span class="caret"></span></a>
                  <ul class="dropdown-menu dropdown-menu-right">
                    <!--<li><a href="#" data-toggle="modal" data-target="#reset_password">Change Password</a></li>-->
                    <li><a href="{{route('logout')}}">Exit ODTS</a></li>
                  </ul>
                </li>
    		</ul>
    	</div>
    </nav>
</div>    
     