<!-- DataTable -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" />
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

	<script type="text/javascript" class="init">
	$(document).ready(function() {
		$('#ticket_list').DataTable( {
			"order": [[ 0, "desc" ]]
		} );

		$('#assigned_ticket').DataTable( {
		    stateSave: true,
			"order": [[ 3, "desc" ]]
		} );
		
		$('#ticket_lists').DataTable( {
			"columnDefs": [
            {
                "targets": [ 2 ],
                "visible": false,
                "searchable": false,
                "dataorder": [ 2, "asc" ]

            },
        ]
		} );
		$('table.display').DataTable();
	} );
	</script>
	

	<script type="text/javascript" class="init">
	$(document).ready(function() {
		$('#ticket_closed').DataTable( {
			"order": [[ 1, "ASC" ]]
		} );
	} );
	</script>
