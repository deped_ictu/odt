@extends('layouts.master')

@section ('title')
    DepEd | Marikina DTS | Login
@endsection

@section('content')
<div class="container">
<h2>Search Result </h2>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Tn</th>
            <th>Title</th>
            <th>From</th>
            <th>Company Name</th>
            <th>Date / Time</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($searchticket as $data)
        <tr>
            <td style="font-family:monospace;text-transform:uppercase;">{{ $data['tn'] }}</td>
            <td>{{ $data['title'] }}</td>
            <td>{{ $data['origin'] }}</td>
            <td>{{ $data['cname'] }}</td>
            <td>{{ $data['date_time'] }} {{ $data['time'] }}</td>
            <td>{{ $data['status'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>

@endsection