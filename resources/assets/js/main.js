//add remarks
$(function(){

   $(".add_remarks").click(function(){
       var id= $(this).data('id');

       $("#txtidx").val(id);
      
  

    }); 

    $("#btn_addremarks").click(function(){

        var remarks = $("#txtremarks").val();
        var _token=$('input[name=_token]').val();
        var id = $("#txtidx").val();

            $.ajax({
                method:'post',
                url: {{route('addremarks')}},
                data:{
                    '_token':_token,
                    'remarks':remarks,
                    'id':id,
                }
            })
            .done(function(msg){
              window.location="/dashboard";
               //console.log(msg['message']);
            });
    });