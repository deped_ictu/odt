@extends('layouts.master')

@section ('title')
    DepEd | Marikina DTS | Reference Number
@endsection

@section('content') 
@include('inc.left_panel')


<div class="col-sm-9">
<h2>Document Trail</h2>
<hr>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<!--<th width="30">ID</th>-->
				<th width="40">Action</th>
				<th width="40">Route</th>
				<th width="60">Remarks</th>

				<!--<th width="50">Status</th>-->
			</tr>
		</thead>
		<tbody>
		<?php
			foreach($refno as $data){
			$data['title'];
			$data['tn'];
			}
			$track = $data['tn'];
			$t = $data['title'];	
      	?>
		<h3 style="text-transform: uppercase;"><strong><?php echo $t; ?></strong><br>
		 #<?php echo $track; ?></h3>
		@foreach($refno as $data)		
				<tr>     
					<td>{{ $data['action'] }}</td>
					<td>{{ $data['route'] }}</td>
					<td>{{ $data['remarks'] }} </td>
				</tr>
		@endforeach
		</tbody>  
	</table>

<!--add document trail-->
@include('inc.add_doc_trail_sc')

<!--end-->
</div>
<script>
var UpdateTicketTN = "{{route('updatetn')}}";
</script>
@endsection
