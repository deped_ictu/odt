@extends('layouts.master')

@section ('title')
    DepEd | Marikina DTS | Summary
@endsection

@section('content')  
<div class="col-md-12">
		<h2>Assigned Tickets from Records</h2>
	</div>
	<div class="col-sm-3">
	    <div class="panel panel-info">
	        <div class="panel-heading">
	            
            <!--Search-->
    		<form action="{{route('search')}}" autocomplete="off" method="POST">
    		{!! csrf_field() !!}
                <div class="input-group input-group-lg">
                    <input type="text" id="search" class="form-control" name="txtsearch_ticket" placeholder="" required/>
                    <span class="input-group-btn">
                        <button class="btn btn-default" id="btnsearch">GO</button>
                    </span>
				</div>
				<div>
					<!--<input type="checkbox" name="searchtn" id="exactsearchtn"/><p>Search key words</p>-->		 
                </div>
            </form>

			<?php
				$sdodepartments = array("SGOD","CID","ICTU","SDS","Finance","Admin","Supply","Cash", "Records","Personnel", "Health", "Legal", "PSDS", "ASDS");
				$Is_Sdo_account = false;
				if(in_array(Session::get('Department'),$sdodepartments)){
					//this is a SDO account
					$Is_Sdo_account = true;			
				}else{
					//This is a School Account
					$Is_Sdo_account = false;
				}
				?>                   
            <!--End of Search-->	
	        </div><!--end of panel-heading-->
	        <div class="list-group">
				<!--<a href="#" class="list-group-item">Assigned Ticket</a>-->
				
				@if($Is_Sdo_account==false)
				<a href="#" class="list-group-item" data-toggle="modal" data-target="#create_ticket">Create Ticket</a>
				<a class="list-group-item" href="{{route('closed_tickets')}}">Closed Tickets</a>
				@elseif($Is_Sdo_account==true)
				
				<a href="{{route('dashboard')}}" class="list-group-item" data-toggle="modal" data-target="#create_ticket">Assigned Tickets</a>
				<a href="#" class="list-group-item" data-toggle="modal" data-target="#create_ticket">Create Ticket</a>
				<a class="list-group-item" href="#" data-toggle="modal" data-target="#dynamicmodal"> Pending Tickets from Other Office </a>
                
                @if(Session::get('Department')=='SDS')
                <a class="list-group-item" href="#" data-toggle="modal" data-target="#dynamicmodal">Pending Tickets from Records 
					<span class="badge badge-info assigned">
					{{ $sdstransfer }}
					</span>
				</a>
                @endif

				@endif
				@if(Session::get('Department')=='Records')
				<a href="{{route('pending_tickets_fromschool')}}" class="list-group-item" data-toggle="modal" data-target="#">Clear Tickets</a>
				@else
				@endif
				<!--<a href="#" class="list-group-item" data-toggle="modal" data-target="#">Follow-up Ticket</a>-->
			</div>
		</div>
			@if($Is_Sdo_account==false)
			@else
			<table class="table table-striped table-bordered">
				<tr>
					<th style="text-align:center;">Open</th>
					<th style="text-align:center;"><a href="#" data-toggle="modal" data-target="#overdue_ticket">Overdue</a></th>
				</tr>
				<tr>
					<td align="center" ><span id="LabelOpen"></span></td>
					<td align="center"><span id='LabelOverdue'>0</span></td>
				</tr>
			</table>
			<!--EFFICIENCY COUNT-->
			<!--<div class='panel panel-default' id="effpanel" style="background-image: linear-gradient(to right, #55efc4 , #00b894);">
			    <div class='panel-body'> <center><h1 id='effrat' style='color:white; text-shadow: 0px 1px 2px rgba(0,0,0,0.5);'></h1>
			    <h1 style='color:white; text-shadow: 0px 1px 2px rgba(0,0,0,0.5);' id='efficiencyrat'></h1>
			    </center></div>
			</div>-->
			@endif
	</div>
	<div class="col-sm-9">
        <!--<button class="btn btn-success pull-right" style="margin-bottom:8px;">Accept all ticket</button>-->
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th width="30">TN</th>
					<th>Title</th>
					<th width="60">Date/Time</th>
                    <th width="20"></th>
				</tr>
			</thead>
			<tbody>
				@foreach($transferto as $data)
                    <tr id="asignedt">
                    <td style="text-transform:uppercase;">{{ $data['tn']}}</td>
					<td>{{ $data['title'] }}</td>
				    <td>{{ $data['date_time'] }} - {{ $data['time'] }}</td>
                    <td><button class='btn btn-primary btn_accepta'  id ="accept_assigned" data-id="{{$data['id']}}" data-tn="{{$data['tn']}}"><span class="glyphicon glyphicon glyphicon-ok"></span></button></td>
				</tr>
			@endforeach
			</tbody>  
	    </table>
    </div>
	<script type="text/JavaScript">
         
            function AutoRefresh( t ) {
               setTimeout("location.reload(true);", t);
            }
         
      </script>
<script>
var acceptassignedTicketURL="{{route('accept_assigned')}}";

</script>

<script>
$(function transferto(){
$(".btn_accepta").click(function(){
        var token= $('input[name=_token]').val();
		//alert(token);
        var id = $(this).data('id');
		var stat = 'Received';
		var date_time = new Date('Y-m-d');
		var time = new Date('H:i:s');
		var agent = $(this).data('agent');
		var route = $(this).data('route');
		var transfer = '';
		
			//if(confirm("Are you sure you want to accept this ticket with id " + id + '?'))
			//{
				//update
				$.ajax({
					type:"POST",
					url: acceptassignedTicketURL,
					data:{
						_token:token,
						id:id,
						status:stat,
						route:route,
						transfer_to:transfer,
						date_time:date_time,
						time:time,
						agent:agent},
					
				})
				.done(function(msg){
				//alert(msg);
				//e.preventDefault();
				location.reload(); 
              //window.location="/for_transfer";
              //console.log(tn, title, stats, stat2, agent, remarks, email, origin, cname, doc_type, route, date, time, date_out, time_out, id);
			  
            });
				//insert
			//}
			//else { 
				//return false;
			//}
			
    });

});
</script>



<!--search for Exact match--> 
            <script>
            $("#btnsearch").click(function(){
                var searchkeyword = $("#search").val();

				var exacttn = document.getElementById("exactsearchtn"); 
				var exacttn;
				if(exacttn.checked == false){
				
                //alert(searchkeyword);
                $.ajax({
                    type: "POST",
                    url: "{{route('searchexact')}}",
                    data: {
                        _token:"{!! csrf_token() !!}",
                        txtsearch_ticket:searchkeyword},
                    success: function(data){
                        $("#ticketnumbersearched").html(searchkeyword + "<br>");
                        $("#doctitle").html("(not found)");
                        $("#off_origin").html("(not found)");
                        data = JSON.parse(data);
                        $("#search").val("");
                        //alert(JSON.stringify(data));
                       $("#searchresult").html("");
                       for(var i =0; i < data.length;i++){
                           
                        $fdate = new Date(data[i]["date_time"]);
                        $fdate = $fdate.getDay() + "/" + $fdate.getMonth() + "/" + $fdate.getFullYear();
                        $("#doctitle").html(data[i]["title"].toUpperCase());
                        $("#searchresult").append("<tr><td>" + $fdate + " - " + data[i]["time"] + "</td>" + 
                        "<td>" + data[i]['route']  + "</td>" + 
                        "<td><div class='rem'>" + data[i]['remarks'] +"</div></td>" + 
                        "<td>" + data[i]['status'] +"</td>" + 
                        "</tr>");
                        if(i <= data.length){
                            $("#off_origin").html(data[i]["origin"] + " (" + data[i]["cname"] + ")");
                        }
						
                       }
                    }
                })
			}
            })

            </script>

@endsection