@extends('layouts.transmital')
@section ('title')
    DepEd | Marikina DTS | Print Transmital
@endsection

@section('content')  
    @foreach($ptickets as $data)
	<div class="print-buttons" onClick="hide()">
		<a href="#" class="fa"><i class="glyphicon glyphicon-print" style=" margin-right:20px;" onClick="window.print();"></i></a>
	</div>

<div class="container well">
	<div class="row">
	    <div class="col-md-12">
    		<table width="100%">
    		    <tr>
    		        <td colspan="2">
    		            <h4>Schools Division Office - Marikina City | Document Tracking System</h4>
    		        </td>
    		    </tr>
    			<tr>
    				<td width="60%" valign="top" style="border-bottom: 1px solid #ccc;">
    					<h3 style="margin:0;">DOCUMENT RECEIPT <small>(School Copy)</small></h3>
    					TICKET CREATED: {{$data['date_time']}} {{$data['time']}}
    					
    				</td>
    				<td width="40%" align="right" style="border-bottom: 1px solid #ccc;">
    					<h1 style="margin-top:0; text-transform:uppercase" id="ptn">&#x23;{{$data['tn']}}</h1>
    				</td>
    			</tr>
    			<tr>
    			    <td colspan="2">
    			        <h2 style="margin-bottom:0; font-weight: bold; text-transform: uppercase;">{{ Session::get('sname')}}</h2>
    			    </td>
    			</tr>
    		</table>
			<table width="100%">
				<tr>
					<td width="100%" colspan="3" valign="top">
					    <h4 style="margin-top:0;">{{$data['title']}}</h4>
					</td>
				</tr>   			    
	    		<tr>
	    			<td width="12%" valign="top">Notes:</td>
	    			<td width="58%" valign="top">{{$data['remarks']}}</td>
	    			<td width="30%" rowspan="3" valign="top" >
	    			    <div style="min-height:170px; margin: 0 15px; padding: 5px; border: 1px solid #ccc;"></div>
	    			</td>
				</tr>
				<tr>
					<td width="30%" colspan="2" valign="bottom">
					    <h4 style="margin-bottom: 6px; font-weight: bold"><?php //echo $principal; ?>{{ Session::get('principal') }}</h4>
					    <p>School Principal / Officer-In-Charge</p>
					</td>
				</tr>
			</table>
	    </div>
	</div><!--END OF ROW-->
</div><!--END OF CONTAINER-->

<hr style="margin: 35px 0; border: 1px dashed #999;">

<div class="container well" style="padding-right:5px; padding-bottom: 5px;">
	<div class="row">
	    <div class="col-md-12">
    		<table width="100%" border="0" cellpadding="0">
    			<tr>
    				<td colspan="2" width="60%" valign="top">
    				    <h4>Schools Division Office - Marikina City | DTS</h4>
    					<h3 style="margin:0;">DOCUMENT TRANSMITTAL</h3>
  					</td>
    				<td rowspan="5" width="40%" valign="top" style="padding: 0 8px; border-left:1px solid #ccc; font-size: .9em">
    				    <p style="margin-top:0; font-weight: bold;">FOR DIVISION OFFICE USE</p>
    				    REFERRED TO
    				    <ul style="list-style-type: circle">
    				        <li><span>OIC, Office of the Assistant Schools Division Superintendent</span></li>
    				        <li><span>Dr. Elisa O. Cerveza, Chief ES - CID</span></li>
    				        <li><span>Dr. Elizalde Q. Cena, Chief ES - SGOD</span></li>
    				        <li><span>Atty. Ceasar Augustos E. Cebujano, Legal Unit</span></li>
    				        <li><span>Richie B. Ignacio, Budget Unit</span></li>
    				        <li><span>Ivy R. Ruallo, Accounting Unit</span></li>
    				        <li><span>Ryan Lee Regencia, ICT Unit</span></li>
    				        <li><span>Claro L. Capco, Administrative Office<span></li>
    				        <li><span>Bienvenido N. Contapay, Records Unit<span></li>
    				        <li><span>Estella M. Uayan, Cash Unit<span></li>
    				        <li><span>Araceli D. Dy, HR unit<span></li>
    				        <li><span>Anna Marie Exequiel, Supply Unit<span></li>
    				    </ul>
    				    ACTION TO BE TAKEN
    				    <ul style="list-style-type: circle">
    				        <li><span>For appropriate action</span></li>
    				        <li><span>For review/comments</span></li>
    				        <li><span>For information/filing</span></li>
    				        <li><span>Please expedite/rush</span></li>
    				        <li><span>Please explain</span></li>
    				    </ul>
                        <hr style="margin:20px 0 0 0; border: 1px solid #ccc;">
                        <hr style="margin:20px 0 0 0; border: 1px solid #ccc;">
                        <hr style="margin:20px 0 0 0; border: 1px solid #ccc;">
                        <p>&nbsp;</p>
                        <h5 style="text-align:center; font-weight: bold;">SHERYLL T. GAYOLA<br>
                        <small>Officer-In-Charge<br>Office of the Schools Division Superintendent</small></h5>
    				</td>  					
                </tr>
                <tr>
                    <td width="" valign="top">
                    <img src="data:image/png;base64,{{DNS1D::getBarcodePNG('15', 'C39', 4, 100)}}" alt="barcode" /><br>
                    <p style="margin-top:8px; font-size:28; font:'sans-serif'; margin-left:60px; text-transform:uppercase" >{{$data['tn']}}</p>
    				</td>

    				<td valign="top">TICKET CREATED: <br>{{$data['date_time']}} <br> {{$data['time']}}</td>
    			</tr>
    			<tr>
    			    <td colspan="2">
    					<h4 style="margin:0 0 5px 0; font-weight: bold; text-transform: uppercase;">{{ Session::get('sname') }}</h4>
    				</td>

    			</tr>
    			<tr>
					<td colspan="2" width="60%" valign="top" style="padding-left: 15px;">
					    <div style="min-height: 110px">
    					    <p style="margin-bottom:10px;">{{$data['title']}}</p>
    					    <p>{{$data['remarks']}}</p>
					    </div>
					</td>
    			</tr>
    			<tr>
    			    <td colspan="2" cellpadding="0">
    			        <p style="margin-bottom:3px;"><b>{{ Session::get('principal') }}</b><br>
    			        <small>School Principal / Officer-In-Charge</small>
    			        </p>
					    
    			    </td>
    			</tr>
    		</table>
	    </div>
	</div><!--END OF ROW-->
</div><!--END OF CONTAINER-->


@endforeach
@endsection