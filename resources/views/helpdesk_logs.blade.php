@extends('layouts.master_modal')

@section('content')
<div class="col-sm-12">
  <table id="logs_helpd" class="table table-striped table-bordered">
  <thead>
      <tr>
          
          <th>Name</th>
          <th>Department</th>          
          <th>Concern</th>
          
      </tr>
  </thead>
  <tbody>
    @foreach($hdlogs as $logs)
                    <tr> 
                      <td>{{$logs['name']}}</td>                    
                      <td>{{ $logs['department'] }}</td>
                      <td>{{ $logs['remarks'] }}</td>
                    </tr>                 
      @endforeach
  </tbody>  
  </table>

</div>
@endsection