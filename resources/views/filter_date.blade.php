@extends('layouts.master')

@section ('title')
    DepEd | Marikina DTS | Login
@endsection

@section('content')
<style>
    #print {

    }
    
    @media print {
        .hide_print,
        .footer {
            display: none;
        }
        
    }
</style>
<table id="p_records" class="table table-bordered">
    <tr>
        <th width="20">ID</th>
        <th width="20%">Tracking Number</th>
        <th>Title</th>
        <th width="20%">From</th>
        <th width="10%">Time</th>
        </tr>
         @foreach($filterlogs as $data)
        <tr>
            <td>{{$data['id']}}</td>
            <td style="font-family:monospace;text-transform:uppercase;">{{$data['tn']}}</td>
            <td>{{$data['title']}}</td>
            <td>{{$data['origin']}} - {{$data['cname']}}</td>
            <td>{{$data['date_time']}} - {{$data['time']}}</td>
        </tr>
            @endforeach
</table>
@endsection