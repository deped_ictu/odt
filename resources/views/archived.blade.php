@extends('layouts.master')

@section ('title')
    DepEd | Marikina DTS | Archived Tickets
@endsection

@section('content')
<h2>Archived Tickets</h2>
<div class="row">
<div class="col-sm-3">
    <div class="panel panel-info">
        <div class="panel-heading">
       <!--Search-->
	   <form action="{{route('search')}}" autocomplete="off" method="POST">
    		{!! csrf_field() !!}
                <div class="input-group input-group-lg">
                    <input type="text" id="search" class="form-control" name="txtsearch_ticket" placeholder="" required/>
                    <span class="input-group-btn">
                        <button class="btn btn-default" id="btnsearch">GO</button>
                    </span>
				</div>
				<div>
					<!--<input type="checkbox" name="searchtn" id="exactsearchtn"/><p>Search key words</p>	-->	 
                </div>
        </form>
        <script>
        $("#btnsearch").click(function(){
            var searchkeyword = $("#search").val();
            //alert(searchkeyword);
            $.ajax({
                type: "POST",
                url: "{{route('search')}}",
                data: {
                    _token:"{!! csrf_token() !!}",
                    txtsearch_ticket:searchkeyword},
                success: function(data){
                    $("#ticketnumbersearched").html(searchkeyword + "<br>");
                    $("#doctitle").html("(not found)");
                    $("#off_origin").html("(not found)");
                    data = JSON.parse(data);
                    $("#search").val("");
                    //alert(JSON.stringify(data));
                   $("#searchresult").html("");
                   for(var i =0; i < data.length;i++){
                       
                    $fdate = new Date(data[i]["date_time"]);
                    $fdate = $fdate.getDay() + "/" + $fdate.getMonth() + "/" + $fdate.getFullYear();
                    $("#doctitle").html(data[i]["title"].toUpperCase());
                    $("#searchresult").append("<tr><td>" + $fdate + " - " + data[i]["time"] + "</td>" + 
                    "<td>" + data[i]['route']  + "</td>" + 
                    "<td><div class='rem'>" + data[i]['remarks'] +"</div></td>" + 
                    "<td>" + data[i]['status'] +"</td>" + 
                    "</tr>");
                    if(i <= data.length){
                        $("#off_origin").html(data[i]["origin"] + " (" + data[i]["cname"] + ")");
                    }
                   }
                }
            })
        })
    </script>  
        <!--End of Search-->	

			<?php
			$sdodepartments = array("SGOD","CID","ICTU","SDS","Finance","Admin","Supply","Cash", "Records","Personnel", "Health", "Legal", "PSDS", "ASDS");
			$Is_Sdo_account = false;
			if(in_array(Session::get('Department'),$sdodepartments)){
				//this is a SDO account
				$Is_Sdo_account = true;			
			}else{
				//This is a School Account
				$Is_Sdo_account = false;
			}
			?>                   
        </div>
        <div class="list-group">
			<!--<a href="#" class="list-group-item">Assigned Ticket</a>-->
            <a href="#" class="list-group-item" data-toggle="modal" data-target="#create_ticket">Create Ticket</a>
			<a href="{{route('dashboard')}}" class="list-group-item" data-toggle="modal" data-target="#create_ticket">Assigned Tickets</a>

			@if(Session::get('Department')=='SDS')
			<a class="list-group-item" href="#" data-toggle="modal" data-target="#dynamicmodal">Pending Ticket
			<span class="badge badge-pill badge-success pending"> {{ $pendingtick - $sdstransfer }} </span>
			</a>
			@else
			<a class="list-group-item" href="#" data-toggle="modal" data-target="#dynamicmodal">Pending Ticket
			<span class="badge badge-pill badge-success pending"> {{ $pendingtick }} </span>
			</a>
			@endif
			
			@if(Session::get('Department')=='SDS')
			<a class="list-group-item" href="{{route('for_transfer')}}">Pending Tickets From Records 
					<span class="badge badge-info assigned">
					{{ $sdstransfer }}
					</span>
			</a>
			@else
			@endif

			@if(Session::get('Department')=='Records')
			<a href="{{route('pending_tickets_fromschool')}}" class="list-group-item" data-toggle="modal" data-target="#">Clear Tickets</a>
			@else
			@endif
			<!--<a href="#" class="list-group-item" data-toggle="modal" data-target="#">Follow-up Ticket</a>-->
		</div>
	</div>
    @if($Is_Sdo_account==false)
	@else
	<table class="table table-striped table-bordered">
		<tr>
			<th style="text-align:center;">Open</th>
			<th style="text-align:center;">Overdue</th>
			
		</tr>
		
		<tr>
			<td align="center" ><span id="LabelOpen"></span></td>
			<td align="center"><span id='LabelOverdue'></span></td>
		
		</tr>
		
	</table>
	<!--EFFICIENCY COUNT
	<div class='panel panel-default' id="effpanel" style="background-image: linear-gradient(to right, #55efc4 , #00b894);">
	    <div class='panel-body'> <center><h1 id='effrat' style='color:white; text-shadow: 0px 1px 2px rgba(0,0,0,0.5);'></h1>
	    <h1 style='color:white; text-shadow: 0px 1px 2px rgba(0,0,0,0.5);' id='efficiencyrat'></h1>
	    </center></div>
	</div>-->
	@endif
</div>
<div class="col-sm-9">
	<h3 style="margin-top: 0;">Archived Tickets assigned to  {{ Session::get('Department') }} office</h3>
	<table id="assigned_ticket" class="table table-striped table-bordered">
		<thead>
			<tr>
				<!--<th width="30">ID</th>-->
				<th width="30">TN</th>
				<th>Title</th>
				<th width="60">From</th>
				<th width="60">Date/Time</th>
				<th width="50">Remarks</th>
				<th width="50" id="action">Actions</th>
			</tr>
		</thead>
		<tbody>
            <?php
            $datacount = 0;
            $overduecount = 0;
            ?>
			@foreach($archived_ticket as $data)						
				@if ($data['route']==Session::get('Department') )
					@if ($data['status']!='Transferred' && $data['status']!='Closed' && $data['status']!='Pending Submission' && $data['status']!='Released')
						<tr>     
						<!--<td>{{ $data['id'] }}</td>     -->
					
							<?php
							//VIRMIL ADDED CODE >>>>>>>> OVERDUE DETECTOR
							$datacount ++;
							$ticketnumber = $data['tn'];
							$fetched_document_day = strtotime($data['date_time']);
							$is_overdue = false;
							$gotdate = $data['date_time'];
							
							//SNIPPED FROM OLA v2
							
							//Compute All Serving Days
							$NumberOfDocumentDays = 0;
					
							//Current Month Integers
							$month = date("m",$fetched_document_day);
							$list = "";
							$lmonth = date("m",$fetched_document_day) -1;
							$year = date("Y",$fetched_document_day); 
						
							for($d = date("d",$fetched_document_day); $d <= 31;$d++){
								$time=mktime(12, 0, 0, $month, $d, $year);
								if(date('m',$time) == $month){
									if(date("l",$time) != "Saturday" && date("l",$time) != "Sunday"){
										if($d <= date("d")){
												$NumberOfDocumentDays += 1;
										}
									}
								}
							}
							//echo $NumberOfDocumentDays;
							if($NumberOfDocumentDays > 5){
								$is_overdue = true;
								$overduecount +=1;
							}
							//END OF SNIP
				
							$imaglink = "<img class=popimg src=https://media.giphy.com/media/qjqUcgIyRjsl2/source.gif><br>";
						    if($is_overdue == true){
						    // OVERDUE
						    ?>
						    
						    <td style='background-color: #E57373; font-family:monospace; text-transform:uppercase;'>
						    <strong style='color:white; text-shadow: 0px 1px 1px rgba(0,0,0,0.2);'>
						    <a href='#' class='btnaddinfo' style='color:white;' data-dayscount="{{$NumberOfDocumentDays}}"
						    data-location="{{ $data['route'] }}"
						    data-remarks="{{ $data['remarks'] }}"
						    data-status="{{ $data['status'] }}"
						    data-toggle="modal"
						    data-target="#datamodal"> {{ $ticketnumber }}</a>
						    </strong></span></td>
						   
						    <?php
					     
						}elseif($data['status_2']=='Priority'){
						    // PRIORITY
						    ?>
						    
						    <td style='background-color: #9C27B0; font-family:monospace;text-transform:uppercase;'>
							    <strong style='color:white; text-shadow: 0px 1px 1px rgba(0,0,0,0.2);'>
							    <a href='#' class='btnaddinfo' style='color:white;' data-dayscount="{{$NumberOfDocumentDays}}"
							    data-location="{{ $data['route'] }}"
							    data-remarks="{{ $data['remarks'] }}"
							    data-status="{{ $data['status'] }}"
							    data-toggle="modal"
							    data-target="#datamodal"> {{ $ticketnumber }}</a>
							    </strong></span>
						    </td>
						    
						    <?php
						    
						}else{
						    //DEFAULT
						    ?>
						    
						    <td style='font-family:monospace;text-transform:uppercase;  font-size: 1.2em'>
						    <span ><strong>
						    <a href='#' class='btnaddinfo' data-toggle="modal" data-dayscount="{{$NumberOfDocumentDays}}"
						    data-location="{{ $data['route'] }}"
						    data-remarks="{{ $data['remarks'] }}"
						    data-status="{{ $data['status'] }}"
						    data-target="#datamodal">{{ $ticketnumber }}</a>
						    </strong></span></td>
				            
						    <?php
						}
						echo "<script>
						$('#LabelOpen').html($datacount);
						$('#LabelOverdue').html($overduecount);
						</script>";
						//VIRMIL ADDED CODE >>>>>>>> OVERDUE DETECTOR
						?>
							<td>{{ $data['title'] }}</td>
							<td>{{ $data['origin'] }}</td>
							<td>{{ $data['date_time'] }} - {{ $data['time'] }}</td>
							<td>{{ $data['remarks'] }}</td>
							<td>
							@if($data['status']!='Pending Transfer' && $data['route']==Session::get('Department'))
							<div class="dropdown">
								<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fas fa-cog"></i>
								<span class="caret"></span></button>
								<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1" >
								<!--user accounts-->    
									<li><a href="" class='archivedre'  data-id="{{$data['id']}}" data-remarks="{{$data['remarks']}}" data-tn="{{$data['tn']}}" data-agent="{{$data['agent']}}" data-toggle="modal" data-target="#archived_remarks">Add Remarks</a></li>
									
									<li><a href="#" class="closed_ticket_ar" data-id="{{$data['id']}}" data-title="{{$data['title']}}" data-status="{{$data['status']}}" data-remarks="{{$data['remarks']}}" data-tn="{{$data['tn']}}" data-agent="{{$data['agent']}}" data-status2="{{$data['status_2']}}" data-email="{{$data['email']}}" data-origin="{{$data['origin']}}" data-cname="{{$data['cname']}}" data-doctype="{{$data['doc_type']}}" data-route="{{$data['route']}}" data-date="{{$data['date_time']}}" data-time="{{$data['time']}}" data-dateout="{{$data['date_out']}}" data-timeout="{{$data['time_out']}}" data-toggle="modal" data-target="#close_ticket">Close Ticket</a></li>

									<li><a href="#" class="transfer_ticket_ar" a href="#transfer_ticket" data-toggle="modal" data-target="#transfer_ticket" data-id="{{$data['id']}}" data-title="{{$data['title']}}" data-status="{{$data['status']}}" data-remarks="{{$data['remarks']}}" data-tn="{{$data['tn']}}" data-agent="{{$data['agent']}}" data-status2="{{$data['status_2']}}" data-email="{{$data['email']}}" data-origin="{{$data['origin']}}" data-cname="{{$data['cname']}}" data-doctype="{{$data['doc_type']}}" data-route="{{$data['route']}}" data-date="{{$data['date_time']}}" data-time="{{$data['time']}}" data-dateout="{{$data['date_out']}}" data-timeout="{{$data['time_out']}}">Transfer Ticket</a></li>
											
							        @else
									<button class="btn btn-primary" disabled=""><i class="fas fa-cog"></i><span class="caret"></span></button>
						        	@endif
											
								</ul>
							</div>
						</tr>
					@endif
				@endif
			@endforeach
		</tbody>  
	</table>
</div>
</div>
<script>
var addARRemarksURL="{{route('archremarks')}}";
</script>
@endsection