<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="The official website of the Schools Division of Marikina City">
<meta name="keywords" content="DepEd, Marikina, SDO, Education, Schools, NCR">
<meta name="author" content="Ryan Lee Regencia">

<title>@yield('title')</title>
<link rel="shortcut icon" type="image/x-icon" href="images/DO-logo.png" title=" Division of City Schools Marikina"/>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link rel="stylesheet" href="{{asset('css/login.css')}}" type="text/css" />
<link rel="stylesheet" href="{{asset('css/custom.css')}}" type="text/css" />

<!-- sweetalert -->
<script type="text/javascript" src="{{asset('sweetalert-master/dist/sweetalert.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('sweetalert-master/dist/sweetalert.css')}}"></link>
<script>
function check_empty_fields()
{
var ValidateInput = true;

$(".validate #un,#password").each(function(){
    if ($.trim($(this).val()).length == 0){
        $(this).addClass("highlight");
        ValidateInput = false;
        $(this).focus();
    }
    else{
        $(this).removeClass("highlight");
    }
});

if (!ValidateInput) {
	$("#login-failed").fadeIn(200);
	$("#login-failed").text('Log in failed!');				
}
  return ValidateInput;  
 }
</script>

<style>
@import url('https://fonts.googleapis.com/css?family=Heebo:400,800,900');

body {

}

.purple {
    background-color: #5C6AC4;
}

.white {
    background-color: #fff;
}

.col-sm-10 {
    padding-top: 10px;
}

.fa-search-location {
    font-size: 3em;
}

.fa-search, .fa-file-invoice {
    color: #5C6AC4;
}


h1 {
    margin: auto;
    font-family: 'Heebo';
    font-weight: 900;
    font-size: 4em;
    text-align: center;
    color: #fff;
}

h3 {
    color: #333;
}

p {
    margin-top: 30px;
    font-size: 3em;
    text-align: center;
    color: #fff;
}

.icon, .icon-text {
    padding: 4px;
    font-size: 1.3em;
    vertical-align: top;
}

.icon {
    padding-top: 6px;
    color: #5C6AC4;
}

.login {
    border: 1px solid #eee;
    border-radius: 4px;
    text-align: center;
    background-color: #ecf0f1;
}

hr {
    margin: 40px 0;
    border: 1px dashed #ccc;
}

#results {
    background-color: transparent;
}

#searchresult_box {
    background-color: #fff;
}

@media only screen and (max-width: 600px) {
    
}

</style>

</head>

<body>

<div class="container-fluid">
@yield('content')
</div>
</body>
</html>