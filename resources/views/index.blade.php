@extends('layouts.login_master')

@section ('title')
    DepEd | Marikina DTS | Login
@endsection

@section('content')

<style>
    #searchresult_box{
        animation-name: loaded;
        animation-duration: 0.3s;
    }
    #loadingbar{
        animation-name: loaded;
        animation-duration: 0.3s;
    }
    #alert_text{
        animation-name: loaded;
        animation-duration: 0.3s;
    }
    @keyframes loaded{
        0%{
            opacity: 0;
            margin-top: 20px;
        }
        100%{
            
        }
    }
    
#search {
    height: 80px;
    font-size: 3em; 
    text-align: center;
    border-top-left-radius: 6px;
    border-bottom-left-radius: 6px;
}

#btn_searchticket {
    height: 80px;
    width: 150px;
}    

#logotext2 {
    display: none;
}

#loadingbar {
    width: 200px;
}

@media only screen and (max-width: 600px) {
    #logotext1 {
    display: none;
    }

    #logotext2 {
        display: block;    
    }
    
    #btn_searchticket {
        width: 80px;
    }
    
    #search {
        font-size: 2em;
    }
    
    p {
        font-size: 2em;
        line-height: 100%;
    }
}
</style>
  @if ($errors->has('token_error'))
    {{ $errors->first('token_error') }}
@endif          
<div class="row purple">
    <div class="col-sm-1"></div>
    <div class="col-sm-10">
        <center>
            <p><img src="{{asset('images/sdo_logo_small.png')}}" alt="" /></p>
        </center>
        <h1 id="logotext1">Document Tracking System</h1>
        <h1 id="logotext2">ODTS</h1>
        <p>Got a tracking number? Type and search here.</p>
    </div>
    <div class="col-sm-1"></div>
</div>
<div class="row purple" style="padding-bottom: 30px">
    <div class="col-sm-4"></div>
    <div class="col-sm-4" style="text-align: center">
        <div class="input-group input-group-lg" style="width: ; margin: 10px auto;">
            {!! csrf_field() !!}
            <input type="text" style='border: 0; background: white;' class="form-control" id="search" placeholder="Ticket Number" autocomplete="off">
            <span class="input-group-btn">
                <button style='border: 0; background: rgba(255,255,255,0.5);' class="btn btn-default" id="btn_searchticket" type="button"><i class="fas fa-search-location"></i></button>
            </span>
        </div>
        <div id="results">
            <!--SEARCH FUNCTIONALITY-->
            <img id="loadingbar" src="http://gifimage.net/wp-content/uploads/2017/09/animated-gif-search.gif">
            <div id="alert_text"></div>
        </div>
        <div id="searchresult_box" class="panel panel-success" style="text-align:left;">
            <div class="panel-heading">
                <h3>Ticket number <strong id="ticketnumbersearched"></strong></h3>
                <h3><strong id="doctitle"></strong><br>from <strong id="off_origin"></strong></h3>
            </div>
            <div class="panel-body table-responsive">
                <table class="table table-striped display">
                    <thead>
                        <tr>
                            <th>Date/Time</th>
                            <th>Location</th>
                            <th>Remarks</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody id="searchresult">

                    </tbody>
                </table>
            </div>
        </div>
        
        <script>
            // Get the input field
            var input = document.getElementById("search");
            // Execute a function when the user releases a key on the keyboard
            input.addEventListener("keyup", function(event) {
            // Cancel the default action, if needed
            event.preventDefault();
            // Number 13 is the "Enter" key on the keyboard
            if (event.keyCode === 13) {
            // Trigger the button element with a click
                document.getElementById("btn_searchticket").click();
              }
            });


            $("#results, #searchresult_box").css("display","none");
            $("#btn_searchticket").click(function(){
                SearchTicketNumber();
            })
            
            function SearchTicketNumber(){
                $("#loadingbar").css("display","block");
                $("#searchresult_box").css("display","none");
                $("#alert_text").css("display","none");
                $("#alert_text").html("");
                var searchkeyword = $("#search").val();
                  
                $("#results").css("display","inline-block");
                $.ajax({
                type: "POST",
                url: "{{route('search_trailticket')}}",
                data: {
                    _token:"{!! csrf_token() !!}",
                    txtsearch_ticket:searchkeyword},
                    success: function(data){
                    if(data.length == 4 || data.length == 0){
                    $("#loadingbar").css("display","none");
                    
                    $("#search").val("");
                    $("#alert_text").css("display","inline-block");
                    $("#alert_text").html("<img src='https://cdn.dribbble.com/users/1208688/screenshots/4563859/no-found.gif' width='200px;'><h4 style='color:white;'>Ticket not found.</h4>");
                    }else{
                    $("#ticketnumbersearched").html(searchkeyword + "<br>");
                    $("#doctitle").html("(not found)");
                    $("#off_origin").html("(not found)");
                    data = JSON.stringify(data);
                    data = JSON.parse(data);
                    $("#search").val("");
                    
                    $("#searchresult").html("");
                    for(var i = data.length -1; i >= 0;i--){
                       if(data[i]["action"] != "Transferred"){
                            $fdate = new Date(data[i]["date"]);
                            $fdate = $fdate.getDay() + "/" + $fdate.getMonth() + "/" + $fdate.getFullYear();
                            $("#doctitle").html(data[i]["title"]);
                            $("#searchresult").append("<tr><td>" + data[i]["date"] + " - " + data[i]["time"] + "</td>" + 
                            "<td>" + data[i]['route']  + "</td>" + 
                            "<td><div class='rem'>" + data[i]['remarks'] +"</div></td>" + 
                            "<td>" + data[i]['action'] +"</td>" + 
                            "</tr>");
                            if(i <= data.length){
                                $("#off_origin").html(data[i]["sender"] + " (" + data[i]["cname"] + ")");
                            }
                       }
                    }
                    $("#alert_text").html("");
                    $("#loadingbar").css("display","none");
                    $("#searchresult_box").css("display","block");
                    }
                }
            })
            }
        </script>
        
        <!--END OF SEARCH FUNCTIONALITY-->
    </div>
    <div class="col-sm-4"></div>
</div>
<div class="row white" style="padding-top: 20px;">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
                
        <h3>Need to have a document received by us?</h3>
        <table class="display">
            <tr>
                <td class="icon"><i class="fas fa-chevron-circle-right"></i></td>
                <td class="icon-text">Proceed to the Records Unit which located at the 3rd floor of the SDO Marikina City building, window transaction only.</td>
            </tr>
            <tr>
                <td class="icon"><i class="fas fa-chevron-circle-right"></i></td>
                <td class="icon-text">Handover your document to the records personnel for checking.</td>
            </tr>
            <tr>
                <td class="icon"><i class="fas fa-chevron-circle-right"></i></td>
                <td class="icon-text">Check your document's ticket number on the receiving copy. If not found, ask for it from the records personnel.</td>
            </tr>
            <tr>
                <td class="icon"><i class="fas fa-chevron-circle-right"></i></td>
                <td class="icon-text">Use the ticket number to check the status of your document.</td>
            </tr>
        </table>
        <hr />
        <div class="login">
            <h3>For SDO and school personnel 
            <button class="btn btn-lg btn-success" href="#" data-toggle="modal" data-target="#loginModal">Login</button></h3>
        </div>
    </div>
    <div class="col-sm-4"></div>
</div>

<!-- Modal -->
<div id="loginModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">

    <!-- Modal content-->
    <div class="modal-content">
            <div class="modal-body">
                <center>
                    <h3><strong>DTS</strong> Login</h3>
                    <img src="http://www.technogeeks.in/tech_uploads/2018/07/Digital-Marketing-strategy-Nigeria-Abule-Graphics.gif" width="100%">
                     
                </center>
                <form method="POST" action="{{route('login_dts')}}">
                {!! csrf_field() !!}
                    <label for="user_name">User Name</label>
                    <input type="text" name="txtUserName" id="un" autocomplete="off" class="form-control" required readonly onfocus="this.removeAttribute('readonly');" required/> <br />
                    <label for="password">Password</label>
                    <input type="password" name="txtPassword" id="password" readonly onfocus="this.removeAttribute('readonly')" autocomplete="off" class="form-control" required/>
                    <button type="submit" name="login" id="login-btn" class="btn btn-lg btn-primary btn-block">Log in</button>
                </form>
            </div>
            <div class="modal-footer">
                <center>All Rights Reserved 2015</center>
            </div>
        </div>
    </div>

  </div>
</div>

@endsection