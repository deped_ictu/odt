@extends('layouts.master')
@section ('title')
    DepEd | Marikina DTS | Open Tickets
@endsection

@section('content') 
<div class="row">
@include('inc.left_panel')
<style>
button, input[type="submit"], input[type="reset"] {
	background: none;
	color: inherit;
	border: none;
	padding: 0;
	font: inherit;
	cursor: pointer;
	outline: inherit;
}
</style>
<div class="col-sm-9">
	<div class="panel panel-info">
		<div class="panel-heading">
        <?php
            foreach($listopen_tckts as $listoopt){
                $listoopt['route'];
            }
            $office = $listoopt['route'];
        ?>
			<h3>List of Open Tickets (<?php echo $office; ?>)</h3>
			
		</div>
		<div class="panel-body">
            <table id="open" class="table table-striped table-bordered">
            <thead>
            <tr>
                    <th>Tracking No.</th>
					<th>Title</th>
                    <th>Date & Time</th>
                    <th>Action</th>
                    <th>Route</th>
                </tr>
            </thead>
            <tbody>
            @foreach($listopen_tckts as $listoopt)
            @if($listoopt['action']=='Closed' || $listoopt['action']=='Released' || $listoopt['action']=='Filed')
            @else
                <tr>     
                    <td>{{$listoopt['tn']}}</td>
                    <td>{{$listoopt['title']}}</td>
                    <td>{{$listoopt['date']}} - {{$listoopt['time']}}</td>
                    <td>{{$listoopt['action']}}</td>
                    <td>{{$listoopt['route']}}</td>

                </tr>
            @endif
            @endforeach
            </tbody> 
            </table>
		</div>
	</div>
</div>
</div><!--end of row-->
@endsection