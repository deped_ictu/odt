@extends('layouts.master')
@section('content')
<div class="row">
	<div class="col-md-12">
			<h2>Tickets from Schools</h2>
	</div>
</div>
<div class="row">
<div class="col-sm-3">
	    <div class="panel panel-info">
	        <div class="panel-heading">
	            
            <!--Search-->
    		<form action="{{route('search')}}" autocomplete="off" method="POST">
    		{!! csrf_field() !!}
                <div class="input-group input-group-lg">
                    <input type="text" id="search" class="form-control" name="txtsearch_ticket" placeholder="" required/>
                    <span class="input-group-btn">
                        <button class="btn btn-default" id="btnsearch">GO</button>
                    </span>
				</div>
				<div>
					<!--<input type="checkbox" name="searchtn" id="exactsearchtn"/><p>Search key words</p>	-->	 
                </div>
            </form>

			<?php
				$sdodepartments = array("SGOD","CID","ICTU","SDS","Finance","Admin","Supply","Cash", "Records","Personnel", "Health", "Legal", "PSDS", "ASDS");
				$Is_Sdo_account = false;
				if(in_array(Session::get('Department'), $sdodepartments)){
					//this is a SDO account
					$Is_Sdo_account = true;			
				}else{
					//This is a School Account
					$Is_Sdo_account = false;
				}

				$datacount = 0;
                $overduecount = 0;
			?>                   
            <!--End of Search-->	
	        </div><!--end of panel-heading-->
	        <div class="list-group">
				<!--<a href="#" class="list-group-item">Assigned Ticket</a>-->
				
				@if($Is_Sdo_account==false)
				<a href="#" class="list-group-item" data-toggle="modal" data-target="#create_ticket"><span class="glyphicon glyphicon-plus"></span> Create Ticket</a>
				<a class="list-group-item" href="{{route('closed_tickets')}}">Closed Tickets</a>
				@elseif($Is_Sdo_account==true)

				<a href="#" class="list-group-item" data-toggle="modal" data-target="#create_ticket"><span class="glyphicon glyphicon-plus"></span> Create Ticket</a>
				<a href="{{route('dashboard')}}" class="list-group-item" data-toggle="modal" data-target="#create_ticket"><span class="glyphicon glyphicon-glyphicon glyphicon-th-list"></span>&nbsp;Assigned Tickets
				<span class="badge badge-info" id="assigned">
					{{ $datacount}}
					</span>
				</a>
				<a class="list-group-item" href="#" data-toggle="modal" data-target="#dynamicmodal"><span class="glyphicon glyphicon-pause"></span> For Receipt 
					<span class="badge badge-info pending">
					{{ $pendingtick }}
					</span>
				</a>
				<a class="list-group-item" href="{{route('transferred')}}" data-toggle="modal" data-target="#"><span class="glyphicon glyphicon-pause"></span> Transferred Tickets
					<span class="badge badge-info pending">
					0
					</span>
				</a>
				
				@endif
				@if(Session::get('Department')=='Records')
				<a href="{{route('pending_submission')}}" class="list-group-item" data-toggle="modal" data-target="#"><span class="glyphicon glyphicon-pause"></span> Tickets From School <span class="badge badge-info pensub">
					{{ $pensub }}
					</span></a>
				<a href="{{route('pending_tickets_fromschool')}}" class="list-group-item" data-toggle="modal" data-target="#"> Clear Tickets</a>
				@else
				@endif
				<!--<a href="#" class="list-group-item" data-toggle="modal" data-target="#">Follow-up Ticket</a>-->
			</div>
		</div>
			@if($Is_Sdo_account==false)
			@else
			<table class="table table-striped table-bordered">
				<tr>
					<th style="text-align:center;">Open</th>
					<th style="text-align:center;"><a href="#" data-toggle="modal" data-target="#overdue_ticket">Overdue</a></th>
				</tr>
				<tr>
					<td align="center" ><span id="LabelOpen"></span></td>
					<td align="center"><span id='LabelOverdue'>0</span></td>
				</tr>
			</table>
			<!--EFFICIENCY COUNT-->
			<div class='panel panel-default' id="effpanel" style="background-image: linear-gradient(to right, #55efc4 , #00b894);">
			    <div class='panel-body'> <center><h1 id='effrat' style='color:white; text-shadow: 0px 1px 2px rgba(0,0,0,0.5);'></h1>
			    <h1 style='color:white; text-shadow: 0px 1px 2px rgba(0,0,0,0.5);' id='efficiencyrat'></h1>
			    </center></div>
			</div>
			@endif
	</div>
	  
			<div class="col-sm-9">
				<div class="panel panel-default">
					<div class="panel-heading">
					
						<h3>Pending Submission route to {{ Session::get('Department') }} office</h3>
					</div>
					<div class="panel-body"> 
					<table id="assigned_ticket" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th width="30">ID</th>
							<th width="100">TN</th>
							<th width="60">Title</th>
							<th width="30">Accept</th>
						</tr>
					</thead>
					<tbody>
					@foreach($submission as $pending_sub)
								<tr>     
									<td>{{ $pending_sub['id'] }}</td>     
										@if($pending_sub['status_2']=='Priority')
										<td style="background-color:#ffcccc; color:#000000; font-family:monospace;text-transform:uppercase;">{{ $pending['tn'] }}</td>
										@else 
										<td style="font-family:monospace;text-transform:uppercase;">{{ $pending_sub['tn'] }}</td>
										@endif
										<td>{{ $pending_sub['title'] }}</td>
									<td><button class='btn btn-primary pending_sub'  data-id="{{$pending_sub['id']}}" data-tn="{{$pending_sub['tn']}}"><span class="glyphicon glyphicon glyphicon-ok"></span></button></td>
									<input type="hidden" id="id_ps" name="txtid" />												
								</tr>
						@endforeach
					</tbody>  
					</table>
				</div>
			</div>
		</div>
</div><!--end of row-->
<script>
	var acceptPSURL="{{route('accept_PSticket')}}";
</script>

@endsection