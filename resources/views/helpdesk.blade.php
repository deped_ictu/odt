@extends('layouts.master')
@section ('title')
    DepEd | Marikina DTS | Helpdesk
@endsection

@section('content') 

<div class="container-fluid">
<div class="row">
	<div class="col-md-12">
			<h2>Helpdesk</h2>
	</div>
</div>
    <div class="row">
        <div class="col-sm-3">
            <div class="panel panel-info">
                <div class="panel-heading">
                    
                <!--Search-->
                <form action="{{route('search_hd')}}" autocomplete="off" method="POST">
                {!! csrf_field() !!}
                    <div class="input-group input-group-lg">
                        <input type="text" id="search_hd" class="form-control" name="txtsearch_hd" placeholder="" required/>
                        <span class="input-group-btn">
                            <button class="btn btn-default" id="btnsearch">GO</button>
                        </span>
                    </div>
                    <div>
                        <!--<input type="checkbox" name="searchtn" id="exactsearchtn"/><p>Search key words</p>	-->	 
                    </div>
                </form>

                <?php
                    $sdodepartments = array("SGOD","CID","ICTU","SDS","Finance","Admin","Supply","Cash", "Records","Personnel", "Health", "Legal", "PSDS", "ASDS");
                    $Is_Sdo_account = false;
                    if(in_array(Session::get('Department'), $sdodepartments)){
                        //this is a SDO account
                        $Is_Sdo_account = true;			
                    }else{
                        //This is a School Account
                        $Is_Sdo_account = false;
                    }
                ?>                   
                <!--End of Search-->	
                </div><!--end of panel-heading-->
                <div class="list-group">
                    <!--<a href="#" class="list-group-item">Assigned Ticket</a>-->
                    
                    @if($Is_Sdo_account==false)
                    <a href="#" class="list-group-item" data-toggle="modal" data-target="#create_ticket">Create Ticket</a>
                    <a class="list-group-item" href="{{route('closed_tickets')}}">Closed Tickets</a>
                    @elseif($Is_Sdo_account==true)
                    
                    <a href="{{route('helpdesk')}}" class="list-group-item">Assigned HD Tickets</a>
                    <a href="#" class="list-group-item" data-toggle="modal" data-target="#help_desk">Create HD Ticket</a>
                    @endif

                    <!--<a href="#" class="list-group-item" data-toggle="modal" data-target="#">Follow-up Ticket</a>-->
                </div>
            </div>
        </div>  
            
        <div class="col-sm-9"> 
        <h3 style="margin-top: 0;">Assigned to  {{ Session::get('Department') }} office</h3>
		<table id="hd_ticket" class="table table-striped table-bordered">
			<thead>
				<tr>
                    <th width="40">Id</th>
                    <th width="80">Date/Time</th>
					<th>Concern</th>
					<th width="80">Sender</th>
					<!--<th width="50" id="action">Actions</th>-->
				</tr>
			</thead>
			<tbody>
            @foreach($hdticket as $data_hd)
                <tr>
                    <td>{{$data_hd['id']}}</td>
                    <td>{{$data_hd['date_time']}}</td>
                    <td>{{$data_hd['remarks']}}</td>
                    <td>{{$data_hd['name']}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div><!--end of row-->
</div><!--end of container-->
@endsection