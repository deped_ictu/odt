@extends('layouts.master')

@section ('title')
    DepEd | Marikina DTS | Login
@endsection

@section('content')  

<style>
    td.text-resize {
        font-size: 1.8em;
    }
</style>

<div class="row">
	<div class="col-md-12">
		<h2>Dashboard</h2>
	</div>
</div>
		
<div class="row">
    <div class="col-sm-3">
        <div class="panel panel-info">
            <div class="panel-heading">
                <!--Search-->
                <!--<form class="navbar-form navbar-left" action="{{route('search')}}" autocomplete="off" method="POST">-->
                {!! csrf_field() !!}
                <div class="input-group input-group-lg">
                    <input type="text" id="search" class="form-control" name="txtsearch_ticket" placeholder="" required/>
                    <span class="input-group-btn">
                        <button data-toggle="modal" data-target="#searchmodal" class="btn btn-default" id="btnsearch">GO</button>
                    </span>
                </div>
            <!--</form>-->
            <script>
               $("#btnsearch").click(function(){
                  var searchkeyword = $("#search").val();
                  //alert(searchkeyword);
                  $.ajax({
                      type: "POST",
                      url: "{{route('search')}}",
                      data: {
                          _token:"{!! csrf_token() !!}",
                          txtsearch_ticket:searchkeyword},
                      success: function(data){
                          $("#ticketnumbersearched").html(searchkeyword + "<br>");
                          $("#doctitle").html("(not found)");
                          $("#off_origin").html("(not found)");
                          data = JSON.parse(data);
                          $("#search").val("");
                          //alert(JSON.stringify(data));
                          $("#searchresult").html("");
                         for(var i =0; i < data.length;i++){
                             
                          $fdate = new Date(data[i]["date_time"]);
                          $fdate = $fdate.getDay() + "/" + $fdate.getMonth() + "/" + $fdate.getFullYear();
                          $("#doctitle").html(data[i]["title"].toUpperCase());
                          $("#searchresult").append("<tr><td>" + $fdate + " - " + data[i]["time"] + "</td>" + 
                          "<td>" + data[i]['route']  + "</td>" + 
                          "<td><div class='rem'>" + data[i]['remarks'] +"</div></td>" + 
                          "<td>" + data[i]['status'] +"</td>" + 
                          "</tr>");
                          if(i <= data.length){
                              $("#off_origin").html(data[i]["origin"] + " (" + data[i]["cname"] + ")");
                          }
                         }
                      }
                  })
              })
            </script>
        <!--End of Search-->	
    
            <?php
            $sdodepartments = array("SGOD","CID","ICTU","SDS","Finance","Admin","Supply","Cash", "Records","Personnel", "Health", "Legal", "PSDS", "ASDS");
            $Is_Sdo_account = false;
            if(in_array(Session::get('Department'),$sdodepartments)){
                //this is a SDO account
                $Is_Sdo_account = true;			
            }else{
                //This is a School Account
                $Is_Sdo_account = false;
            }
            ?>                   
            
        </div><!--end of panel-heading-->
        
        <div class="list-group">
            <a href="{{route('dashboard')}}" class="list-group-item">Assigned Ticket</a>
            <a href="#" class="list-group-item" data-toggle="modal" data-target="#create_ticket">Create Ticket</a>
            <a class="list-group-item" href="#" data-toggle="modal" data-target="#dynamicmodal">Pending Ticket</a>
            @if(Session::get('Department')=='Records')
            <a href="{{route('pending_tickets_fromschool')}}" class="list-group-item" data-toggle="modal" data-target="#">Clear Tickets</a>
            @else
            @endif
            <!--<a href="#" class="list-group-item" data-toggle="modal" data-target="#">Follow-up Ticket</a>-->
        </div>
        
    </div><!--end of panel-info-->
</div><!--end of col-sm3-->
        
    <div class="col-sm-9">
        <h3 style="margin-top:0;">A new ticket has been created</h3>
        <table id="" class="table table-striped table-bordered">
            @foreach($createdticket as $data)						
                @if ($data['route']==Session::get('Department') )
                    @if ($data['status']!='Transferred' && $data['status']!='Closed' && $data['status']!='Pending Submission' && $data['status']!='Released' && $data['status']!='Pending Transfer')
                    
                        <tr>
                            <td width="20%">Ticket Number</td>
                            <td class="text-resize"  style="font-family:monospace;text-transform:uppercase;">{{$data['tn']}}</td>
                        </tr>
                        <tr>
                            <td>Document Origin</td>
                            <td class="text-resize">{{$data['origin']}}, {{$data['cname']}}</td>
                        </tr>
                        <tr>
                            <td>Date and Time</td>
                            <td class="text-resize">{{$data['date_time']}} - {{$data['time']}}</td>
                        </tr>
                        <tr>
                            <td>Remarks</td>
                            <td class="text-resize">{{$data['remarks']}}</td>
                        </tr>
                    @endif
                @endif
            @endforeach
        </table>
        <div>
        <button class="btn btn-primary assign_ticket" name="submit" data-toggle="modal" data-target="#assign_ticket" a href="#" class="assign_ticket" a href="#" data-id="{{$data['id']}}" data-title="{{$data['title']}}" data-status="{{$data['status']}}" data-remarks="{{$data['remarks']}}" data-tn="{{$data['tn']}}" data-agent="{{$data['agent']}}" data-status2="{{$data['status_2']}}" data-email="{{$data['email']}}" data-origin="{{$data['origin']}}" data-cname="{{$data['cname']}}" data-doctype="{{$data['doc_type']}}" data-route="{{$data['route']}}" data-date="{{$data['date_time']}}" data-time="{{$data['time']}}" data-dateout="{{$data['date_out']}}" data-timeout="{{$data['time_out']}}" data-toggle="modal" data-target="#assign_ticket">Assign Ticket</button>
        </div>
    </div><!--end of col-sm-9-->
</div>

<script>
//For dashborad popover

$(".btnaddinfo").click(function(){
var daysindepartment = $(this).data("dayscount");
var Loc = $(this).data("location");
var Rem = $(this).data("remarks");
var Stat = $(this).data("status");
if(Loc == "" && Loc == null){ Loc="(empty)";}
if(Rem.length ==0 ){ Rem="(empty)";}
if(Stat == "" && Stat == null){ Stat="(empty)";}
$("#d1").html(Loc);
$("#d2").html(Rem);
$("#d3").html(Stat); 

if(daysindepartment > 5){
$("#overduepanel").css("display","block");
}else{
$("#overduepanel").css("display","none");
}
$("#d0").html(daysindepartment); 

});


var closedTicketURL="{{route('closed_ticket')}}";
var submitTicketURL="{{route('submit_ticket')}}";

</script>

<script>

EfficiencyRating();
function EfficiencyRating(){
var open = $("#LabelOpen").html();
var overdue = $("#LabelOverdue").html();
var rating = open - overdue;
rating = rating / open * 100;
var n = rating.toFixed(2);
if(rating > 90){
$("#effrat").html('<i class="far fa-grin-stars"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
}else if(rating > 80){
$("#effpanel").css("background-image","linear-gradient(to right, #FFEB3B , #8BC34A)");
$("#effrat").html('<i class="far fa-smile"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
}else if(rating > 70){
$("#effrat").html('<i class="far fa-meh"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
}else if(rating > 50){
$("#effpanel").css("background-image","linear-gradient(to right, #F44336 , #FFC107)");
$("#effrat").html('<i class="far fa-frown"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
}else if(rating > 20){
$("#effrat").html('<i class="far fa-sad-tear"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
}

$("#efficiencyrat").html(n);
}     
setInterval(function()
{
EfficiencyRating();

}, 300);
</script>

@endsection