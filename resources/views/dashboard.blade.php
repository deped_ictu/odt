@extends('layouts.master')

@section ('title')
    DepEd | Marikina DTS | Dashboard
@endsection

@section('content')  

<div class="row">	
@if(Session::get('Department')=='Records')
	@include('inc/dashboard_records')

@elseif (Session::get('Department')=='CID' || Session::get('Department')=='SDS' || Session::get('Department')=='Finance' || Session::get('Department')=='Legal' || Session::get('Department')=='SGOD' || Session::get('Department')=='Personnel' || Session::get('Department')=='Health' || Session::get('Department')=='Admin' || Session::get('Department')=='Cash' || Session::get('Department')=='Supply' || Session::get('Department')=='PSDS' || Session::get('Department')=='ICTU')
@include('inc/dashboard_user') 

@else
	@include('inc/dashboard_school')

@endif
</div>


@endsection