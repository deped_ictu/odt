@extends('layouts.master')

@section ('title')
    DepEd | Marikina DTS | Login
@endsection

@section('content')

<div class="row">

	<div class="col-sm-12">
	    	<h2>Search Results</h2>
	</div>
</div>
@include('inc.left_panel')

<div class="col-sm-9">
<table class="table table-striped table-bordered" >
    <thead>
        <tr>
            <th>Tn</th>
            <th>Title</th>
            <th>Location</th>
            <th>Status</th>
            <th>Date / Time</th>
            
        </tr>
    </thead>
    <tbody>
    @foreach ($searchticket as $data)
        <tr>
            <td style="font-family:monospace;text-transform:uppercase;"><a data-toggle="modal" data-id_search="{{ $data['tn'] }}" class="searchtt" data-target="#doc_trail_modal" href="#">{{ $data['tn'] }}</a></td>
            <td>{{ $data['title'] }}</td>
            <td>{{ $data['route'] }}</td>
            <td>{{ $data['action'] }}</td>
            <td>{{ $data['date'] }} {{ $data['time'] }}</td>
            
        </tr>
    @endforeach
    </tbody>
</table>

<!--modal for search result-->
<div class="modal fade" role="dialog" id="doc_trail_modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <h5 class="modal-title" id="exampleModalLabel">Ticket Number: <strong style='color: #03A9F4;' id='ticketnumbersearched_2'></strong>Title: <strong id='doctitle_2'></strong><br>From: <strong id='off_origin_2'></strong></h5>
                
			</div>
			<div class="modal-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Date/Time</th>
                                <th>Location</th>
                                <th>Remarks</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                            <tbody id="trail_res">

                            </tbody>
                    </table>
			</div>
        </div>
	</div>
</div>

<script>
         $(".searchtt").click(function(){
                 $("#trail_res").html("");
                 $("#receive_ticket").html("");
            //  alert($(this).data("id_search"));
            var searchkeyword = $(this).data("id_search");
            //alert(searchkeyword);
            $.ajax({
                type: "POST",
                url: "{{route('search_trail')}}",
                data: {
                    _token:"{!! csrf_token() !!}",
                    txtsearch_tickettn:searchkeyword},
                    success: function(data){
                   // $("#coco").append(data);
                    $("#ticketnumbersearched_2").html(searchkeyword + "<br>");
                    $("#doctitle_2").html("(not found)");
                    $("#off_origin_2").html("(not found)");
                    
                    // alert(JSON.stringify(data));
                    data = JSON.stringify(data);
                   data = JSON.parse(data);
                   //alert(data);
                   for(var i =0; i < data.length; i++){
                       
                    $fdate = new Date(data[i]["date"]);
                    $fdate = $fdate.getDay() + "/" + $fdate.getMonth() + "/" + $fdate.getFullYear();
                    $("#doctitle_2").html(data[i]["title"]);
                    $("#trail_res").append("<tr><td>" + data[i]["date"] + " - " + data[i]["time"] + "</td>" + 
                    "<td>" + data[i]['route']  + "</td>" + 
                    "<td><div class='rem'>" + data[i]['remarks'] +"</div></td>" + 
                    "<td>" + data[i]['action'] +"</td>" + 
                    "</tr>");
                    if(i <= data.length){
                        $("#off_origin_2").html(data[i]["sender"] + " (" + data[i]["cname"] + ")");
                    }
                   }
                }
            })
         })
                                   
</script>


</div>

@endsection








