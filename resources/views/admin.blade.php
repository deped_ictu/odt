@extends('layouts.master')
@section ('title')
    DepEd | Marikina DTS | Admin
@endsection

@section('content') 
<div class="row">
@include('inc.left_panel')
<style>
button, input[type="submit"], input[type="reset"] {
	background: none;
	color: inherit;
	border: none;
	padding: 0;
	font: inherit;
	cursor: pointer;
	outline: inherit;
}
</style>
<div class="col-sm-9">
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3>Open Tickets </h3>
		</div>
		<div class="panel-body">
            <table id="open" class="table table-striped table-bordered">
            <thead>
            <tr>
                    <th>Office Name</th>
					<th>Total # of Open Tickets</th>
                </tr>
            </thead>
            <tbody>
            @foreach($countopentkts as $count_tick)
                <tr>     
                    <td>{{$count_tick['route']}}</td>
                    <!--<td><a href="" class="list_open" data-route="{{$count_tick['route']}}" data-title="{{$count_tick['title']}}" data-tn="{{$count_tick['tn']}}" data-date="{{$count_tick['time']}}" data-time="{{$count_tick['time']}}" data-toggle="modal" data-target="#list_opentkts">{{$count_tick['opentickets']}}</a></td>
                    <td><a href="{{route('open_tickets')}}" class="list_open" data-route="{{$count_tick['route']}}">{{$count_tick['opentickets']}}</a></td>-->

                    <td>
						<form method="POST" action="{{route('open_tickets')}}">
						<input type="hidden" name="route" value="{{$count_tick['route']}}" />
						{!! csrf_field() !!}
						<button type="submit"><a heref="#">{{$count_tick['opentickets']}}</a></button>
						</form>
					</td>
                </tr>
            @endforeach
            </tbody> 
            </table>
		</div>
	</div>
</div>
</div><!--end of row-->

<!--<div class="modal fade" role="dialog" id="list_opentkts">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
            <h3 class="modal-title">List of Open Ticket</strong></h3>
                
			</div>
			<div class="modal-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Tracking No.</th>
                                <th>Title</th>
                                <th>Date</th>
                                <th>Time</th>
                            </tr>
                        </thead>
                            <tbody id="open_tkts_list">

                            </tbody>
                    </table>
			</div>
        </div>
	</div>
</div>

<script>
         $(".list_open").click(function(){
            $("#open_tkts_list").html("");
            var route = $(this).data("route");
            alert(route);
            $.ajax({
                type: "POST",
                url: "{{route('open_tickets')}}",
                data: {
                    _token:"{!! csrf_token() !!}",
                    route_tosend:route,
                    },
                    success: function(data){
                    // alert(data);
                     //alert(JSON.stringify(data));
                    //data = JSON.stringify(data);
                   data = JSON.parse(data);
                   //alert(data);
                   for(var i =0; i < data.length; i++){
                       
                    $("#open_tkts_list").append("<tr><td>" + data[i]["tn"] + "</td>" + 
                    "<td>" + data[i]['title']  + "</td>" + 
                    "<td>" + data[i]['date'] + "</td>" +
                    "<td>" + data[i]['time'] + "</td>" +
                    "</tr>");
                   }
                }
            })
         })
                                   
</script>-->



@endsection