@include('inc.left_panel')
<style>
button, input[type="submit"], input[type="reset"] {
	background: none;
	color: inherit;
	border: none;
	padding: 0;
	font: inherit;
	cursor: pointer;
	outline: inherit;
}
</style>
<div class="col-sm-9">
    	
	<div class="panel panel-info">
		<div class="panel-heading">
		<h2>OUTGOING TICKETS</h2>			
		</div>
		<div class="panel-body">

    		<table id="assigned_ticket" class="table table-striped table-bordered">
    			<thead>
    				<tr>
    					<th width="30">ID</th>
    					<th width="30">Company Name</th>
    					<th width="30">TN</th>
    					<th width="40">Document Type</th>
    					<th>Title</th>
    					<th width="60">Remarks</th>
    
    					<!--<th width="50">Status</th>-->
    				</tr>
    			</thead>
    			<tbody>
    				@foreach($ticket as $data)						
    					@if ($data['route']==Session::get('Department') )
	                    @if($data['action']=='' || $data['action']=='Transferred' || $data['action']=='Filed' || $data['action']=='Closed')
						@else    							
						<tr>   
						            <td>{{ $data['id'] }}</td>
    								<td>{{ $data['cname'] }}</td>
    								<td style="text-transform:uppercase;">
    									<form method="POST" action="{{route('ticket')}}">
    									<input type="hidden" name="tn" value="{{$data['tn']}}" />
    									{!! csrf_field() !!}
    									<button type="submit" style="text-transform:uppercase;"><a heref="#">{{ $data['tn'] }}</a></button>
    									</form>
    								</td>
    								<td>{{ $data['doc_type'] }}</td>
    								<td style="text-transform:capitalize;">{{ $data['title'] }}</td>
    								<td>{{$data['remarks']}} </td>
    							</tr>
    						@endif
    					@endif
    				@endforeach
    			</tbody>  
    		</table>
    	</div>
    </div>
</div>
<!--tickets trail-->
<script>
/*var tn = "";
$('#result').hide();
$(".searchtt").click(function(){
    tn = $(this).data("tn");
   //alert(tn);
});
$("#tn").click(function(){
	var _token=$('input[name=_token]').val();
	var that = this;
	$.ajax({
		method:'post',
		url: "{{route('ticket_details')}}",
		data:{
			'_token':_token,
			'tn':tn,			
		},
	
		success:function(data){
			data = JSON.stringify(data);
			//data = JSON.parse(data);
			alert(data);
		$(that).closest('#default').hide();
		$('#result').show();
		

		//window.location="/ticket";
		//console.log(msg['message']);
		}
	})
});*/
		
</script>
