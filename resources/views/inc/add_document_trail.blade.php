<script>
$('select').on('change', function() {
  alert( this.value );
});
</script>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h4><b>ADD DOCUMENT TRAIL</b></h4>
	</div>
	<div class="panel-body">
				<form method="post" id="transfer" action="{{route('addtrail')}}">	
				{!! csrf_field() !!}	
				<input type="hidden" name="txttn" value="{{ $data['tn'] }}" />
				<input type="hidden" name="txtref_no" value="{{ $data['ref_no'] }}" />
				<input type="hidden" name="txttitle" value="{{ $data['title'] }}" />
				<input type="hidden" name="txtstatus" value="{{ $data['status'] }}" />
				<input type="hidden" name="txtsender" value="{{ $data['sender'] }}" />
				<input type="hidden" name="txtcname" value="{{ $data['cname'] }}" />
				<input type="hidden" name="txtdoc_type" value="{{ $data['doc_type'] }}" />

					<div class="form-login">
							<div class="row">
								<div class="col-md-3">
									<div class="form-group">
										<label class="input-group">Action</label>
										<select class="form-control" name="txtaction" id="action">
											<option value="Received">Received</option>
											<option value="Transferred">Transfer</option>
											<option value="Filed">Filed</option>
											<option value="Referred">Referred</option>
											<option value="Closed">Closed</option>
											@if(Session::get('Department')=='Records')
											<option value="Released">Released</option>
											@else
											@endif
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="input-group">Document Route</label>
										<select class="form-control" name="txtroute" id="route" required>
										@if(Session::get('Department')=='Personnel')
											<option value="{{Session::get('Department')}}">{{Session::get('Department')}}</option>
											<option value="SDS">SDS</option>
											<option value="ASDS">ASDS</option>
											<option value="Admin">Admin</option>
											<option value="Finance">Finance</option>   
											<option value="Legal">Legal</option>
											<option value="CID">CID</option>
											<option value="SGOD">SGOD</option>
											<option value="PSDS">PSDS</option>
											<option value="Cash">Cash</option>
											<option value="Records">Records</option>
											<option value="Supply">Supply and Property</option>
											<option value="Health and Nutrition">Health and Nutrition</option>
											<option value="ICTU">ICTU</option>
										@else
											<option value="">Select Office</option>
											<option value="SDS">SDS</option>
											<option value="ASDS">ASDS</option>
											<option value="Admin">Admin</option>
											<option value="Finance">Finance</option>   
											<option value="Legal">Legal</option>
											<option value="Personnel">Personnel</option>
											<option value="CID">CID</option>
											<option value="SGOD">SGOD</option>
											<option value="PSDS">PSDS</option>
											<option value="Cash">Cash</option>
											<option value="Records">Records</option>
											<option value="Supply">Supply and Property</option>
											<option value="Health and Nutrition">Health and Nutrition</option>
											<option value="ICTU">ICTU</option>
										@endif
										</select>
									</div>	
								</div>
								<div class="col-md-6">
									<div class="form-group">				
										<label class="input-group">Remarks:</label>
										<input class="form-control" rows="5" id="comment" name="txtremarks" autocomplete="off"/>
									</div>
								</div>
									<button type="submit" class="btn btn-primary pull-right" name="add_trail" id="add_trail" >Save</button>
							</div><!--end of row-->
					</div><!--end of form login-->
                </form>
	</div><!--end of panel body-->				
</div><!--panel-->