<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<?php
	$sdodepartments = array("SGOD","CID","ICTU","SDS","Finance","Admin","Supply","Cash", "Records","Personnel", "Health", "Legal", "PSDS", "ASDS");
		$Is_Sdo_account = false;
		if(in_array(Session::get('Department'), $sdodepartments)){
		//this is a SDO account
		$Is_Sdo_account = true;			
		}else{
		//This is a School Account
		$Is_Sdo_account = false;
	}
?>  
<div class="col-sm-3 hide_print">
	<div class="panel" style="background-color:#ac3b61; color:white;">
	    <div class="panel-body">
            <!--Search-->
    		<form action="{{route('search')}}" autocomplete="off" method="POST">
    		{!! csrf_field() !!}
                <div class="input-group">
                    <input type="text" id="search_etn" class="form-control" name="txtsearch_tickettn" placeholder="Search Tracking Number" required/>
                    <span class="input-group-btn">
                        <button class="btn btn-default tn" id="btnsearchtn">GO</button>
                    </span>
                </div>
				<!--<br>
				<div class="input-group">
                    <input type="text" id="search_kw" class="form-control" name="txtsearch_ticketkw" placeholder="Search key words" required/>
                    <span class="input-group-btn">
                        <button class="btn btn-default kw" id="btnsearchkw">GO</button>
                    </span>
                </div>-->
            </form>
	</div>
	<div class="list-group hide_print" style="background-color:#ac3b61;">
        @if(session('Department')=='Records')	
        <a class="list-group-item" href="{{route('dashboard')}}" style="background-color:#ac3b61; color:white;"><span class="glyphicon glyphicon-plus-sign" style="color:white; font-size:20px;"></span> Create Ticket</a>	
        <a class="list-group-item" href="{{route('incoming_tickets')}}" style="background-color:#ac3b61; color:white;"><span class="glyphicon glyphicon-check" style="color:white; font-size:20px;"></span> Incoming Tickets <span class="badge badge-light" style="background-color:white;color:black;">{{$rec_incoming}}</span></a>
        <a class="list-group-item" href="{{route('outgoing_tickets')}}" style="background-color:#ac3b61; color:white;"><i class="fa fa-share-square-o" style="color:white; font-size:20px;"></i> Outgoing Tickets <span class="badge badge-light" style="background-color:white;color:black;">{{ $rec_outgoing }}</span></a>
        <a href="{{route('pending_tickets')}}" class="list-group-item" style="background-color:#ac3b61; color:white;"><span class="glyphicon glyphicon-check" style="color:white; font-size:20px;"></span> Transferred Tickets <span class="badge badge-light" style="background-color:white;color:black;">{{ $count_p }}</span></a>
        @elseif(session('Department')!='Records' AND $Is_Sdo_account=='false')	
        <a class="list-group-item" href="{{route('dashboard')}}" style="background-color:#ac3b61; color:white;"><span class="fa fa-share-square-o" style="color:white; font-size:20px;"></span> Outgoing Tickets <span class="badge badge-light" style="background-color:white;color:black;">{{ $outgoing }}</span></a>	
            <a href="{{route('pending_tickets')}}" class="list-group-item" style="background-color:#ac3b61; color:white;"><span class="glyphicon glyphicon-check" style="color:white; font-size:20px;"></span> Incoming Tickets <span class="badge badge-light" style="background-color:white;color:black;">{{ $count_p }}</span></a>	
        @else	
        <a class="list-group-item" href="{{route('school_tickets')}}" style="background-color:#ac3b61; color:white;"><span class="glyphicon glyphicon-th-list" style="color:white; font-size:20px;"></span>My Tickets</a>	
        @endif
        @if(session('Department')=='ICTU')
        <a class="list-group-item" href="{{route('admin')}}" style="background-color:#ac3b61; color:white;"><span class="glyphicon glyphicon-open" style="color:white; font-size:20px;"></span> Open Tickets <span class="badge badge-light" style="background-color:white;color:black;"></span></a>
        <a class="list-group-item" href="{{route('all_tickets')}}" style="background-color:#ac3b61; color:white;"><span class="glyphicon glyphicon-th-list" style="color:white; font-size:20px;"></span> All Tickets <span class="badge badge-light" style="background-color:white;color:black;"></span></a>
        @else
        @endif
		<a href="{{route('print_logs')}}" class="list-group-item" style="background-color:#ac3b61; color:white;"><span class="glyphicon glyphicon-print" style="color:white; font-size:20px;"></span> Print Logs</a>

	</div>
    </div>
</div>
