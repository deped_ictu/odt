
@include('inc.left_panel')

<div class="col-md-9">
	<h2>Dashboard</h2>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4><b>CREATE TICKET</b><p class="LC pull-right" style="font-size: 15px; margin:0px;">
			</h4>
		</div>
		<div class="panel-body">
				<form method="post" id="create" action="{{route('addticket')}}">	
				{!! csrf_field() !!}	
					<div class="form-login">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="input-group"><span class="form-input-required" style="font-weight: normal; color:red;">*</span>
Name of Sender</label>
										<input type="text" id="origin" name="sender" class="form-control" placeholder="First, Middle Name,  Last Name" autocomplete="off" required />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="input-group">Company Name</label>
										<input type="text" id="cname" name="cname" class="form-control" placeholder="Company Name" autocomplete="off" />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="input-group"><span class="form-input-required" style="font-weight: normal; color:red;">* </span>Tracking No.</label>
										<input type="text" id="txttnt" name="tn" class="form-control" placeholder="Tracking #" autocomplete="off" required />
									</div>
								</div>
							</div> 
							<div class="row">
							<div class="col-md-5">
									<div class="form-group">
										<label class="input-group"><span class="form-input-required" style="font-weight: normal; color:red;">* </span>Title</label>
										<input type="text" id="" name="title" class="form-control" placeholder="Title" autocomplete="off" placeholder="" required />
									</div>
								</div>
							<div class="col-md-4">
									<div class="form-group">
										<label class="input-group"><span class="form-input-required" style="font-weight: normal; color:red;">* </span>Type of Document</label>
										<select class="form-control" name="doc_type" required>
											<option value="">Select Document Type</option>
											<option value="Admin">Admin</option>
											<option value="Finance">Finance</option>
											<option value="Legal">Legal</option>
											<option value="Personnel">Personnel</option>
											<option value="Private">Private</option>
										</select>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="input-group"><span class="form-input-required" style="font-weight: normal; color:red;">* </span>Action</label>
										<select class="form-control" name="action" required>
											<option value="Received">Received</option>
											<option value="Transferred">Transfer</option>
											<option value="Released">Released</option>
											<option value="Filed">Filed</option>
											<option value="Without Action">Without Action</option>
											<option value="Referred">Refer</option>
										</select>
									</div>
								</div>

							</div> 
							<div class="row">
							<div class="col-md-4">
									<div class="form-group">
										<label class="input-group"><span class="form-input-required" style="font-weight: normal; color:red;">* </span>Document Route</label>
										<select class="form-control" name="route" required>
											<option value="SDS">SDS</option>
											<option value="ASDS">ASDS</option>
											<option value="Admin">Admin</option>
											<option value="Finance">Finance</option>   
											<option value="Legal">Legal</option>
											<option value="Personnel">Personnel</option>
											<option value="CID">CID</option>
											<option value="SGOD">SGOD</option>
											<option value="PSDS">PSDS</option>
											<option value="Cash">Cash</option>
											<option value="Records">Records</option>
											<option value="Supply">Supply and Property</option>
											<option value="Health and Nutrition">Health and Nutrition</option>
											<option value="ICTU">ICTU</option>
										</select>
									</div>	
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="input-group">Status</label>
										<select class="form-control" name="status">
											<option value="Normal">Normal</option>
											<option value="Priority">Priority</option>
										</select>
									</div>	
								</div>
								<div class="col-md-5">
									<div class="form-group">				
										<label class="input-group">Remarks:</label>
										<input class="form-control" rows="5" id="comment" name="remarks" placeholder="Remarks" autocomplete="off"/>
									</div>
								</div>
							</div>
					</div><!--end of form login-->
		</div><!--end of panel body-->
					<div class="panel-footer clearfix">				   
						<button type="submit" class="btn btn-primary pull-right" name="create_ticket" id="create_ticket" value="Submit">Create</button>
					</div>
				</form>
</div><!--end of col-md-9-->
		<div class="panel panel-default">
			<div class="panel-heading">
			<h4><strong>RECENTLY CREATED TICKETS</strong></h4>
			</div>
			<div class="panel-body">
				<table id="assigned_ticket" class="table table-striped table-bordered">
					<thead>
						<tr>
							<!--<th width="30">ID</th>-->
							<th width="30">Company Name</th>
							<th width="30">TN</th>
							<th width="40">Document Type</th>
							<th>Title</th>
							<th width="60">Remarks</th>
							<th width="20"><button type="button" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-pencil"></span></button></th>
							<th width="20"><button type="button" class="btn btn-xs btn-danger" style="text-align:center;">
  							<span class="glyphicon glyphicon-trash"></span>
							</button></th>
							<!--<th width="40"><input type="checkbox" name="ticket" class="selectall" id="select_all"/></th>-->
							<!--<th width="50">Status</th>-->
						</tr>
					</thead>
					<tbody>
					@foreach($recent_t as $recent_tickets)
							@if ($recent_tickets['action']=='')	
								<tr>     
									<td>{{$recent_tickets['cname']}}</td>
									<td>{{$recent_tickets['tn']}}</td>
									<td>{{$recent_tickets['doc_type']}}</td>
									<td>{{$recent_tickets['title']}}</td>
									<td>{{$recent_tickets['remarks']}}</td>

									<td><a class="uptworow" data-tn="{{ $recent_tickets['tn'] }}" data-title="{{$recent_tickets['title']}}" data-cname="{{$recent_tickets['cname']}}" data-dt="{{$recent_tickets['doc_type']}}" data-route="{{$recent_tickets['doc_type']}}" href="" data-toggle="modal" data-target="#edit_tn"><span class="glyphicon glyphicon-pencil"></span></a></td>

									<td><a href="#" class="btndelete_ticket btn-danger" id="delete_ticket" data-tn="{{$recent_tickets['tn']}}"><button type="button" class="btn btn-xs btn-danger">
									<span class="glyphicon glyphicon-trash"></span>
									</button></a></td>
									
									<!--<td><input class="trans_ticket" type="checkbox" name="transfer" id="checked" value="{{$recent_tickets['id']}}" data-id ="{{$recent_tickets['id']}}" data-user="{{$recent_tickets['user']}}"></td>-->
									<input type="hidden" name="txttn" id="deltn" value="{{$recent_tickets['tn']}}"/> 
								</tr>
							@endif
							<input type="hidden" name="txttn" value="{{ $recent_tickets['tn'] }}" />
							<input type="hidden" name="txtref_no" value="{{ $recent_tickets['ref_no'] }}" />
							<input type="hidden" name="txttitle" value="{{ $recent_tickets['title'] }}" />
							<input type="hidden" name="txtstatus" value="{{ $recent_tickets['status'] }}" />
							<input type="hidden" name="txtsender" value="{{ $recent_tickets['sender'] }}" />
							<input type="hidden" name="txtcname" value="{{ $recent_tickets['cname'] }}" />
							<input type="hidden" name="txtdoc_type" value="{{ $recent_tickets['doc_type'] }}" />
							<input type="hidden" name="txtremarks" value="{{ $recent_tickets['remarks'] }}" />
					@endforeach
					</tbody>  
				</table>	
			</div>
			<div class="panel-footer clearfix">		
				<button type="submit" class="btn btn-info pull-right transfer" name="transfer" id="transfer" value="Submit" style="margin-right:10px;color:black;"><b>Transfer</b></button>
			</div>
		</div>
	</div>
 <br>

<script>
	var TransferURL="{{route('transfer')}}";
	var DeleteURL = "{{route('delete_ticket')}}";
	var EditURL = "{{route('editthesametn')}}";
//local storage
	/*$('#update').click(function(){
		var tn=$(this).data("tn");
		localStorage.setItem('tn_cache', tn);
		//alert(tn);
	})

$(document).ready(function(){
	//alert(localStorage.getItem('tn_cache')); //call the local storage

	//show button if checkbox is check
	var	button = $("#transfer").hide();
		btn = $('input[name="transfer"]').click(function() {
                button.toggle( btn.is(":checked") );
        });
	//
	//check all checkbox
	$('#select_all').change(function() {
		if($(this).is(':checked')) {
			$("input[type='checkbox']").attr('checked', 'checked');
			button = $("#transfer").show();
		} else {
			$("input[type='checkbox']").removeAttr('checked');
			button = $("#transfer").hide();
		}
	});
	//$("input[type='checkbox']").not('#select_all').change( function() {
		//$('#select_all').removeAttr('checked');
	//});
	//
    $('#transfer').on('click', function(){
        // Declare a checkbox array
        var transferArray = [];
        
        // Look for all checkboxes that have a specific class and was checked
        $(".trans_ticket:checked").each(function() {
            transferArray.push($(this).data('id'));
			//alert($(this).data('id'));
        });
        // declare variable for array
        var selected;
        selected = transferArray;
		//alert(selected.length);

		//data to be inserted in the database
		var token= $('input[name=_token]').val();
		id = $(this).data('id');
		action = 'Transferred';
		//get the time
		var t = new Date();
		var hour = t.getHours();
		var min = t.getMinutes();
		var sec = t.getSeconds();
		var time = hour + ':' + min + ':' + sec;
		//get the date
		var d = new Date();
		var year = d.getFullYear();
		var month =  ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
		var day = d.getDate();
		var date = year + '-' + month[d.getMonth()] + '-' + day;

		user = $(this).data('user');
// Check if there are selected checkboxes
	//one checkbox selected
		if(selected.length==1) {
			id=selected[0];
			//ajax
			$.ajax({
				type:"POST",
				url: TransferURL,
				data: {
					_token:token,
					id:id,
					action:action,
					date:date,
					time:time,
					user:user 
					
					},
				success: function(data)
				{
				window.location="/dashboard";
				}
			})
		} 
	//two or more selected checkbox
		else if (selected.length>=2){
			for(var i=0; i < selected.length; i++){
				trans_select = selected[i];
				//ajax
				$.ajax({
					type:"POST",
					url: TransferURL,
					data: {
						_token:token,
						id:trans_select,
						action:action,
						date:date,
						time:time,
						user:user 						
						},
					success: function(data)
					{
						//alert(data)
					//window.location="/pending_tickets";
					}
				})
				setTimeout(function() {
					window.location="/dashboard";
				}, 1000);
			}
			
		}
	//else if (selected.length==0){
		//$("#accept").hide();	
        //}
    });
});
*/
</script>
<!--modal edit-->
<div class="modal fade" id="edit_tn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-sm">
	<div class="modal-header modal-primary">
        <h3 class="modal-title">EDIT TICKET
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
		</h3>
      </div>
      <div class="modal-body">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <input type="hidden" name ="txtid" id="txtids2" />
			<input type="hidden" name="txtuser" id="txtagent" value="{{Session::get('Firstname')}} {{Session::get('Lastname')}}" />
        <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                  <label for="add">&nbsp;<b>Tracking No.</b>&nbsp;&nbsp;</label>
                  <input type="text" name="txttn" id="txttno2" class="form-control" />
              </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
						<div class="form-group">
                <label for="add">&nbsp;<b>Title</b>&nbsp;&nbsp;</label>
                <input type="text" name="txttitle" id="txttitle2" class="form-control" />
					  </div>	
          </div>
        </div>
				<div class="row">
          <div class="col-md-12">
						<div class="form-group">
                <label for="add">&nbsp;<b>Company Name</b>&nbsp;&nbsp;</label>
                <input type="text" name="txtcname" id="txtcname2" class="form-control" />
					  </div>	
          </div>
        </div>
				<div class="row">
          <div class="col-md-12">
						<div class="form-group">
                <label for="add">&nbsp;<b>Document Type</b>&nbsp;&nbsp;</label>
								<select class="form-control" name=""txtdoc_type" id="txtdoc_type2" required>
											<option value="">Select Document Type</option>
											<option value="Admin">Admin</option>
											<option value="Finance">Finance</option>
											<option value="Legal">Legal</option>
											<option value="Personnel">Personnel</option>
											<option value="Private">Private</option>
										</select>		
					  </div>	
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="btnsave">Save</button>
      </div>
    </div>
  </div>
</div>
