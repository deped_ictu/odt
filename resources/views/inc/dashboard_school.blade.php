
@include('inc.left_panel')

<div class="col-md-9">
<h2>Dashboard</h2>

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4><b>CREATE TICKET</b></h4>
		</div>
		<div class="panel-body">
				<form method="post" id="create" action="{{route('addticket_SC')}}">
				<?php
					
				//date
				date_default_timezone_set('Asia/Manila');
				$date_time = date('ymdHis');
				$year = date('y');
				$cname = session('Department');
				
				   foreach($lasttn as $LastTN){
					$LastTN['id'];
					}

					if(empty($LastTN['id'])){
						$xxx = 0;
					}
					else{
						$xxx = $LastTN['id'];
					}
					$dx = $xxx;
					$xx = $dx + 1; 

				$ref_no = $cname.''.$year;
				//$result = $data1 . ' ' . $data2;
				?>
				<input type="hidden" name="ref_no" value="<?php echo $ref_no.''.$xx; ?>" />
				<input type="hidden" name="cname" value="{{session('Department')}}" />
				{!! csrf_field() !!}	
					<div class="form-login">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="input-group">Sender</label>
										<input type="text" id="origin" name="sender" class="form-control" placeholder="First, Middle Name,  Last Name" autocomplete="off" required />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="input-group">Status</label>
										<select class="form-control" name="status">
											<option value="Normal">Normal</option>
											<option value="Priority">Priority</option>
										</select>
									</div>	
								</div>
							</div> 
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="input-group">Title</label>
										<input type="text" id="" name="title" class="form-control" placeholder="" autocomplete="off" placeholder="" required />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">				
										<label class="input-group">Remarks:</label>
										<input class="form-control" rows="5" id="comment" name="remarks" autocomplete="off"/>
									</div>
								</div>
							</div> 
					</div><!--end of form login-->
		</div><!--end of panel body-->
					<div class="panel-footer clearfix">				   
						<button type="submit" class="btn btn-primary pull-right" name="create_ticket" id="create_ticket" value="Submit">Create</button>
					</div>
				</form>
</div><!--end of col-md-9-->
<div class="panel panel-default">
			<div class="panel-heading">
			<h4><strong>RECENTLY CREATED TICKETS</strong></h4>
			</div>
			<div class="panel-body">
				<table id="assigned_ticket" class="table table-striped table-bordered">
					<thead>
						<tr>
							<!--<th width="30">ID</th>-->
							<th width="30">Company Name</th>
							<th width="30">TN</th>
							<th width="40">Document Type</th>
							<th>Title</th>
							<th width="60">Remarks</th>
							<!--<th width="40"><input type="checkbox" name="ticket" class="selectall" id="select_all"/></th>
							<th width="50">Status</th>-->
						</tr>
					</thead>
					<tbody>
					@foreach($recentSA as $recent_SA)
								<tr>     
									<td>{{$recent_SA['cname']}}</td>
									<td>{{$recent_SA['tn']}}</td>
									<td>{{$recent_SA['doc_type']}}</td>
									<td>{{$recent_SA['title']}}</td>
									<td>{{$recent_SA['remarks']}}</td>
									<!--<td><input class="trans_ticket" type="checkbox" name="transfer" id="checked" value="{{$recent_SA['id']}}" data-id ="{{$recent_SA['id']}}" data-user="{{$recent_SA['user']}}"></td>-->
								</tr>
					@endforeach
					</tbody>  
				</table>	
			</div>
		</div>
</div>