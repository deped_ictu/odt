<div class="panel panel-primary">
	<div class="panel-heading">
		<h4><b>ADD DOCUMENT TRAIL</b></h4>
	</div>
    <script>
  $("input").keyup(function () {
       if ($(this).val()) {
          $("button").show();
       }
       else {
          $("button").hide();
       }
    });
    $("button").click(function () {
       $("input").val('');
       $(this).hide();
    });
    </script>
	<div class="panel-body">
				<input type="text" name="_token" value="{{ csrf_token() }}" />
				<input type="text" name="txtref_no" id="refno" value="{{ $data['ref_no'] }}" />
				<input type="text" name="txttitle" value="{{ $data['title'] }}" />
				<input type="hidden" name="txtstatus" value="{{ $data['status'] }}" />
				<input type="hidden" name="txtsender" value="{{ $data['sender'] }}" />
				<input type="hidden" name="txtcname" value="{{ $data['cname'] }}" />
				<input type="hidden" name="action" value="{{ $data['action'] }}" />
				<input type="hidden" name="date" value="{{ $data['date'] }}" />
				<input type="hidden" name="time" value="{{ $data['time'] }}" />
				<input type="hidden" name="remarks" value="{{ $data['remarks'] }}" />
				<input type="hidden" name="user" value="{{ $data['user'] }}" />
				<input type="hidden" name="route" value="{{ $data['route'] }}" />
					<div class="form-login">
							<div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="input-group">Tracking No.</label>
                                            <input type="text" class="form-control" name="txttn" id="txttns"placeholder="Tracking No."/>
                                    </div>
                                </div>
                                <div class="col-md-4">
									<div class="form-group">
										<label class="input-group">Document Type</label>
										<select class="form-control" name="txtdoc_type">
                                            <option>Choose Document Type</option>
											<option value="Admin">Admin</option>
											<option value="Legal">Legal</option>
											<option value="Personnel">Personnel</option>
											<option value="Finance">Finance</option>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="input-group">Action</label>
										<select class="form-control" name="txtaction">
                                            <option>Choose Action</option>
											<option value="Transferred">Transfer</option>
											<option value="Received">Received</option>
											<option value="Released">Released</option>
											<option value="Filed">Filed</option>
											<option value="Without Action">Without Action</option>
											<option value="Referred">Referred</option>
											<option value="Closed">Closed</option>
										</select>
									</div>
								</div>
                            </div>
                            <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="input-group">Document Route</label>
										<select class="form-control" name="txtroute">
											<option>Select Office</option>
											<option value="SDS">SDS</option>
											<option value="ASDS">ASDS</option>
											<option value="Admin">Admin</option>
											<option value="Finance">Finance</option>   
											<option value="Legal">Legal</option>
											<option value="Personnel">Personnel</option>
											<option value="CID">CID</option>
											<option value="SGOD">SGOD</option>
											<option value="PSDS">PSDS</option>
											<option value="Cash">Cash</option>
											<option value="Records">Records</option>
											<option value="Supply">Supply and Property</option>
											<option value="Health and Nutrition">Health and Nutrition</option>
											<option value="ICTU">ICTU</option>
										</select>
									</div>	
								</div>
								<div class="col-md-6"
									<div class="form-group">				
										<label class="input-group">Remarks:</label>
										<input class="form-control" rows="5" id="comment" name="txtremarks" autocomplete="off" placeholder="Remarks"/>
									</div>
								</div>
									<button type="button" class="btn btn-primary pull-right add_trail_sc" id="add_trail_sc">Save</button>
							</div><!--end of row-->
					</div><!--end of form login-->
	</div><!--end of panel body-->				
</div><!--panel-->
<script>
var UpdateTicketTN = "{{route('updatetn')}}";
</script>