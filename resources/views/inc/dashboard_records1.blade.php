<!--<meta http-equiv="refresh" content="0" />
auto refresh wholepage-->

<!--<script type="text/javascript">
    function autoRefreshPage()
    {
        window.location = window.location.href;
    }
    setInterval('autoRefreshPage()', 5000);
</script>
<!--end-->

<!--auto refresh div
<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
setInterval(function(){
	$("#dash_records").load();
//$("#dash_records").load("#assigned_ticket")
}, 1000);
});
</script>-->

<!--end-->



	<div class="col-sm-3">
	    <div class="panel panel-info">
	        <div class="panel-heading">
	            
            <!--Search-->
    		<form action="{{route('search')}}" autocomplete="off" method="POST">
    		{!! csrf_field() !!}
                <div class="input-group input-group-lg">
                    <input type="text" id="search" class="form-control" name="txtsearch_ticket" placeholder="" required/>
                    <span class="input-group-btn">
                        <button class="btn btn-default" id="btnsearch">GO</button>
                    </span>
				</div>
				<div>
					<!--<input type="checkbox" name="searchtn" id="exactsearchtn"/><p>Search key words</p>	-->	 
                </div>
            </form>

			<?php
				$sdodepartments = array("SGOD","CID","ICTU","SDS","Finance","Admin","Supply","Cash", "Records","Personnel", "Health", "Legal", "PSDS", "ASDS");
				$Is_Sdo_account = false;
				if(in_array(Session::get('Department'), $sdodepartments)){
					//this is a SDO account
					$Is_Sdo_account = true;			
				}else{
					//This is a School Account
					$Is_Sdo_account = false;
				}

				$datacount = 0;
                $overduecount = 0;
			?>                   
            <!--End of Search-->	
	        </div><!--end of panel-heading-->
	        <div class="list-group">
				<!--<a href="#" class="list-group-item">Assigned Ticket</a>-->
				
				@if($Is_Sdo_account==false)
				<a href="#" class="list-group-item" data-toggle="modal" data-target="#create_ticket"><span class="glyphicon glyphicon-plus"></span> Create Ticket</a>
				<a class="list-group-item" href="{{route('closed_tickets')}}">Closed Tickets</a>
				@elseif($Is_Sdo_account==true)

				<a href="#" class="list-group-item" data-toggle="modal" data-target="#create_ticket"><span class="glyphicon glyphicon-plus"></span> Create Ticket</a>
				<a href="{{route('dashboard')}}" class="list-group-item" data-toggle="modal" data-target="#create_ticket"><span class="glyphicon glyphicon-glyphicon glyphicon-th-list"></span>&nbsp;Assigned Tickets
				<span class="badge badge-info" id="assigned">
					{{ $datacount}}
					</span>
				</a>
				<a class="list-group-item" href="#" data-toggle="modal" data-target="#dynamicmodal"><span class="glyphicon glyphicon-pause"></span> For Receipt
					<span class="badge badge-info pending">
					{{ $pendingtick }}
					</span>
				</a>
				<a class="list-group-item" href="{{route('transferred')}}" data-toggle="modal" data-target="#"><span class="glyphicon glyphicon-share-alt"></span> Transferred Tickets
					<span class="badge badge-info pending">
					{{ $transferbu }}
					</span>
				</a>
				@endif
				@if(Session::get('Department')=='Records')
				<a href="{{route('pending_submission')}}" class="list-group-item" data-toggle="modal" data-target="#"><span class="glyphicon glyphicon-pause"></span> Pending Tickets From School <span class="badge badge-info pensub">
					{{ $pensub  }}
					</span></a>
				<a href="{{route('pending_tickets_fromschool')}}" class="list-group-item" data-toggle="modal" data-target="#"> Clear Tickets</a>
				@else
				@endif
				<!--<a href="#" class="list-group-item" data-toggle="modal" data-target="#">Follow-up Ticket</a>-->
			</div>
		</div>
			@if($Is_Sdo_account==false)
			@else
			<table class="table table-striped table-bordered">
				<tr>
					<th style="text-align:center;">Open</th>
					<th style="text-align:center;"><a href="#" data-toggle="modal" data-target="#overdue_ticket">Overdue</a></th>
				</tr>
				<tr>
					<td align="center" ><span id="LabelOpen"></span></td>
					<td align="center"><span id='LabelOverdue'>0</span></td>
				</tr>
			</table>
			<!--EFFICIENCY COUNT-->
			<div class='panel panel-default' id="effpanel" style="background-image: linear-gradient(to right, #55efc4 , #00b894);">
			    <div class='panel-body'> <center><h1 id='effrat' style='color:white; text-shadow: 0px 1px 2px rgba(0,0,0,0.5);'></h1>
			    <h1 style='color:white; text-shadow: 0px 1px 2px rgba(0,0,0,0.5);' id='efficiencyrat'></h1>
			    </center></div>
			</div>
			@endif
	</div>

	<div class="col-sm-9" id="dash_records">
		<h3 style="margin-top: 0;">Active Tickets assigned to  {{ Session::get('Department') }} office</h3>
		<table id="assigned_ticket" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th width="30">TN</th>
					<th>Title</th>
					<th>Transfer to</th>
					<th width="60">Date/Time</th>
					<th width="50" id="action">Actions</th>
				</tr>
			</thead>
			<tbody>
                <?php
                $datacount = 0;
                $overduecount = 0;
                ?>
				@foreach($ticket as $data)

					@if ($data['route']==Session::get('Department') )
					@if ($data['status']!='Transferred' && $data['status']!='Closed' && $data['status']!='Pending Submission' && $data['status']!='Released' && $data['status']!='Pending Transfer')	

					<?php
					//VIRMIL ADDED CODE >>>>>>>> OVERDUE DETECTOR
					$datacount ++;
					$ticketnumber = $data['tn'];
					$fetched_document_day = strtotime($data['date_time']);
					$is_overdue = false;
					$gotdate = $data['date_time'];
					
					//SNIPPED FROM OLA v2
					
					//Compute All Serving Days
					$NumberOfDocumentDays = 0;
			
					//Current Month Integers
					$month = date("m",$fetched_document_day);
					$list = "";
					$lmonth = date("m",$fetched_document_day) -1;
					$year = date("Y",$fetched_document_day); 
				
					for($d = date("d",$fetched_document_day); $d <= 31;$d++){
						$time=mktime(12, 0, 0, $month, $d, $year);
						if(date('m',$time) == $month){
							if(date("l",$time) != "Saturday" && date("l",$time) != "Sunday"){
								if($d <= date("d")){
										$NumberOfDocumentDays += 1;
								}
							}
						}
					}
					//echo $NumberOfDocumentDays;
					if($NumberOfDocumentDays > 5){
						$is_overdue = true;
						$overduecount +=1;
					}
					//END OF SNIP
					?>
					@if($is_overdue!= true)
				<tr>     
						    
					<?php
						$imaglink = "<img class=popimg src=https://media.giphy.com/media/qjqUcgIyRjsl2/source.gif><br>";
   
					if($data['status_2']=='Priority'){
					    // PRIORITY
					    ?>
					    
					    <td style='background-color: #9C27B0; font-family:monospace;text-transform:uppercase;'>
					    <strong style='color:white; text-shadow: 0px 1px 1px rgba(0,0,0,0.2);'>
					    <a href='#' class='btnaddinfo' style='color:white;' data-dayscount="{{$NumberOfDocumentDays}}"
					    data-location="{{ $data['route'] }}"
					    data-remarks="{{ $data['remarks'] }}"
					    data-status="{{ $data['status'] }}"
					    data-toggle="modal"
					    data-target="#datamodal"> {{ $ticketnumber }}</a>
					    </strong></span></td>
					    
					    <?php
					    
					}else{
					    //DEFAULT
					    ?>
					    
					    <td style='font-family:monospace;text-transform:uppercase; font-size: 1.2em'><span ><strong>
					    <a href='#' class='btnaddinfo' data-toggle="modal" data-dayscount="{{$NumberOfDocumentDays}}"
					    data-location="{{ $data['route'] }}"
					    data-remarks="{{ $data['remarks'] }}"
					    data-status="{{ $data['status'] }}"
					    data-target="#datamodal">{{ $ticketnumber }}</a>
					    </strong></span></td>
			            
					    <?php
					}
					echo "<script>
					
					$('#LabelOpen').html($datacount);
					$('#LabelOverdue').html($overduecount);
					$('#assigned').html($datacount);
					</script>";
					//VIRMIL ADDED CODE >>>>>>>> OVERDUE DETECTOR
					?>
						
					<td>{{ $data['title'] }}</td>
					<td>{{$transferred['transfer_to']}}</td>
				    <td>{{ $data['date_time'] }} - {{ $data['time'] }}</td>
					<td>
					<!--@if($data['status']!='Pending Transfer' && $data['route']==Session::get('Department'))-->
						<div class="dropdown">
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><i class="fas fa-cog"></i> <span class="caret"></span></button>
						<ul class="dropdown-menu dropdown-menu-right">
					<!--records /admin account-->  
							<li><a href="" class='btnadd_remarks'  data-id="{{$data['id']}}" data-remarks="{{$data['remarks']}}" data-tn="{{$data['tn']}}" data-agent="{{$data['agent']}}" data-toggle="modal" data-target="#add_remarks">Add Remarks</a></li>
							<li><a href="#" class="assign_ticket" a href="#" data-id="{{$data['id']}}" data-title="{{$data['title']}}" data-status="{{$data['status']}}" data-remarks="{{$data['remarks']}}" data-tn="{{$data['tn']}}" data-agent="{{$data['agent']}}" data-status2="{{$data['status_2']}}" data-email="{{$data['email']}}" data-origin="{{$data['origin']}}" data-cname="{{$data['cname']}}" data-doctype="{{$data['doc_type']}}" data-route="{{$data['route']}}" data-date="{{$data['date_time']}}" data-time="{{$data['time']}}" data-dateout="{{$data['date_out']}}" data-timeout="{{$data['time_out']}}" data-toggle="modal" data-target="#assign_ticket">Assign Ticket</a></li>

							<li><a href="#" class="btnupdate_ticket" data-toggle="modal" data-target="#update_ticket" id="btnupdate_ticket" data-id="{{$data['id']}}" data-title="{{$data['title']}}" data-cname="{{$data['cname']}}" data-origin="{{$data['origin']}}">Update Ticket</a></li>
							
							<li><a href="#" class="transfer_ticket" data-id="{{$data['id']}}" data-title="{{$data['title']}}" data-status="{{$data['status']}}" data-remarks="{{$data['remarks']}}" data-tn="{{$data['tn']}}" data-agent="{{$data['agent']}}" data-status2="{{$data['status_2']}}" data-email="{{$data['email']}}" data-origin="{{$data['origin']}}" data-cname="{{$data['cname']}}" data-doctype="{{$data['doc_type']}}" data-route="{{$data['route']}}" data-date="{{$data['date_time']}}" data-time="{{$data['time']}}" data-dateout="{{$data['date_out']}}" data-timeout="{{$data['time_out']}}" data-toggle="modal" data-target="#transfer_ticket">Transfer Ticket</a></li>
							<li class="divider"></li>
							<li><a href="#" class="closed_ticket" data-id="{{$data['id']}}" data-title="{{$data['title']}}" data-status="{{$data['status']}}" data-remarks="{{$data['remarks']}}" data-tn="{{$data['tn']}}" data-agent="{{$data['agent']}}" data-status2="{{$data['status_2']}}" data-email="{{$data['email']}}" data-origin="{{$data['origin']}}" data-cname="{{$data['cname']}}" data-doctype="{{$data['doc_type']}}" data-route="{{$data['route']}}" data-date="{{$data['date_time']}}" data-time="{{$data['time']}}" data-dateout="{{$data['date_out']}}" data-timeout="{{$data['time_out']}}" data-toggle="modal" data-target="#close_ticket">Close Ticket</a></li>
							<li><a href="#" class="release_ticket" data-id="{{$data['id']}}" data-title="{{$data['title']}}" data-status="{{$data['status']}}" data-remarks="{{$data['remarks']}}" data-tn="{{$data['tn']}}" data-agent="{{$data['agent']}}" data-status2="{{$data['status_2']}}" data-email="{{$data['email']}}" data-origin="{{$data['origin']}}" data-cname="{{$data['cname']}}" data-doctype="{{$data['doc_type']}}" data-route="{{$data['route']}}" data-date="{{$data['date_time']}}" data-time="{{$data['time']}}" data-dateout="{{$data['date_out']}}" data-timeout="{{$data['time_out']}}" data-toggle="modal" data-target="#release_ticket">Release Ticket</a></li>
							@if($data['status_2']!='')
							@else
							<li><a href="#" class="btndelete_ticket" id="delete_ticket" data-id="{{$data['id']}}">Delete</a></li>
							<!--@endif

		                    @else
						    <button class="btn btn-primary" disabled=""><i class="fas fa-cog"></i> <span class="caret"></span></button>
		                    @endif	-->		
					    </ul>
					</div>
				</tr>
					@endif
				@endif
				@endif
			@endforeach
			</tbody>  
	    </table>
    </div>

<script>
    EfficiencyRating();
    function EfficiencyRating(){
        var open = $("#LabelOpen").html();
        var overdue = $("#LabelOverdue").html();
        var rating = open - overdue;
        rating = rating / open * 100;
        var n = rating.toFixed(2);
            if(rating > 90){
        $("#effrat").html('<i class="far fa-grin-stars"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
    }else if(rating > 80){
                $("#effpanel").css("background-image","linear-gradient(to right, #FFEB3B , #8BC34A)");
        $("#effrat").html('<i class="far fa-smile"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
    }else if(rating > 70){
        $("#effrat").html('<i class="far fa-meh"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
    }else if(rating > 50){
        $("#effpanel").css("background-image","linear-gradient(to right, #F44336 , #FFC107)");
        $("#effrat").html('<i class="far fa-frown"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
    }else if(rating > 20){
        $("#effrat").html('<i class="far fa-sad-tear"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
    }

        $("#efficiencyrat").html(n);
    }     
setInterval(function()
{
    EfficiencyRating();

}, 300);
</script>

<script>
//For dashborad popover
$(".btnaddinfo").click(function(){
    var daysindepartment = $(this).data("dayscount");
    var Loc = $(this).data("location");
    var Rem = $(this).data("remarks");
    var Stat = $(this).data("status");
    if(Loc == "" && Loc == null){ Loc="(empty)";}
    if(Rem.length ==0 ){ Rem="(empty)";}
    if(Stat == "" && Stat == null){ Stat="(empty)";}
    $("#d1").html(Loc);
    $("#d2").html(Rem);
    $("#d3").html(Stat); 
    
    if(daysindepartment > 5){
        $("#overduepanel").css("display","block");
    }else{
        $("#overduepanel").css("display","none");
    }
     $("#d0").html(daysindepartment); 

});
    var closedTicketURL="{{route('closed_ticket')}}";
	var submitTicketURL="{{route('submit_ticket')}}";
     
</script>

<!--search for Exact match--> 
            <script>
            $("#btnsearch").click(function(){
                var searchkeyword = $("#search").val();

				var exacttn = document.getElementById("exactsearchtn"); 
				var exacttn;
				if(exacttn.checked == false){
				
                //alert(searchkeyword);
                $.ajax({
                    type: "POST",
                    url: "{{route('searchexact')}}",
                    data: {
                        _token:"{!! csrf_token() !!}",
                        txtsearch_ticket:searchkeyword},
                    success: function(data){
                        $("#ticketnumbersearched").html(searchkeyword + "<br>");
                        $("#doctitle").html("(not found)");
                        $("#off_origin").html("(not found)");
                        data = JSON.parse(data);
                        $("#search").val("");
                        //alert(JSON.stringify(data));
                       $("#searchresult").html("");
                       for(var i =0; i < data.length;i++){
                           
                        $fdate = new Date(data[i]["date_time"]);
                        $fdate = $fdate.getDay() + "/" + $fdate.getMonth() + "/" + $fdate.getFullYear();
                        $("#doctitle").html(data[i]["title"].toUpperCase());
                        $("#searchresult").append("<tr><td>" + $fdate + " - " + data[i]["time"] + "</td>" + 
                        "<td>" + data[i]['route']  + "</td>" + 
                        "<td><div class='rem'>" + data[i]['remarks'] +"</div></td>" + 
                        "<td>" + data[i]['status'] +"</td>" + 
                        "</tr>");
                        if(i <= data.length){
                            $("#off_origin").html(data[i]["origin"] + " (" + data[i]["cname"] + ")");
                        }
						
                       }
                    }
                })
			}
            })

            </script>

