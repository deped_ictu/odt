@extends('layouts.master')
@section ('title')
    DepEd | Marikina DTS | All Tickets
@endsection

@section('content') 
<div class="row">

@include('inc.left_panel')

        <div class="col-sm-9">
			<div class="panel panel-info">
					<div class="panel-heading">
						<h3>All Tickets</h3>
					</div>
					<div class="panel-body">
					<table id="tick_OO" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th width="30">ID</th>
							<th width="100">TN</th>
							<th>Title</th>
              <th width="50">No. of Days</th>
							<th width="20">Action</th>
              <th width="60">Route</th>
              <th width="20">Edit</th>
						</tr>
					</thead>
					 <tbody>

						@foreach($alltickets as $data)
<!--Count the # of days-->
<?php		
//$start_date = strtotime($data['date']);
//$TodayDate = strtotime('today');
//$timeDiff = abs($TodayDate - $start_date);
 //$numberDays = $timeDiff/86400;  // 86400 seconds in one day
//total # of days
 //$numberDays = intval($numberDays);
//echo $numberDays;
 //$noOfdaysToCheck ="30";



$start = new DateTime($data['date']);
$end = new DateTime(date("Y-m-d"));
// otherwise the  end date is excluded (bug?)
//$end->modify('+1 day');

$interval = $end->diff($start);

// total days
$days = $interval->days;

// create an iterateable period of date (P1D equates to 1 day)
$period = new DatePeriod($start, new DateInterval('P1D'), $end);

// best stored as array, so you can add more than one
$holidays = array();

foreach($period as $dt) {
    $curr = $dt->format('D');

    // substract if Saturday or Sunday
    if ($curr == 'Sat' || $curr == 'Sun') {
        $days--;
    }

    // (optional) for the updated question
    elseif (in_array($dt->format('Y-m-d'), $holidays)) {
        $days--;
    }
}
//echo $days;
?>

							<tr>     
							  <td>{{ $data['id'] }}</td>  
                @if($days>='30')							
								<td style="background-color:#EF9A9A;">{{ $data['tn']}}</td>
                @else
                <td>{{ $data['tn']}}</td>
                @endif
								<td>{{ $data['title'] }}</td>
                <td>{{$days}}</td>
								<td>{{ $data['action']}}</td>
                <td>{{ $data['route']}}</td>
                <td><button class="btn btn-info" style="background-color:#2D5F5D; color:white"><a class="updatet" data-id="{{ $data['id'] }}" data-title="{{$data['title']}}" data-route="{{$data['route']}}" data-action="{{$data['action']}}" href="" data-toggle="modal" data-target="#edit_ticket"><span class="glyphicon glyphicon-pencil" style="color:white"></span></a></button></td>
							</tr>
              <input type="hidden" name="txtid" id="updateid" value="{{$data['id']}}"/> 
						@endforeach
					</tbody> 
					</table>
				</div>
			</div>
		</div>
	</div>
<!--modal-->
<div class="modal fade" id="edit_ticket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-sm">
	<div class="modal-header modal-primary">
        <h3 class="modal-title">UPDATE TICKET
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
		</h3>
      </div>
      <div class="modal-body">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <input type="hidden" name ="txtid" id="txtids3" />
        <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                  <label for="add">&nbsp;<b>Title</b>&nbsp;&nbsp;</label>
                  <input type="text" name="txttitle" id="txttitle" class="form-control" />
              </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
				<div class="form-group">
                <label for="add">&nbsp;<b>Route</b>&nbsp;&nbsp;</label>
                <input type="text" name="txtroute" id="txtroute" class="form-control" />
				</div>	
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
				<div class="form-group">
                <label for="add">&nbsp;<b>Action</b>&nbsp;&nbsp;</label>
                <input type="text" name="txtaction" id="txtaction" class="form-control" />
				</div>	
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="btnsaveupdate">Save</button>
      </div>
    </div>
  </div>
</div>
<script>
	var UPDATEONETICKET="{{route('updateoneticket')}}";
</script>
@endsection
