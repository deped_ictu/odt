@extends('layouts.master')
@section ('title')
    DepEd | Marikina DTS | Helpdesk
@endsection

@section('content') 
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript">
$(function(){
    $("#s1").show();
    $("#s2").hide();
    $("#s3").hide();
    $("#s4").hide();
    $("#s5").hide();
    $("#div1").on("click", function(){
        $("#s1").show();
        $("#s2").hide();
        $("#s3").hide();
        $("#s4").hide();
        $("#s5").hide();
    });
    $("#div2").on("click", function(){
        $("#s2").show();
        $("#s1").hide();
        $("#s3").hide();
        $("#s4").hide();
        $("#s5").hide();
    });
    $("#div3").on("click", function(){
        $("#s3").show();
        $("#s1").hide();
        $("#s2").hide();
        $("#s4").hide();
        $("#s5").hide();
    });
    $("#div4").on("click", function(){
        $("#s4").show();
        $("#s1").hide();
        $("#s2").hide();
        $("#s3").hide();
        $("#s5").hide();
    });
    $("#div5").on("click", function(){
        $("#s5").show();
        $("#s1").hide();
        $("#s2").hide();
        $("#s4").hide();
        $("#s3").hide();
    });
});
</script>

<div class="container-fluid">
  <h2><strong>KNOWLEDGEBASE</strong></h2>
  <h4>Sample sample</h4><br>
    <div class="row">
        <div class="col-sm-3">
            <div class="panel panel-info">
                <div class="list-group-item"><strong>KNOWLEDGEBASE</strong></div>
                    <ul class="list-group">
                        <a href="#" id="div1" class="list-group-item list-group-item-action">General Information</a>
                        <a href="#" id="div2" class="list-group-item list-group-item-action">Sample</a>
                        <a href="#" id="div3" class="list-group-item list-group-item-action">Sample1</a>
                        <a href="#" id="div4" class="list-group-item list-group-item-action">Sample2</a>
                        <a href="#" id="div5" class="list-group-item list-group-item-action">Sample3</a>
                    </ul>
                    
            </div>
        <div class="row">
        </div>
        </div>
            
        <div class="col-sm-9"> 
                <div class="collapse multi-collapse in" id="s1">
                        <hr>
                        <h4>Knowledgebase : General Information</h4>
                        <hr>
                        
                        <h4 style="text-transform:uppercase;"><strong>How to Clear the Cache on your Browser</strong></h4>
                        <ol>
                            <strong><li>Google Chrome Browser</li></strong>
                        
                        <ul>
                            <li>To clear the Cache of your Google Chrome browsing history. Click the <b>ctrl+shift+del</b> on the key board. </li>
                            <li>A new window will pop up. Check all checkboxes and click clear data to clear the cache.</li><br>
                            <img src="{{asset('images/clear_data.png')}}" style="height:450px; width:400px;"></img><br>
                        </ul>
                        <br>
                            <strong><li>Internet Explorer Browser</li></strong>
                            <ul>
                                <li>To clear the Cache of your Internet Explorer browsing history. Click the <b>ctrl+shift+del</b> on the key board. </li>
                                <li>A new window will pop up. Put a check on Browsing History, Cookies and saved website data,Cached data and files and Tabs I've set aside or recently closed and then click clear to clear the cache.</li><br>
                                <img src="{{asset('images/ie_clear_history.png')}}" style="height:550px;"></img><br>
                            </ul>
                            <br>
                            <strong><li>Mozilla Firefox Browser</li></strong>
                            <ul>
                                <li>To clear the Cache of your Mozilla Firefox browsing history. Click the <b>ctrl+shift+del</b> on the key board. </li>
                                <li>A new window will pop up. Select Everything from the Time range to clear.</li>
                                <li>Put a check on Browsing & Download History, Form & Search History, Cookies, Cache and Active Logins and then click Clear now to clear the cache.</li><br>
                                <img src="{{asset('images/firefox_clear_cache.png')}}" style="height:550px;"></img><br>
                            </ul>
                        </ol>
                        <br>
                </div> 
                <div class="collapse multi-collapse" id="s2">
                        <hr>
                        <h4>Knowledgebase : Sample</h4>
                        <hr>
                        <h4><strong>Sample</strong></h4>
                        <ul>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                        </ul>
                        <br>
                        <h4><strong>Sample</strong></h4>
                        <ul>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                        </ul>
                        <br>
                </div>
                <div class="collapse multi-collapse" id="s3">
                        <hr>
                        <h4>Knowledgebase : Sample1</h4>
                        <hr>
                        <h4><strong>Sample1</strong></h4>
                        <ul>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                        </ul>
                        <br>
                        <h4><strong>Sample1</strong></h4>
                        <ul>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                        </ul>
                        <br>
                </div>
                <div class="collapse multi-collapse" id="s4">
                        <hr>
                        <h4>Knowledgebase : Sample2</h4>
                        <hr>
                        <h4><strong>Sample2</strong></h4>
                        <ul>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                        </ul>
                        <br>
                        <h4><strong>Sample2</strong></h4>
                        <ul>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                        </ul>
                        <br>
                </div>
                <div class="collapse multi-collapse" id="s5">
                        <hr>
                        <h4>Knowledgebase : Sample3</h4>
                        <hr>
                        <h4><strong>Sample3</strong></h4>
                        <ul>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                        </ul>
                        <br>
                        <h4><strong>Sample3</strong></h4>
                        <ul>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                            <li>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.</li>
                        </ul>
                        <br>
                </div>
        </div>
  </div><!--end of row-->
</div><!--end of container-->
@endsection