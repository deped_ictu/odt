@extends('layouts.master')

@section ('title')
    DepEd | Marikina DTS | Ticket Details
@endsection

@section('content') 
@include('inc.left_panel')


<div class="col-sm-9">
<h2>Document Trail</h2>
<hr>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th width="30">ID</th>
				<th width="40">Action</th>
				<th width="40">Route</th>
				<th width="60">Remarks</th>

				<!--<th width="50">Status</th>-->
			</tr>
		</thead>
		<tbody>
		<?php
			foreach($ticket as $data){
			$data['title'];
			$data['tn'];
			}
			$track = $data['tn'];
			$t = $data['title'];	
      	?>
		<h3 style="text-transform: uppercase;"><strong><?php echo $t; ?></strong><br>
		 #<?php echo $track; ?></h3>
		@foreach($ticket as $data)		
		@if($data['action']!='Transferred')
				<tr> 
					<td>{{$data['id']}}</td>    
					<td>{{ $data['action'] }}</td>
					<td>{{ $data['route'] }}</td>
					<td>{{ $data['remarks'] }} </td>
				</tr>
		@endif	
		@endforeach
		</tbody>  
	</table>

<!--add document trail-->
<?php
foreach($ticket as $data){
	//echo $data['action'];
	 
}
$ac=$data['action'];
//echo $ac;
?>
@if($ac=='Released')
@else
	@include('inc.add_document_trail');
@endif
<!--end-->
</div>

@endsection
