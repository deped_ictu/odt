@extends('layouts.master')
@section ('title')
    DepEd | Marikina DTS | My Tickets
@endsection

@section('content') 
<div class="row">
@include('inc.left_panel')
<style>
button, input[type="submit"], input[type="reset"] {
	background: none;
	color: inherit;
	border: none;
	padding: 0;
	font: inherit;
	cursor: pointer;
	outline: inherit;
}
</style>

        <div class="col-sm-9">
			<div class="panel panel-info">
					<div class="panel-heading">
					
						<h3>My Tickets ({{ Session::get('Department') }}) </h3>
					</div>
					<div class="panel-body">
					<table id="tick_OO" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th width="30">ID</th>
							<th width="100">TN</th>
							<th>Title</th>
							<th>Action</th>
                            <th width="60">Route</th>
						</tr>
					</thead>
					 <tbody>
						@foreach($ticketsFOO as $TFOO)
						@if($TFOO['action']=='' || $TFOO['action']=='Transferred')
						@else
							<tr>     
							    <td>{{ $TFOO['id'] }}</td>  							
								<td>{{$TFOO['tn']}}</td>
								<td>{{ $TFOO['title'] }}</td>
								<td>{{ $TFOO['action']}}</td>
                                <td>{{$TFOO['route']}} </td>
                      			<input type="hidden" id="id" name="txtid"/>
							</tr>
						@endif
						@endforeach
					</tbody> 
					</table>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">
	/*$(document).ready(function() {
	    $('#mytickets').DataTable({
	        "processing": true,
	        "serverSide": true,
	        "ajax": {
	        	url: "{{route('fetch_tickets')}}",
	        	data:{
	        		cname : "{{ Session::get('Department') }}"
					//route : "{{ Session::get('Department') }}"
	        	}
	        },
	        "columns":[
	        	{"data":"id"},
	        	{"data":"tn"},
	        	{"data":"title"},
	        	{"data":"action"},
	        	{"data":"route"}
	        ]
	    } );
	} );*/
</script>
@endsection