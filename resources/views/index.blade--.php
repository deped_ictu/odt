@extends('layouts.login_master')

@section ('title')
    DepEd | Marikina DTS | Login
@endsection

@section('content')

<div class="container">
    <div class="wrapper"> 

        <h3 class="form-signin-heading">Deped Marikina Online Document Tracking System</h3>    
        
        <hr class="colorgraph"><br />
        
        <form method="POST" action="{{route('login_dts')}}" class="form-signin">
        {!! csrf_field() !!}
            <input type="email" name="txtEmail" id="email" autocomplete="off" class="form-control" placeholder="Email" required/> <br />
            <input type="password" name="txtPassword" id="password" readonly onfocus="this.removeAttribute('readonly')" autocomplete="off" class="form-control" placeholder="Password" />
            <button type="submit" name="login" id="login-btn" class="btn btn-lg btn-primary btn-block">Log in</button>
        </form>

        <!--<span><button id="register-btn" onclick="window.location.href='registration_form.php'" class="register-btn">Register</button></span>-->
        
    </div> 
</div>

<p align="center">Schools Division Office - Marikina City</p>
@endsection