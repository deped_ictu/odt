@extends('layouts.master')
@section ('title')
    DepEd | Marikina DTS | Print Todays Logs
@endsection

@section('content')
@include('inc.left_panel') 
<style>
    #print {

    }
    
    @media print {
        .hide_print,
        .footer {
            display: none;
        }
        
    }
</style>

<!-- Script -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<script>
$(document).ready(function(){
	$.datepicker.setDefaults({
		dateFormat: 'yy-mm-dd'
	});
	$(function(){
		$("#From").datepicker();
		$("#to").datepicker();
	});
    
	$('#search').click(function(){
        $("#logs").html("");
        $("#date").html("");
		var From = $("#From").val();
        var to = $("#to").val();
        var options = {  month: 'long', day: 'numeric', year: 'numeric' };
        var fd = new Date(From);
        var d = fd.toLocaleDateString("en-US", options);
		
        var to = $("#to").val();
        var options2 = {  month: 'long', day: 'numeric', year: 'numeric' };
        var fd2 = new Date(to);
        var d2 = fd2.toLocaleDateString("en-US", options2);
        if(to==From){
            document.getElementById("date").innerHTML = d;
        }
        else{
        document.getElementById("date").innerHTML = d + ' - ' + d2;
        }
		//$("#date").html(df);
        //alert(d);
		if(From != '' && to != '')
		{
			$.ajax({
				url:"{{route('filter_date')}}",
				method:"POST",
				data:{
                    _token:"{!! csrf_token() !!}",
                    From:From, 
                    to:to},
				success:function(data) {
				
                    data = JSON.stringify(data);
                data = JSON.parse(data);
                    for(var i =0; i < data.length; i++){
                        //alert(data.length);
                        $("#p_records").append("<tr><td>"+ data[i]["tn"] +"</td>"
                        + "<td>" + data[i]['title']  + "</td>" 
                        + "<td>" + data[i]['sender']  + " - " + data[i]["cname"] + "</td>" 
                        + "<td>" + data[i]["date"] + ' ' + data[i]["time"] + "</td></tr>");
                    }
                    $("#logs").html("");
                    $("#date").html("");

				}
			});
		}
		else
		{
			alert("Please Select the Date");
		}
	});
});
</script>
    <div class="col-sm-9" id="print_records">
    <h3>Daily DTS Logs </h3><p id="date">(<?php 
        $d = date("Y-m-d"); 
        echo date("F j, Y", strtotime("$d"));
        ?>)</p>
    <!--<hr class="hide_print">
    <div class="row">
    <form action="{{route('filter_date')}}" method="POST">
            <div class="col-sm-3 hide_print">
                <h4>Select Date</h4>
                <div class="form-group">
                <p>From</p>
                <input type="text" name="From" id="From" class="form-control print-hidden" placeholder="From Date"/>
                </div>
            </div>
            <div class="col-sm-3 hide_print">
            <h4>&nbsp;</h4>
                <div class="form-group">
                <p>To</p>
                <input type="text" name="to" id="to" class="form-control print-hidden" placeholder="To Date"/>
                <p></p>
                </div>
            </div>
            <div class="col-sm-3 hide_print">
            <h4>&nbsp;</h4>
                <div class="form-group">
                <p>&nbsp;</p>
                <button type="button" name="range" id="search" class="btn btn-success print-hidden">Search</button>
                <button type="button" name="print" id="print" value="Print" class="btn btn-primary print-hidden pull-right" onClick="window.print()"><i class="fas fa-print"></i>Print Logs</button>
                </div>
            </div>
    </form>
    </div>
    <hr>-->
        <table id="p_records" class="table table-bordered">
            <tr>
                <th width="20%">Tracking Number</th>
                <th>Title</th>
                <th width="20%">From</th>
                <th width="10%">Time</th>
            </tr>
            @foreach($filter as $data) 
            <tr id="logs">
                <td style="font-family:monospace;text-transform:uppercase;">{{$data['tn']}}</td>
                <td>{{$data['title']}}</td>
                <td>{{$data['sender']}} - {{$data['cname']}}</td>
                <td>{{$data['date']}} - {{$data['time']}}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
</div>
@endsection