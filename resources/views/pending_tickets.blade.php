@extends('layouts.master')
@section('content')
@include('inc.left_panel')		  
			<div class="col-sm-9">
				<div class="panel panel-info">
					<div class="panel-heading">
						@if(session::get('Department')!='Records')<h3>Receive Tickets </h3>@else <h3>Transferred Tickets </h3>@endif
					</div>
					<div class="panel-body">
					<table id="assigned_ticket" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th width="30">ID</th>
							<th width="100">TN</th>
							<td width="30">Action</th>
							<th width="30">Route</th>
							<th width="60" tex-align="center">Title</th>
							<th width="30" style="tex-align:center";><input type="checkbox" id="select_all" style="width:20px; height:20px; text-align:center;"/></th>
						</tr>
					</thead>
					<tbody>
					@foreach($pending as $pending)
						@if($pending['tn']=='')
						@else
								<tr>     
									<td>{{ $pending['id'] }}</td>     
										@if($pending['status']=='Priority')
										<td style="background-color:#ffcccc; color:#000000; font-family:monospace;text-transform:uppercase;">{{ $pending['tn'] }}</td>
										@else 
										<td style="font-family:monospace;text-transform:uppercase;">{{ $pending['tn'] }}</td>
										@endif
										<td>{{$pending['action']}}</td>
										<td>{{$pending['route']}}</td>
										<td>{{ $pending['title'] }}</td>
									<td><input type="checkbox" name="accept" class="accept" data-id="{{$pending['id']}}" data-tn="{{$pending['tn']}}" style="width:20px; height:20px;"/></td>
									<input type="hidden" id="id" name="txtid" />															
								</tr>
						@endif
						@endforeach
						<button class='btn btn-success pull-right'  id="accept" style="margin-right:8px; background-color:#2D5F5D;color:white;">Accept </button> <br><br>
					</tbody>  
					</table>
				</div>
			</div>
		</div>

<script>
	var ACCEPPTTURL="{{route('accept_ticket')}}";

$(document).ready(function(){
	//show button if checkbox is check
	//var	button = $("#accept").hide();
		//btn = $('input[name="accept"]').click(function() {
          //      button.toggle( btn.is(":checked") );
        //});
	//
	//check all checkbox
	$('#select_all').change(function() {
		if($(this).is(':checked')) {
			$("input[type='checkbox']").attr('checked', 'checked');
			button = $("#accept").show();
		} else {
			$("input[type='checkbox']").removeAttr('checked');
			button = $("#accept").hide();
		}
	});
	//$("input[type='checkbox']").not('#select_all').change( function() {
		//$('#select_all').removeAttr('checked');
	//});
	//
    $('#accept').on('click', function(){
        // Declare a checkbox array
        var acceptArray = [];
        
        // Look for all checkboxes that have a specific class and was checked
        $(".accept:checked").each(function() {
            acceptArray.push($(this).data('id'));
			//alert($(this).data('id'));
        });
        // declare variable for array
        var selected;
        selected = acceptArray;
		//alert(selected.length);

		//data to be inserted in the database
		var token= $('input[name=_token]').val();
		id = $(this).data('id');
		action = 'Received';
		//get the time
		var t = new Date();
		var hour = t.getHours();
		var min = t.getMinutes();
		var sec = t.getSeconds();
		var time = hour + ':' + min + ':' + sec;
		//get the date
		var d = new Date();
		var year = d.getFullYear();
		var month =  ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
		var day = d.getDate();
		var date = year + '-' + month[d.getMonth()] + '-' + day;

		user = $(this).data('user');
// Check if there are selected checkboxes
	//one checkbox selected
		if(selected.length==1) {
			id=selected[0];
			//ajax
			$.ajax({
				type:"POST",
				url: ACCEPPTTURL,
				data: {
					_token:token,
					id:id,
					action:action,
					date:date,
					time:time,
					user:user 
					
					},
				success: function(data)
				{
				window.location="/pending_tickets";
				}
			})
		} 
	//two or more selected checkbox
		else if (selected.length>=2){
			for(var i=0; i < selected.length; i++){
				id_select = selected[i];
				//ajax
				$.ajax({
					type:"POST",
					url: ACCEPPTTURL,
					data: {
						_token:token,
						id:id_select,
						action:action,
						date:date,
						time:time,
						user:user 						
						},
					success: function(data)
					{
						//alert(data)
					//window.location="/pending_tickets";
					}
				})
				setTimeout(function() {
					window.location="/pending_tickets";
				}, 1000);
			}
			
		}
	//else if (selected.length==0){
		//$("#accept").hide();	
        //}
    });
});

</script>


@endsection

