
			<?php
				$sdodepartments = array("SGOD","CID","ICTU","SDS","Finance","Admin","Supply","Cash", "Records","Personnel", "Health", "Legal", "PSDS", "ASDS");
				$Is_Sdo_account = false;
				if(in_array(Session::get('Department'), $sdodepartments)){
					//this is a SDO account
					$Is_Sdo_account = true;			
				}else{
					//This is a School Account
					$Is_Sdo_account = false;
				}

				$datacount = 0;
                $overduecount = 0;
			?>

<div class="container" style="margin-top: 15px; padding:0;">
    <!-- Navigation -->
    <nav class="navbar" style="margin:0; border-radius:0; border-top-right-radius: 6px; border-top-left-radius: 6px; background-color:#265077;">
    	<div class="container-fluid">
    		<div class="navbar-header">
    			<a class="navbar-brand" href="#" style="color:white;">Online Document Tracking System</a>
    		</div>
    		<ul class="nav navbar-nav">
            <!--<li class="active"><a href="{{route('dashboard')}}">Dashboard</a></li>-->
            <li><a href="" data-toggle="modal" data-target="#howto" style="color:white;">How to?</a></li>			
			
            <ul class="nav navbar-nav"> 
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color:white;"><i class="fa fa-phone-square" style="color:white;"></i>Help Desk<span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{route('helpdesk')}}">Help Desk</a></li>
                                <li><a href="{{route('knowledgebase')}}">Knowledgebase</a></li>
                                
                            </ul>
                        </li>
    	    	    </ul>
    		</ul>

    		<ul class="nav navbar-nav pull-right"> 
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color:white;">Hi {{Session::get('Firstname')}} {{Session::get('Lastname')}}! <span class="caret"></span></a>
                  <ul class="dropdown-menu dropdown-menu-right">
                    <!--<li><a href="#" data-toggle="modal" data-target="#reset_password">Change Password</a></li>-->
                   
                    @if ($Is_Sdo_account==false)
                        <li><a href="" data-toggle="modal" data-target="#sh_name">Update School Head / OIC Name</a></li>
                        <li><a href="{{route('logout')}}">Exit ODTS</a></li> 
                    
                    @else
                    <li><a id="reset_pass" href="" data-toggle="modal" data-target="#reset_password" data-un="{{Session::get('UserName')}}" data-pass="{{Session::get('Password')}}">Reset Password</a></li>
                    <li><a href="{{route('logout')}}">Exit ODTS</a></li>
                    @endif
                  </ul>
                </li>
    		</ul>
    	</div>
    </nav>
</div>    
<!--How to modal-->
<div class="modal" tabindex="-1" role="dialog" id="howto">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-lg">
      <div class="modal-header modal-info">
        <h3 class="modal-title"><strong>HOW TO? </strong>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </h3>
      </div>
      <div class="modal-body">
        <h4><b><i>Create Ticket?</i></b></h4>
          <ul>
            <li style="font-size:16px;">Click create ticket</li>
            <img src="{{asset('images/create_ticket.png')}}"></img>
            <li style="font-size:16px;">Fill in all fields. Required field has an asterisk (*).</li>
            <img src="{{asset('images/create_ticket_form.png')}}" width="100%"></img>
            <li style="font-size:16px;">Click create button.</li>
          </ul>
        <hr>
        <h4><b><i>Accept Ticket?</i></b></h4>
          <ul>
            <li style="font-size:16px;">Click receive tickets on the left side menu.</li>
            <img src="{{asset('images/receive_ticket.png')}}" width="50%"></img>
            <li style="font-size:16px;">Check the check box.</li>
            <img src="{{asset('images/receive_ticket_data.png')}}" width="100%"></img>
            <li style="font-size:16px;">Click accept button to accept the ticket.</li>
            <li style="font-size:16px;">Go to my ticket to see the list of tickets accepted.</li>
            <img src="{{asset('images/my_ticket.png')}}" width="50%"></img>
          </ul>
          <hr>
        <h4><b><i>Add document Trail?</i></b></h4>
          <ul>
            <li style="font-size:16px;">Click the tracking number.</li>
            <img src="{{asset('images/tracking_number.png')}}" width="100%"></img>
            <li style="font-size:16px;">Fill in the required field. Required field has an asterisk (*).</li>
            <img src="{{asset('images/add_doctrail.png')}}" width="100%"></img>
            <li style="font-size:16px;">Click save.</li>
          </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>   
<!-- reset password-->
<div id="reset_password" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Change User Name & Password</h4>
      </div>
    <div class="modal-body">
      <form action="{{route('resetpassword')}}" method="POST">
        <input type="hidden" name="Firstname" id="txtFirstname" value="{{Session::get('Firstname')}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

         <div class="form-group">
         <label>Current User Name: {{Session::get('UserName')}}</label>
          <?php
              $oldp = "{{(Session::get('Password'))}}";
              //$oldp_sha1 = sha1($oldp);
              //$old_md5 = md5($old_sha1);
              //$oldpass = $old_md5;
              echo sha1($oldp);
          ?>
         <input type="hidden" value="{{Session::get('Password')}}" id="oldpass"/>
         </div>
          <div class="form-group">
            <label>User Name</label>
              <input class="form-control" type="text" name="UserName" id="txtun" placeholder="Type your new user name" value="{{Session::get('UserName')}}">
          </div> 
          <div class="form-group">
            <label>Old Password</label>
              <input class="form-control" type="password" name="oldPassword" id="txtoldpass" placeholder="Type your old password" onChange="checkPasswordMatch();" required />
          </div>
          <div class="registrationFormAlert" id="oldPasswordMatch" style="color:red;"></div>
          <div class="form-group">
            <label>New Password</label>
              <input class="form-control" type="password" name="newPassword" id="pass" placeholder="Type your new password" required>
          </div>
          <div class="form-group">
            <label>Confirm Password</label>
              <input class="form-control" type="password" name="Password" id="confirm_pass" placeholder="Confirm Password" onChange="checkPasswordMatch();" required>
          </div>
            <div class="registrationFormAlert" id="divCheckPasswordMatch"></div>
    </div>
        <div class="modal-footer">
          <button class="btn btn-sm btn-success" type="submit" name="btnConfirm" id="btnConfirm">Reset</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
    </form>
    </div>
  </div>
</div>
 <script>
//var EditUserNamePass = "{{route('resetpassword')}}";

//check if password is match (new & confirm password)
function checkPasswordMatch() {
    var password = $("#pass").val();
    var confirmPassword = $("#confirm_pass").val();

    if (password != confirmPassword)
        $("#divCheckPasswordMatch").html("Passwords do not match!");
    else
        $("#divCheckPasswordMatch").html("Password match.");
}

$(document).ready(function () {
   $("#confirm_pass").keyup(checkPasswordMatch);
});
//old password
function oldPassMatch(){
  var oldpass = $("#oldpass").val();
  var pass_old = $("#txtoldpass").val();
  if(oldpass != pass_old)
    $("#oldPasswordMatch").html("Old password is incorrect!");
  else
    $("#oldPasswordMatch").html("Old password is correct!")
}

$(document).ready(function () {
   $("#txtoldpass").keyup(oldPassMatch);
});
 </script>    
 