<!-- Modal add remarks-->
<div class="modal fade" id="archived_remarks" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="hidden" id="id_ar" name="txtid"/>
		<div class="form-group form-inline">
            <h3>Add Remarks</h3>
             <!--<input type="text" name="txtremarks" id="txttn_ar" style="border:0px; background-color: transparent" />-->
        </div>
      </div>
      <div class="modal-body">
		<div class="form-group">
            <!--<label for="add">&nbsp;<b>Add Remarks:</b></label>-->
            <textarea type="text" name ="txtremarks_ar" id="archived_remarks" class="form-control"></textarea>				
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" id="arch_rem">Update</button>
      </div>
    </div>
  </div>
</div>
<script>
    var addARRemarksURL="{{route('archremarks')}}";
</script>
