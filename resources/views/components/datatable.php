<!-- DataTable -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" />
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

	<script type="text/javascript" class="init">
	$(document).ready(function() {
		$('#ticket_list').DataTable( {
			"order": [[ 0, "desc" ]]
		} );

		$('#assigned_ticket').DataTable( {
		    stateSave: true,
			"order": [[ 3, "desc" ]]
		} );

		$('#due_ticket').DataTable( {
		    stateSave: true,
			"order": [[ 3, "desc" ]]
		} );

		$('#logs_helpd').DataTable( {
		    stateSave: true,
			"order": [[ 1, "asc" ]]
		} );

		$('#ticket_lists').DataTable( {
			"order": [[ 2, "DESC" ]],
        	"columnDefs": [
            {
                "targets": [ 2 ],
                "visible": false,
                "searchable": false
            },
        ]
		} );
		$('table.display').DataTable();
	} );
	</script>
	

	<script type="text/javascript" class="init">
	$(document).ready(function() {
		$('#ticket_closed').DataTable( {
			"order": [[ 1, "ASC" ]]
		} );
		$('#transferredbs').DataTable( {
			"order": [[ 1, "ASC" ]]
		} );
		$('#recenttickets').DataTable( {
			"order": [[ 1, "ASC" ]]
		} );

		$('#open').DataTable( {
			"order": [[ 1, "ASC" ]]
		} );

		$('#mytickets').DataTable( {
		"order": [[ 0, "ASC" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
        ]
    	} );
		
		$('#outgoing').DataTable( {
		"order": [[ 0, "ASC" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
        ]
    	} );

		$('#tick_OO').DataTable( {
		"order": [[ 3, "desc" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
        ]
    	} );

		$('#incoming').DataTable( {
		"order": [[ 0, "ASC" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
        ]
    	} );


	} ); //end
	</script>

<script>
$(document).ready(function() {
    $('#hd_ticket').DataTable( {
		"order": [[ 0, "DESC" ]],
        "columnDefs": [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
        ]
    } );
} );
</script>