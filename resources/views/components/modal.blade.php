<!--edit ticket modal-->
<div class="modal fade" id="update_tn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-sm">
      <div class="modal-header modal-primary">
        <h2 class="modal-title" id="exampleModalLabel">Update Ticket</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <input type="hidden" name ="txtid" id="txtids" />
			<input type="hidden" name="txtuser" id="txtagent" value="{{Session::get('Firstname')}} {{Session::get('Lastname')}}" />
      @foreach($incomingT as $data)
      <input type="hidden" name="txtref_no" value="{{$data['ref_no']}}" />
      <input type="hidden" name="txttitle" value="{{$data['title']}}" />
      <input type="hidden" name="txtstatus" value="{{$data['status']}}" />
      <input type="hidden" name="txtsender" value="{{$data['sender']}}" />
      <input type="hidden" name="txtcname" value="{{$data['cname']}}" />
      @endforeach
        <div class="row">
          <div class="col-md-12">
						  <div class="form-group">
                <label for="add">&nbsp;<b>Document Type</b>&nbsp;&nbsp;</label>
                  <select class="form-control" name="txtdoc_type" id="doc_type">
											<option>Select Document Type</option>
											<option value="Admin">Admin</option>
											<option value="Finance">Finance</option>
											<option value="Legal">Legal</option>
											<option value="Personnel">Personnel</option>
											<option value="Private">Private</option>
										</select>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                  <label for="add">&nbsp;<b>Tracking No.</b>&nbsp;&nbsp;</label>
                  <input type="text" name="txttn" id="txttno" class="form-control" placeholder="Tracking #" autocomplete="off"/>
              </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
              <label for="add">&nbsp;<b>Action</b>&nbsp;&nbsp;</label>
                <select class="form-control" name="txtaction" id="txtaction">
                <option value="Transferred">Transfer</option>
									<option value="Received">Received</option>
									<option value="Released">Released</option>
									<option value="Filed">Filed</option>
									<option value="Without Action">Without Action</option>
									<option value="Referred">Refer</option>
								</select>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
						<div class="form-group">
							<label for="add">Document Route</label>
              <select class="form-control" name="txtroute" id="txtroute">
								<option value="SDS">SDS</option>
								<option value="ASDS">ASDS</option>
								<option value="Admin">Admin</option>
								<option value="Finance">Finance</option>   
								<option value="Legal">Legal</option>
								<option value="Personnel">Personnel</option>
								<option value="CID">CID</option>
								<option value="SGOD">SGOD</option>
								<option value="PSDS">PSDS</option>
								<option value="Cash">Cash</option>
								<option value="Records">Records</option>
								<option value="Supply">Supply and Property</option>
								<option value="Health and Nutrition">Health and Nutrition</option>
								<option value="ICTU">ICTU</option>
							</select>
					  </div>	
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">				
              <label class="input-group">Remarks:</label>
              <input type="text" name="txtremarks" id="txtre" class="form-control" placeholder="Remarks" autocomplete="off"/>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="btnupdate_tn">Save</button>
      </div>
    </div>
  </div>
</div>
<script>
    var UpdateTicketTN="{{route('updatetn')}}";
</script>








