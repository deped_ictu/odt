@extends('layouts.master')
@section ('title')
    DepEd | Marikina DTS | Update Tickets
@endsection

@section('content') 
@include('inc.left_panel')

<div class="col-md-9">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4><b>UPDATE TICKET</b><p class="LC pull-right" style="font-size: 15px; margin:0px;">
			</h4>
		</div>
		<div class="panel-body">
        <!--<form action="{{route('editthesametn')}}" method="post">
				{!! csrf_field() !!}
				@foreach($recent_t as $recent_tickets)	-->
					<div class="form-login">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="input-group">Name of Sender</label>
										<input type="text" id="txtorigin" name="sender" value="{{$recent_tickets['sender']}}" class="form-control" />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="input-group">Company Name</label>
										<input type="text" id="txtcname" name="cname" value="{{$recent_tickets['cname']}}" class="form-control" />
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="input-group">Tracking No.</label>
										<input type="text" id="txttnup" name="tn" class="form-control" />
									</div>
								</div>
							</div> 
							<div class="row">
							    <div class="col-md-4">
									<div class="form-group">
										<label class="input-group">Title</label>
										<input type="text" id="txttitleup" name="title" class="form-control" />
									</div>
								</div>
							    <div class="col-md-4">
									<div class="form-group">
										<label class="input-group">Type of Document</label>
										<select class="form-control" name="doc_type" id="txtdoctype">
										@foreach($doc_type as $type)
                							<option value="{{$type}}" {{ ($type==$recent_tickets['doc_type']) ? 'selected':''}}>{{$type}}</option>
                						@endforeach
										</select>
									</div>
								</div>
                                <div class="col-md-4">
									<div class="form-group">				
										<label class="input-group">Remarks:</label>
										<input class="form-control" rows="5" id="txtcomment" value="{{$recent_tickets['remarks']}}" name="remarks"/>
									</div>
								</div>
								@endforeach
							</div> 
					</div><!--end of form login-->
		</div><!--end of panel body-->
					<div class="panel-footer clearfix">				   
						<button type="submit" class="btn btn-primary pull-right" name="create_ticket" id="create_ticket" >Save</button>
					</div>
		<!--</form>-->
</div><!--end of col-md-9-->
		<div class="panel panel-default">
			<div class="panel-heading">
			<h4><strong>RECENTLY CREATED TICKETS</strong></h4>
			</div>
			<div class="panel-body">
				<table id="assigned_ticket" class="table table-striped table-bordered">
					<thead>
						<tr>
							<!--<th width="30">ID</th>-->
							<th width="30">Company Name</th>
							<th width="30">TN</th>
							<th width="40">Document Type</th>
							<th>Title</th>
							<th width="60">Remarks</th>
							<th width="20"><span class="glyphicon glyphicon-pencil"></span></th>
							<th width="20"><span class="glyphicon glyphicon-trash"></span></th>
							<th width="40"><input type="checkbox" name="ticket" class="selectall" id="select_all"/></th>
							<!--<th width="50">Status</th>-->
						</tr>
					</thead>
					<tbody>
					@foreach($recent_t as $recent_tickets)
							@if ($recent_tickets['action']=='')	
								<tr>     
									<td>{{$recent_tickets['cname']}}</td>
									<td>{{$recent_tickets['tn']}}</td>
									<td>{{$recent_tickets['doc_type']}}</td>
									<td>{{$recent_tickets['title']}}</td>
									<td>{{$recent_tickets['remarks']}}</td>

									<td><a href="#" class="btnupdatettn" id="btnupdatettn" data-tn="{{$recent_tickets['tn']}}" data-title="{{$recent_tickets['title']}}" data-doctype="{{$recent_tickets['doc_type']}}" data-sender="{{$recent_tickets['sender']}}" data-cname="{{$recent_tickets['cname']}}" data-remarks="{{$recent_tickets['remarks']}}"><span class="glyphicon glyphicon-pencil"></span></a></td>

									<td><a href="#" class="btndelete_ticket" id="delete_ticket" data-tn="{{$recent_tickets['tn']}}"><span class="glyphicon glyphicon-trash glyphicon-danger"></span></a></td>

									<td><input class="trans_ticket" type="checkbox" name="transfer" id="checked" value="{{$recent_tickets['id']}}" data-id ="{{$recent_tickets['id']}}" data-user="{{$recent_tickets['user']}}"></td>
									<input type="hidden" name="txttn" id="deltn" value="{{$recent_tickets['tn']}}"/> 
								</tr>
							@endif
							<input type="hidden" name="txttn" value="{{ $recent_tickets['tn'] }}" />
							<input type="hidden" name="txtref_no" value="{{ $recent_tickets['ref_no'] }}" />
							<input type="hidden" name="txttitle" value="{{ $recent_tickets['title'] }}" />
							<input type="hidden" name="txtstatus" value="{{ $recent_tickets['status'] }}" />
							<input type="hidden" name="txtsender" value="{{ $recent_tickets['sender'] }}" />
							<input type="hidden" name="txtcname" value="{{ $recent_tickets['cname'] }}" />
							<input type="hidden" name="txtdoc_type" value="{{ $recent_tickets['doc_type'] }}" />
							<input type="hidden" name="txtremarks" value="{{ $recent_tickets['remarks'] }}" />
					@endforeach
					</tbody>  
				</table>	
			</div>
			<div class="panel-footer clearfix">		
				<button type="submit" class="btn btn-info pull-right transfer" name="transfer" id="transfer" value="Submit" style="margin-right:10px;color:black;"><b>Transfer</b></button>
			</div>
		</div>
	</div>
 <br>

<script>
	var TransferURL="{{route('transfer')}}";
	var DeleteURL = "{{('delete_ticket')}}";
$(document).ready(function(){
	//show button if checkbox is check
	var	button = $("#transfer").hide();
		btn = $('input[name="transfer"]').click(function() {
                button.toggle( btn.is(":checked") );
        });
	//
	//check all checkbox
	$('#select_all').change(function() {
		if($(this).is(':checked')) {
			$("input[type='checkbox']").attr('checked', 'checked');
			button = $("#transfer").show();
		} else {
			$("input[type='checkbox']").removeAttr('checked');
			button = $("#transfer").hide();
		}
	});
	//$("input[type='checkbox']").not('#select_all').change( function() {
		//$('#select_all').removeAttr('checked');
	//});
	//
    $('#transfer').on('click', function(){
        // Declare a checkbox array
        var transferArray = [];
        
        // Look for all checkboxes that have a specific class and was checked
        $(".trans_ticket:checked").each(function() {
            transferArray.push($(this).data('id'));
			//alert($(this).data('id'));
        });
        // declare variable for array
        var selected;
        selected = transferArray;
		//alert(selected.length);

		//data to be inserted in the database
		var token= $('input[name=_token]').val();
		id = $(this).data('id');
		action = 'Transferred';
		//get the time
		var t = new Date();
		var hour = t.getHours();
		var min = t.getMinutes();
		var sec = t.getSeconds();
		var time = hour + ':' + min + ':' + sec;
		//get the date
		var d = new Date();
		var year = d.getFullYear();
		var month =  ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
		var day = d.getDate();
		var date = year + '-' + month[d.getMonth()] + '-' + day;

		user = $(this).data('user');
// Check if there are selected checkboxes
	//one checkbox selected
		if(selected.length==1) {
			id=selected[0];
			//ajax
			$.ajax({
				type:"POST",
				url: TransferURL,
				data: {
					_token:token,
					id:id,
					action:action,
					date:date,
					time:time,
					user:user 
					
					},
				success: function(data)
				{
				window.location="/dashboard";
				}
			})
		} 
	//two or more selected checkbox
		else if (selected.length>=2){
			for(var i=0; i < selected.length; i++){
				trans_select = selected[i];
				//ajax
				$.ajax({
					type:"POST",
					url: TransferURL,
					data: {
						_token:token,
						id:trans_select,
						action:action,
						date:date,
						time:time,
						user:user 						
						},
					success: function(data)
					{
						//alert(data)
					//window.location="/pending_tickets";
					}
				})
				setTimeout(function() {
					window.location="/dashboard";
				}, 1000);
			}
			
		}
	//else if (selected.length==0){
		//$("#accept").hide();	
        //}
    });
});

</script>
@endsection