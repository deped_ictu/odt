@extends('layouts.master')
@section ('title')
    DepEd | Marikina DTS | My Tickets
@endsection

@section('content') 
<div class="row">
@include('inc.left_panel')
<style>
button, input[type="submit"], input[type="reset"] {
	background: none;
	color: inherit;
	border: none;
	padding: 0;
	font: inherit;
	cursor: pointer;
	outline: inherit;
}
.box{
	background-color:#ac3b61;
	
}
.box h4{
	color:white;
}
.for-body{
	background-color:white;
}
</style>

        <div class="col-sm-9">
			<!--Out going tickets-->
			<div class="panel box">
					<div class="panel-heading">
						<h4>Outgoing Tickets ({{ Session::get('Department') }}) </h4>
					</div>
					<div class="panel-body for-body">
					<table id="outgoing" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th width="30">ID</th>
							<th width="100">TN</th>
							<th>Title</th>
                            <th width="60">Route</th>
						</tr>
					</thead>
					 <tbody>
						@foreach($outgoingT as $outgoing)
						@if($outgoing['action']=='Released' || $outgoing['action']=='Filed' || $outgoing['action']=='Without Action' || $outgoing['action']=='Transferred')
						@else
						@if($outgoing['tn']=='')
						@else
							<tr>     
							    <td>{{ $outgoing['id'] }}</td>  							
								<td>
								<form method="POST" action="{{route('ticket')}}">
									<input type="hidden" name="tn" value="{{ $outgoing['tn']}}" />
									{!! csrf_field() !!}
									<button type="submit"><a heref="#">{{  $outgoing['tn'] }}</a></button>
								</form>
								</td>
								<td>{{ $outgoing['title'] }}</td>
                                <td>{{ $outgoing['route']}} </td>
							</tr>
						@endif
						@endif
						@endforeach
					</tbody> 
					</table>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript">
	/*$(document).ready(function() {
	    $('#mytickets').DataTable({
	        "processing": true,
	        "serverSide": true,
	        "ajax": {
	        	url: "{{route('fetch_tickets')}}",
	        	data:{
	        		cname : "{{ Session::get('Department') }}"
					//route : "{{ Session::get('Department') }}"
	        	}
	        },
	        "columns":[
	        	{"data":"id"},
	        	{"data":"tn"},
	        	{"data":"title"},
	        	{"data":"action"},
	        	{"data":"route"}
	        ]
	    } );
	} );*/
</script>
@endsection