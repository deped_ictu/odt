@extends('layouts.master')
@section('content')
<div class="row">
	<div class="col-md-12">
			<h2>Transferred Tickets</h2>
	</div>
</div>
<div class="row">
<div class="col-sm-3">
	    <div class="panel panel-info">
	        <div class="panel-heading">
	            
            <!--Search-->
    		<form action="{{route('search')}}" autocomplete="off" method="POST">
    		{!! csrf_field() !!}
                <div class="input-group input-group-lg">
                    <input type="text" id="search" class="form-control" name="txtsearch_ticket" placeholder="" required/>
                    <span class="input-group-btn">
                        <button class="btn btn-default" id="btnsearch">GO</button>
                    </span>
				</div>
				<div>
					<!--<input type="checkbox" name="searchtn" id="exactsearchtn"/><p>Search key words</p>	-->	 
                </div>
            </form>

			<?php
				$sdodepartments = array("SGOD","CID","ICTU","SDS","Finance","Admin","Supply","Cash", "Records","Personnel", "Health", "Legal", "PSDS", "ASDS");
				$Is_Sdo_account = false;
				if(in_array(Session::get('Department'), $sdodepartments)){
					//this is a SDO account
					$Is_Sdo_account = true;			
				}else{
					//This is a School Account
					$Is_Sdo_account = false;
				}

				$datacount = 0;
                $overduecount = 0;
			?>                   
            <!--End of Search-->	
	        </div><!--end of panel-heading-->
	        <div class="list-group">
				<!--<a href="#" class="list-group-item">Assigned Ticket</a>-->
				
				@if($Is_Sdo_account==false)
				<a href="#" class="list-group-item" data-toggle="modal" data-target="#create_ticket"><span class="glyphicon glyphicon-plus"></span> Create Ticket</a>
				<a class="list-group-item" href="{{route('closed_tickets')}}">Closed Tickets</a>
				@elseif($Is_Sdo_account==true)

				<a href="#" class="list-group-item" data-toggle="modal" data-target="#create_ticket"><span class="glyphicon glyphicon-plus"></span> Create Ticket</a>
				<a href="{{route('dashboard')}}" class="list-group-item" data-toggle="modal" data-target="#create_ticket"><span class="glyphicon glyphicon-glyphicon glyphicon-th-list"></span>&nbsp;Assigned Tickets
				<span class="badge badge-info" id="assigned">
					{{ $datacount}}
					</span>
				</a>
				<a class="list-group-item" href="#" data-toggle="modal" data-target="#dynamicmodal"><span class="glyphicon glyphicon-pause"></span> For Receipt 
					<span class="badge badge-info pending">
					{{ $pendingtick }}
					</span>
				</a>
				<a class="list-group-item" href="{{route('transferred')}}" data-toggle="modal" data-target="#"><span class="glyphicon glyphicon-share-alt"></span> Transferred Tickets
                <span class="badge badge-info transferred">
                {{ $transferbu }}
					</span>
				</a>
				
				@endif
				@if(Session::get('Department')=='Records')
				<a href="{{route('pending_submission')}}" class="list-group-item" data-toggle="modal" data-target="#"><span class="glyphicon glyphicon-pause"></span> Tickets From School <span class="badge badge-info pensub">
					{{ $pensub }}
					</span></a>
				<a href="{{route('pending_tickets_fromschool')}}" class="list-group-item" data-toggle="modal" data-target="#"><span class="glyphicon glyphicon-trash"></span> Clear Tickets</a>
				@else
				@endif
				<!--<a href="#" class="list-group-item" data-toggle="modal" data-target="#">Follow-up Ticket</a>-->
			</div>
		</div>
			@if($Is_Sdo_account==false)
			@else
			<table class="table table-striped table-bordered">
				<tr>
					<th style="text-align:center;">Open</th>
					<th style="text-align:center;"><a href="#" data-toggle="modal" data-target="#overdue_ticket">Overdue</a></th>
				</tr>
				<tr>
					<td align="center" ><span id="LabelOpen"></span></td>
					<td align="center"><span id='LabelOverdue'>0</span></td>
				</tr>
			</table>
			<!--EFFICIENCY COUNT-->
			<div class='panel panel-default' id="effpanel" style="background-image: linear-gradient(to right, #55efc4 , #00b894);">
			    <div class='panel-body'> <center><h1 id='effrat' style='color:white; text-shadow: 0px 1px 2px rgba(0,0,0,0.5);'></h1>
			    <h1 style='color:white; text-shadow: 0px 1px 2px rgba(0,0,0,0.5);' id='efficiencyrat'></h1>
			    </center></div>
			</div>
			@endif
	</div>
	  
			<div class="col-sm-9">
				<div class="panel panel-default">
					<div class="panel-heading">
					
						<h3>Transferred Tickets by {{ Session::get('Department') }} office</h3>
					</div>
					<div class="panel-body"> 
                    <table id="assigned_ticket" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th width="30">TN</th>
					<th>Title</th>
                    <th>Transfer to</th>
					<th>Status</th>
					<th width="20">Date/Time</th>
				</tr>
			</thead>
			<tbody>
                <?php
                $datacount = 0;
                $overduecount = 0;
                ?>
				@foreach($transferred as $transferred)

					<?php
					//VIRMIL ADDED CODE >>>>>>>> OVERDUE DETECTOR
					$datacount ++;
					echo "<script>
					
					$('#LabelOpen').html($datacount);
					$('#LabelOverdue').html($overduecount);
					$('#assigned').html($datacount);
					</script>";
					//VIRMIL ADDED CODE >>>>>>>> OVERDUE DETECTOR
					?>
					<td>{{ $transferred['tn'] }}</td>	
					<td>{{ $transferred['title'] }}</td>
                    <td>{{$transferred['transfer_to']}}</td>
					<td>{{$transferred['status']}}</td>
				    <td>{{ $transferred['date_time'] }} - {{ $transferred['time'] }}</td>

				</tr>

			@endforeach
			</tbody>  
	    </table>
				</div>
			</div>
		</div>
</div><!--end of row-->
<script>
	var acceptPSURL="{{route('accept_PSticket')}}";
</script>

@endsection
<script>
    EfficiencyRating();
    function EfficiencyRating(){
        var open = $("#LabelOpen").html();
        var overdue = $("#LabelOverdue").html();
        var rating = open - overdue;
        rating = rating / open * 100;
        var n = rating.toFixed(2);
            if(rating > 90){
        $("#effrat").html('<i class="far fa-grin-stars"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
    }else if(rating > 80){
                $("#effpanel").css("background-image","linear-gradient(to right, #FFEB3B , #8BC34A)");
        $("#effrat").html('<i class="far fa-smile"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
    }else if(rating > 70){
        $("#effrat").html('<i class="far fa-meh"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
    }else if(rating > 50){
        $("#effpanel").css("background-image","linear-gradient(to right, #F44336 , #FFC107)");
        $("#effrat").html('<i class="far fa-frown"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
    }else if(rating > 20){
        $("#effrat").html('<i class="far fa-sad-tear"></i><br><small style="font-size=5px; color:white;">Efficiency Rating</small>');
    }

        $("#efficiencyrat").html(n);
    }     
setInterval(function()
{
    EfficiencyRating();

}, 300);
</script>

<script>
//For dashborad popover
$(".btnaddinfo").click(function(){
    var daysindepartment = $(this).data("dayscount");
    var Loc = $(this).data("location");
    var Rem = $(this).data("remarks");
    var Stat = $(this).data("status");
    if(Loc == "" && Loc == null){ Loc="(empty)";}
    if(Rem.length ==0 ){ Rem="(empty)";}
    if(Stat == "" && Stat == null){ Stat="(empty)";}
    $("#d1").html(Loc);
    $("#d2").html(Rem);
    $("#d3").html(Stat); 
    
    if(daysindepartment > 5){
        $("#overduepanel").css("display","block");
    }else{
        $("#overduepanel").css("display","none");
    }
     $("#d0").html(daysindepartment); 

});
    var closedTicketURL="{{route('closed_ticket')}}";
	var submitTicketURL="{{route('submit_ticket')}}";
     
</script>

<!--search for Exact match--> 
            <script>
            $("#btnsearch").click(function(){
                var searchkeyword = $("#search").val();

				var exacttn = document.getElementById("exactsearchtn"); 
				var exacttn;
				if(exacttn.checked == false){
				
                //alert(searchkeyword);
                $.ajax({
                    type: "POST",
                    url: "{{route('searchexact')}}",
                    data: {
                        _token:"{!! csrf_token() !!}",
                        txtsearch_ticket:searchkeyword},
                    success: function(data){
                        $("#ticketnumbersearched").html(searchkeyword + "<br>");
                        $("#doctitle").html("(not found)");
                        $("#off_origin").html("(not found)");
                        data = JSON.parse(data);
                        $("#search").val("");
                        //alert(JSON.stringify(data));
                       $("#searchresult").html("");
                       for(var i =0; i < data.length;i++){
                           
                        $fdate = new Date(data[i]["date_time"]);
                        $fdate = $fdate.getDay() + "/" + $fdate.getMonth() + "/" + $fdate.getFullYear();
                        $("#doctitle").html(data[i]["title"].toUpperCase());
                        $("#searchresult").append("<tr><td>" + $fdate + " - " + data[i]["time"] + "</td>" + 
                        "<td>" + data[i]['route']  + "</td>" + 
                        "<td><div class='rem'>" + data[i]['remarks'] +"</div></td>" + 
                        "<td>" + data[i]['status'] +"</td>" + 
                        "</tr>");
                        if(i <= data.length){
                            $("#off_origin").html(data[i]["origin"] + " (" + data[i]["cname"] + ")");
                        }
						
                       }
                    }
                })
			}
            })

            </script>
