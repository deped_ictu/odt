@extends('layouts.master')
@section ('title')
    DepEd | Marikina DTS | My Tickets
@endsection

@section('content') 
<div class="row">
@include('inc.left_panel')
<style>
button, input[type="submit"], input[type="reset"] {
	background: none;
	color: inherit;
	border: none;
	padding: 0;
	font: inherit;
	cursor: pointer;
	outline: inherit;
}
</style>

<div class="col-sm-9">
	<div class="panel panel-info">
		<div class="panel-heading">
			<h3>Incoming Tickets ({{ Session::get('Department') }}) <button class="btn btn-danger pull-right"><a href="{{route('deletetfs')}}">Clear Tickets</a></button></h3>
			
		</div>
		<div class="panel-body">
            <table id="incoming" class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th width="30">ID</th>
					<th width="30">Company Name</th>
                    <th width="30">Reference No.</th>
                    <th>Title</th>
                    <th width="60">Route</th>
                </tr>
            </thead>
            <tbody>
                @foreach($incomingT as $data)
                    <tr>     
                        <td>{{ $data['id'] }}</td> 
						<td>{{ $data['cname'] }}</td> 	
                        <td><button a href="#" class="btnedit" data-toggle="modal" data-target="#update_tn" id="update_blank_tn" data-id="{{$data['id']}}" data-user="{{Session::get('Firstname')}} {{Session::get('Lastname')}}" data-title="{{$data['title']}}" data-refno="{{$data['ref_no']}}" data-status="{{$data['status']}}" data-sender="{{$data['sender']}}" data-cname="{{$data['cname']}}">{{ $data['ref_no'] }}</button></td>
                        <!--<td>{{$data['ref_no']}}</td>-->
                        <td>{{ $data['title'] }}</td>
                        <td>{{$data['route']}} </td>
                    </tr>

                @endforeach
            </tbody> 
            </table>
		</div>
	</div>
</div>
</div><!--end of row-->

<!--edit ticket modal-->
<div class="modal fade" id="update_tn" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-sm">
      <div class="modal-header modal-primary">
        <h2 class="modal-title" id="exampleModalLabel">Update Ticket</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      <input type="hidden" name ="id" id="txtids" />
	  <input type="hidden" name="user" id="txtagent" value="{{Session::get('Firstname')}} {{Session::get('Lastname')}}" />
      <input type="hidden" name="ref_no" id="txtrefno"  />
      <input type="hidden" name="title" id="txttit"  />
      <input type="hidden" name="status" id="txtstat"  />
      <input type="hidden" name="sender" id="txtsend" />
      <input type="hidden" name="cname" id="txtcomname" />
     
        <div class="row">
          <div class="col-md-12">
						  <div class="form-group">
                <label for="add">&nbsp;<b>Document Type</b>&nbsp;&nbsp;</label>
                <select class="form-control" name="doc_type" id="doc_type">
					<option>Select Document Type</option>
					<option value="Admin">Admin</option>
					<option value="Finance">Finance</option>
					<option value="Legal">Legal</option>
					<option value="Personnel">Personnel</option>
					<option value="Private">Private</option>
				</select>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                  <label for="add">&nbsp;<b>Tracking No.</b>&nbsp;&nbsp;</label>
                  <input type="text" name="tn" id="txttno" class="form-control" placeholder="Tracking #" autocomplete="off"/>
              </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
              <label for="add">&nbsp;<b>Action</b>&nbsp;&nbsp;</label>
                <select class="form-control" name="action" id="txtact">
                <option value="Transferred">Transfer</option>
				<option value="Received">Received</option>
				<option value="Released">Released</option>
				<option value="Filed">Filed</option>
				<option value="Without Action">Without Action</option>
				<option value="Referred">Refer</option>
				</select>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
						<div class="form-group">
							<label for="add">Document Route</label>
              <select class="form-control" name="route" id="txtoffice">
								<option value="SDS">SDS</option>
								<option value="ASDS">ASDS</option>
								<option value="Admin">Admin</option>
								<option value="Finance">Finance</option>   
								<option value="Legal">Legal</option>
								<option value="Personnel">Personnel</option>
								<option value="CID">CID</option>
								<option value="SGOD">SGOD</option>
								<option value="PSDS">PSDS</option>
								<option value="Cash">Cash</option>
								<option value="Records">Records</option>
								<option value="Supply">Supply and Property</option>
								<option value="Health and Nutrition">Health and Nutrition</option>
								<option value="ICTU">ICTU</option>
							</select>
					  </div>	
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">				
              <label class="input-group">Remarks:</label>
              <input type="text" name="remarks" id="txtre" class="form-control" placeholder="Remarks" autocomplete="off"/>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="btnupdate_tn">Save</button>
      </div>
    </div>
  </div>
</div>
<script>
    var UpdateTicketTN="{{route('updatetn')}}";
	var INSERTRECORD="{{route('updatetn')}}";
</script>

@endsection
<script>
/*
$(".btnedit").click(function(){
	var token=$('input[name=_token]').val();
	var id = $(this).data('id');
	var title = $(this).data('title');
	var ref_no = $(this).data('refno');
	var status = $(this).data('status');
	var user = $(this).data('user');
	var sender = $(this).data('sender');
	var cname = $(this).data('cname');
	
	//$("#action").val(action);
	//$("#route").val(route);
	//$("#remarks").val(remarks);
	$("#txtids").val(id);
	//alert(user);
});

$('#btnupdate_tn').click(function() {
	var id = $("#txtids").val();
        var tn = $("#txttno").val();
		var title = $("#txttit").val();
		var refno = $("#txtrefno").val();
		var act = $("#txtact").val();
		var rem = $("#txtre").val();
		var stat = $("#txtstat").val();
		var send = $("#txtsend").val();
		var cname = $("#txtcomname").val();
		var doc_type = $("#doc_type").val();
		var route = $("#txtoffice").val();
        var _token=$('input[name=_token]').val();
        var id = $("#txtids").val();
		var action = 'Received';
		//var remarks = $("#remarks").val();
		var user = $("#txtagent").val();
		//get the date
		var d = new Date();
		var year = d.getFullYear();
		var month =  ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
		var day = d.getDate();
		var date = year + '-' + month[d.getMonth()] + '-' + day;
		//get the time
		var t = new Date();
		var hour = t.getHours();
		var min = t.getMinutes();
		var sec = t.getSeconds();
		var time = hour + ':' + min + ':' + sec;
    $.when(
        $.ajax({
			method:'post',
            url: UpdateTicketTN,
            data:{
                    '_token':_token,
                    'tn':tn,
                    'doc_type':doc_type,
                    'action':action,
					//'route':route,
					//'remarks':remarks,
					'date':date,
					'time':time,
					'user':user,
					'id':id,
                }
            })
            success: function(data) {
                alert('request complete')
            }
        //}),
        $.ajax({
			method:'post',
                url: INSERTRECORD,
                data:{
                    '_token':_token,
                    'tn':tn,
					'title':title,
					'ref_no':refno,
					'action':act,
					'remarks':rem,
					'status':stat,
					'sender':send,
					'cname':cname,
                    'doc_type':doc_type,
					'route':route,
					'date':date,
					'time':time,
					'user':user,
                }
            })
            success: function(data) {
                alert('request complete')
            }
        //})
    ).then( function(){
        alert('all complete');
    });
});
*/
 
</script>