@extends('layouts.master')
@section ('title')
    DepEd | Marikina DTS | Pending Submission Tickets
@endsection

@section('content') 
<div class="col-md-3">

    			
    			 <div class="list-group">
    					<a href="{{route('dashboard')}}" class="list-group-item">Assigned Ticket</a>
    					<a href="#" class="list-group-item" data-toggle="modal" data-target="#create_ticket">Create Ticket</a>
    					<a class="list-group-item" href="#" data-toggle="modal" data-target="#dynamicmodal">Pending Ticket</a>
    					<a href="{{route('pending_tickets_fromschool')}}" class="list-group-item" data-toggle="modal" data-target="#">Clear Tickets</a>
    					<!--<a href="#" class="list-group-item" data-toggle="modal" data-target="#">Follow-up Ticket</a>-->
    				</div>
</div>
        <div class="col-sm-9">
				<div class="panel panel-default">
					<div class="panel-heading">
					
						<h3>Pending Submission route to {{ Session::get('Department') }} office</h3>
                        <button class="btn btn-danger pending_submission" a href="{{route('delete_ps')}}">Clear all pending tickets from school</a></button>
					</div>
					<div class="panel-body">
					<table id="assigned_ticket" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th width="30">ID</th>
							<th width="100">TN</th>
							<th>Title</th>
                            <th width="60">From</th>
							
						</tr>
					</thead>
					<tbody>

						@foreach($pending_submission as $data)
							<tr>     
							    <td>{{ $data['id'] }}</td>     
								@if($data['status_2']=='Priority')
								<td style="background-color:#ffcccc; color:#000000; font-family:monospace;text-transform:uppercase;">{{ $data['tn'] }}</td>
								@else 
								<td style="font-family:monospace;text-transform:uppercase;">{{ $data['tn'] }}</td>
								@endif
								<td>{{ $data['title'] }}</td>
                                <td>{{$data['origin']}} - {{$data['cname']}}</td>
                    
                      <input type="hidden" id="id" name="txtid"/>
                                
								<!--add remarks-->
								
							</tr>
						@endforeach
					</tbody>  
					</table>
				</div>
			</div>
		</div>

<script>
	var DeletePendingSubmissionURL="{{route('delete_ps')}}";
</script>



@endsection