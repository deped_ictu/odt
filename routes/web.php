<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //Cache::put( 'cachekey', 'I am in the cache baby!', 1 );
    //return Cache::get( 'cachekey' );
    return view('index');
});

//login
//Route::get('/', ['uses'=>'LoginController@login', 'as'=>'index'] );

Route::get('/index', ['uses'=>'TicketsController@login', 'as'=>'index'] );
Route::post('/login_dts', ['uses'=>'TicketsController@login_dts', 'as'=>'login_dts'] );

//logout
Route::get('/logout', ['uses'=>'TicketsController@logout', 'as'=>'logout'] );

//dashboard 
Route::get('/dashboard', ['uses'=>'TicketsController@dashboard', 'as'=>'dashboard'] );
Route::post('/addticket', ['uses'=>'TicketsController@addTicket', 'as'=>'addticket'] );
Route::get('/pending_tickets', ['uses'=>'TicketsController@pending', 'as'=>'pending_tickets'] );
Route::post('/accept_ticket', ['uses'=>'TicketsController@AcceptTicket', 'as'=>'accept_ticket'] );
Route::post('/closed_ticket', ['uses'=>'TicketsController@closedTicket', 'as'=>'closed_ticket'] );
Route::post('/update_ticket', ['uses'=>'TicketsController@updateticket', 'as'=>'update_ticket'] );
Route::post('/delete_ticket', ['uses'=>'TicketsController@deletedticket', 'as'=>'delete_ticket']);
Route::post('/released_ticket', ['uses'=>'TicketsController@releasedTicket', 'as'=>'released_ticket'] );
Route::post('/transfer_ticket', ['uses'=>'TicketsController@transferTicket', 'as'=>'transfer_ticket'] );
Route::post('/assign_ticket', ['uses'=>'TicketsController@assignTicket', 'as'=>'assign_ticket'] );
Route::post('/submit_ticket', ['uses'=>'TicketsController@SubmitTicket', 'as'=>'submit_ticket'] );
Route::get('/pending_submission', ['uses'=>'TicketsController@pendingTicketsubmission', 'as'=>'pending_submission'] );
Route::post('/accept_PSticket', ['uses'=>'TicketsController@AcceptPSTicket', 'as'=>'accept_PSticket'] );
Route::get('/transferred', ['uses'=>'TicketsController@transferredTickets', 'as'=>'transferred'] );
Route::get('/transferredbyschool', ['uses'=>'TicketsController@transferredTS', 'as'=>'transferredbyschool'] );
Route::post('/changeprin_name', ['uses'=>'TicketsController@changeprincipalname', 'as'=>'changeprin_name'] );

Route::get('/print_transmital/{id}', ['uses'=>'TicketsController@getPtickets', 'as'=>'print_transmital'] );

Route::post('/addremarks', ['uses'=>'TicketsController@addRemarks', 'as'=>'addremarks'] );
Route::get('/getticket/{id}', ['uses'=>'TicketsController@getticket', 'as'=>'getticket']);

//search
Route::post('/search', ['uses'=>'TicketsController@search', 'as'=>'search'] );

Route::get('/print_logs', ['uses'=>'TicketsController@printlogs', 'as'=>'print_logs'] );
Route::post('/filter_date', ['uses'=>'TicketsController@filterdate', 'as'=>'filter_date'] );
Route::get('/pending_tickets_fromschool', ['uses'=>'TicketsController@pendingSubmission', 'as'=>'pending_tickets_fromschool'] );
Route::post('/delete_ps', ['uses'=>'TicketsController@deletependingsubmission', 'as'=>'delete_ps']);
Route::get('/mytickets', ['uses'=>'TicketsController@myticket', 'as'=>'mytickets'] );
Route::get('/created_ticket', ['uses'=>'TicketsController@createdTicket', 'as'=>'created_ticket'] );
Route::get('/overdue_tickets', ['uses'=>'TicketsController@overdue_ticket', 'as'=>'overdue_tickets'] );

Route::get('/helpdesk', ['uses'=>'TicketsController@hd', 'as'=>'helpdesk'] );
Route::get('/knowledgebase', ['uses'=>'TicketsController@kb', 'as'=>'knowledgebase'] );
Route::post('/hd', ['uses'=>'TicketsController@hd', 'as'=>'hd'] );
Route::post('/helpdesk_action', ['uses'=>'TicketsController@helpdeskaction', 'as'=>'helpdesk_action'] );
Route::get('/helpdesk_logs', ['uses'=>'TicketsController@helpdesklogs', 'as'=>'helpdesk_logs'] );
Route::post('/search_hd', ['uses'=>'TicketsController@searchHD', 'as'=>'search_hd'] );

Route::post('/search_trail', ['uses'=>'TicketsController@search_trail', 'as'=>'search_trail'] );
Route::post('/searchexact', ['uses'=>'TicketsController@searchexactticket', 'as'=>'searchexact'] );
Route::get('/for_transfer', ['uses'=>'TicketsController@transferticketto', 'as'=>'for_transfer'] );
Route::post('/accept_assigned', ['uses'=>'TicketsController@AcceptAssignedTicket', 'as'=>'accept_assigned'] );

Route::get('/archived', ['uses'=>'TicketController@archived_tickets', 'as'=>'archived'] );

//old tickets
Route::post('/archremarks', ['uses'=>'TicketController@archivedRemarks', 'as'=>'archremarks'] );

//Server Side
Route::get('/fetch_tickets', ['uses'=>'TicketController@fetch_tickets', 'as'=>'fetch_tickets'] ); //school account
Route::get('/mytickets', ['uses'=>'TicketsController@TicketsFOO', 'as'=>'mytickets'] ); 

//new dts
Route::POST('/ticket_details', ['uses'=>'TicketsController@ticketdetails', 'as'=>'ticket_details'] );
Route::POST('/ticket', ['uses'=>'TicketsController@ticketdetails', 'as'=>'ticket'] );
Route::POST('/reference', ['uses'=>'TicketsController@ReferenceNumber', 'as'=>'reference'] );
Route::POST('/addtrail', ['uses'=>'TicketsController@AddDocumentTrail', 'as'=>'addtrail'] );
Route::POST('/addtrail_SC', ['uses'=>'TicketsController@AddDocumentTrailSA', 'as'=>'addtrail_SC'] );
Route::POST('/addticket_SC', ['uses'=>'TicketsController@addTicketSchoolAccount', 'as'=>'addticket_SC'] );
Route::post('/updatetn', ['uses'=>'TicketsController@UpdateTN', 'as'=>'updatetn'] );
Route::get('/all_tickets', ['uses'=>'TicketsController@AllTickets', 'as'=>'all_tickets'] );
Route::post('/updateoneticket', ['uses'=>'TicketsController@UpdateOneTicket', 'as'=>'updateoneticket'] );

Route::get('/incoming_tickets', ['uses'=>'TicketsController@IncomingTickets', 'as'=>'incoming_tickets'] ); 
Route::get('/outgoing_tickets', ['uses'=>'TicketsController@OutgoingTickets', 'as'=>'outgoing_tickets'] ); 
Route::post('/search_trailticket', ['uses'=>'TicketsController@search_trailI', 'as'=>'search_trailticket'] );
Route::get('/school_tickets', ['uses'=>'TicketsController@SchoolTickets', 'as'=>'school_tickets'] ); 
//Route::get('/update_ticket', ['uses'=>'LoginController@UPDATE', 'as'=>'update_ticket'] ); 
Route::get('/update_ticket/{tn}', ['uses'=>'TicketsController@UPDATE', 'as'=>'update_ticket'] );
Route::post('/editthesametn', ['uses'=>'TicketsController@UpdatethesameTN', 'as'=>'editthesametn'] );
Route::post('/insertnew', ['uses'=>'TicketsController@INSERTNEWRECORDS', 'as'=>'insertnew'] );
Route::get('/admin', ['uses'=>'TicketsController@OPENTICKETS', 'as'=>'admin'] );
Route::post('/open_tickets', ['uses'=>'TicketsController@LISTOPENTICKETS', 'as'=>'open_tickets'] );
Route::post('/deletetfs', ['uses'=>'TicketsController@deletedticketfromschool', 'as'=>'deletetfs']);
Route::get('/filterdate', ['uses'=>'TicketsController@filterdate', 'as'=>'filterdate'] );
Route::post('/resetpassword', ['uses'=>'TicketsController@UpdateUserNamePass', 'as'=>'resetpassword'] );

//for transfer
Route::post('/transfer', ['uses'=>'TicketsController@Transfer_Ticket', 'as'=>'transfer'] );
Route::post('/transferall', ['uses'=>'TicketsController@Transfer_AllTicket', 'as'=>'transferall'] );




